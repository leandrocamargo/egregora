<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Servico extends Model
{
    use SoftDeletes;
    protected $table = "servicos";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "nome",
        "descricao",
        "valor",
        "color",
        "ativo",
        "imagem"
    ];

    public function pacotesServicos()
    {
        return $this->belongsToMany(Pacote::class,'pacotes_servicos','servico_id','pacote_id')->withPivot('valor_sessao','qtd_sessao','id')->withTrashed();
    }
}
