<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class TratamentoEstetico extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'tratamentos_esteticos';

    protected $fillable = [
        'id_cliente',
        'impl_dentario',
        'impl_dentario_qual',
        'trat_dermato',
        'trat_dermato_qual',
        'cir_plastica',
        'cir_plastica_qual',
        'cir_reparadora',
        'cir_reparadora_qual',
        'outra_cirurgia',
        'outra_cirurgia_qual'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente', 'cliente_id', 'id')->withTrashed();
    }

    public function validator(array $input)
    {
        $rules = array(
            'id_cliente'=>'required',
            'impl_dentario'=>'required',
//            'impl_dentario_qual'=>'required',
            'trat_dermato'=>'required',
//            'trat_dermato_qual'=>'required',
            'cir_plastica'=>'required',
//            'cir_plastica_qual'=>'required',
            'cir_reparadora'=>'required',
//            'cir_reparadora_qual'=>'required',
            'outra_cirurgia'=>'required',
//            'outra_cirurgia_qual'=>'required',
        );

        $msg = array(

            'id_cliente.required' => 'Por favor selecione o cliente',
            'impl_dentario.required' => 'Por favor preencha a pergunta 1',
//            'impl_dentario_qual.required' => 'Por favor preencha esta pergunta',
            'trat_dermato.required' => 'Por favor preencha a pergunta 2',
//            'trat_dermato_qual.required' => 'Por favor preencha esta pergunta',
            'cir_plastica.required' => 'Por favor preencha a pergunta 3',
//            'cir_plastica_qual.required' => 'Por favor preencha esta pergunta',
            'cir_reparadora.required' => 'Por favor preencha a pergunta 4',
//            'cir_reparadora_qual.required' => 'Por favor preencha esta pergunta',
            'outra_cirurgia.required' => 'Por favor preencha a pergunta 5',
//            'outra_cirurgia_qual.required' => 'Por favor preencha esta pergunta',
        );

        $validator = Validator::make($rules, $msg);

        return $validator;
    }
}
