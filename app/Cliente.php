<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Cliente extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'clientes';

    protected $fillable = [
        'nome',
        'cpf',
        'data_nasc',
        'profissao',
        'estado_civil',
        'unidade_id',
        'endereco_id',
        'contato_id',
        'ativo'
    ];

    const ESTADO_CIVIL = [
        'Solteiro(a)',
        'Casado(a)',
        'Viuvo(a)',
        'Divorciado(a)',
        'Outro'
    ];

    public static function clienteSave (Request $request, Endereco $endereco, Contato $contato): Cliente
    {
        $cliente = new Cliente();
        $cliente->nome         = $request->input('nome');
        $cliente->cpf          = $request->input('cpf');

        $data_nasc = str_replace('/', '-', $request->input('data_nasc'));
        $data_nasc_convertida = date("Y-m-d", strtotime($data_nasc));
        $cliente->data_nasc = $data_nasc_convertida;

        $cliente->profissao    = $request->input('profissao');
        $cliente->estado_civil = $request->input('estado_civil');
        $cliente->ativo        = $request->input('ativo');
        $cliente->unidade_id   = auth()->user()->unidade_id;
        $cliente->endereco_id  = $endereco->id;
        $cliente->contato_id   = $contato->id;

        $cliente->save();

        return $cliente;
    }

    public static function clienteUpdate (Request $request,$id, Endereco $endereco)
    {
        $cliente = Cliente::findOrfail($id);

        $cliente->nome         = $request->input('nome');
        $cliente->cpf          = $request->input('cpf');

        $data_nasc = str_replace('/', '-', $request->input('data_nasc'));
        $data_nasc_convertida = date("Y-m-d", strtotime($data_nasc));
        $cliente->data_nasc = $data_nasc_convertida;

        if(!$cliente->endereco_id) {
            $cliente->endereco_id = $endereco->id;
        }

        $cliente->profissao    = $request->input('profissao');
        $cliente->estado_civil = $request->input('estado_civil');
        $cliente->ativo        = $request->input('ativo');

        $cliente->save();

        return view('clients.index');
    }

    public function contato()
    {
        return $this->hasOne('App\Contato','id', 'contato_id')->withTrashed();
    }

    public function endereco()
    {
        return $this->hasOne('App\Endereco','id', 'endereco_id')->withTrashed();
    }

    public function unidade()
    {
        return $this->hasOne('App\Unidade','id','unidade_id')->withTrashed();
    }

    public function avaliacao()
    {
        return $this->hasMany('App\Avaliacao', 'id', 'id_cliente')->withTrashed();
    }

    public function anamnese()
    {
        return $this->hasMany('App\Anamnese', 'cliente_id', 'id')->withTrashed();
    }

    public function historicoMedico()
    {
        return $this->hasMany('App\HistoricoMedico', 'id', 'cliente_id')->withTrashed();
    }

    public function tratamentoEstetico()
    {
        return $this->hasMany('App\TratamentoEstetico', 'id', 'cliente_id')->withTrashed();
    }
}
