<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class FormaPagamento extends Model
{
    use SoftDeletes;
    protected $table = "forma_pagamento";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "nome",
        "bandeira",
        "parcela",
        "observ"
    ];

    public function validator($data)
    {
        $rules = array(
            'nome' => 'required',
        );

        $messages = array(
            'nome.required'=> 'O campo nome é obrigatório',
        );

        $validator = Validator::make($data, $rules, $messages);

        return $validator;
    }
}
