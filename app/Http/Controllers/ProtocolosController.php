<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ProtocolosController extends Controller
{
    public function index()
    {

    }

    public function protocolo(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrFail($id);

        return view('protocolos.protocolo', compact('cliente'));
    }
}
