<?php

namespace App\Http\Controllers;

use App\Pacote;
use App\Produto;
use App\Servico;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    public function index(Produto $model)
    {
        return view('products.index', ['products' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $produto = new Produto();
        $produto->nome = $request->input('nome');
        $produto->descricao = $request->input('descricao');
        $produto->preco = $request->input('preco');
        $produto->estoque = $request->input('estoque');
        $produto->imagem = $request->input('imagem');

        $produto->save();

        return redirect()->route('produto.index')->withStatus(__('Produto criado com sucesso.'));
    }

    public function edit(Produto $produto_edit, $id)
    {
        $produto = $produto_edit->findOrFail($id);

        return view('products.edit', compact('produto'));
    }

    public function update(Request $request, $id)
    {
        $produto = Produto::findOrFail($id);
        $produto->nome = $request->input('nome');
        $produto->descricao = $request->input('descricao');
        $produto->preco = $request->input('preco');
        $produto->estoque = $request->input('estoque');
        $produto->imagem = $request->input('imagem');

        $produto->save();

        return redirect()->route('produto.index')->withStatus(__('Produto editado com sucesso.'));
    }

    public function destroy(Produto $produto)
    {
        $produto->delete();

        return redirect()->route('produto.index')->withStatus(__('Produto deletado com sucesso.'));
    }
}
