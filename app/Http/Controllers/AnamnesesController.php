<?php

namespace App\Http\Controllers;

use App\Agenda;
use App\Anamnese;
use App\Cliente;
use Illuminate\Http\Request;

class AnamnesesController extends Controller
{
    public function index(Anamnese $model)
    {
        return view('anamneses.index', ['anamneses' => $model]);
    }

    public function create(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrfail($id);

        return view('anamneses.create',compact('cliente'));
    }

    public function store(Request $request)
    {
        //dd($request);
        $anamnese = new Anamnese();
        $anamnese->cliente_id = $request->input('cliente_id');
        $anamnese->trat_estetic_anterior = $request->input('trat_estetic_anterior');
        $anamnese->trat_estetic_anterior_qual = $request->input('trat_estetic_anterior_qual');
        $anamnese->utiliz_cosmeticos = $request->input('utiliz_cosmeticos');
        $anamnese->utiliz_cosmeticos_qual = $request->input('utiliz_cosmeticos_qual');
        $anamnese->exp_sol = $request->input('exp_sol');
        $anamnese->uso_filtro = $request->input('uso_filtro');
        $anamnese->tabagismo = $request->input('tabagismo');
        $anamnese->bebida_alcoolica = $request->input('bebida_alcoolica');
        $anamnese->func_intestinal = $request->input('func_intestinal');
        $anamnese->func_intestinal_obs = $request->input('func_intestinal_obs');
        $anamnese->qualidade_sono = $request->input('qualidade_sono');
        $anamnese->qualidade_sono_obs = $request->input('qualidade_sono_obs');
        $anamnese->ingest_agua = $request->input('ingest_agua');
        $anamnese->alimentacao = $request->input('alimentacao');
        $anamnese->alimentos_pref = $request->input('alimentos_pref');
        $anamnese->prat_atv_fisica = $request->input('prat_atv_fisica');
        $anamnese->prat_atv_fisica_freq = $request->input('prat_atv_fisica_freq');
        $anamnese->uso_anticoncepcional = $request->input('uso_anticoncepcional');
        $anamnese->gestacao = $request->input('gestacao');
        $anamnese->gestacao_qtde = $request->input('gestacao_qtde');
        $anamnese->gravida = $request->input('gravida');

        $anamnese->save();

        return redirect()->route('sessao.index')->withStatus(__('Anamnese criado com sucesso.'));
    }

    public function edit(Anamnese $anamnese_edit, $id)
    {
        $anamnese = $anamnese_edit->findOrFail($id);

        return view('anamneses.edit', compact('anamnese'));
    }

    public function update(Request $request, $id)
    {
        $anamnese = Anamnese::findOrFail($id);
        $anamnese->cliente_id = $request->input('cliente_id');
        $anamnese->trat_estetic_anterior = $request->input('trat_estetic_anterior');
        $anamnese->trat_estetic_anterior_qual = $request->input('trat_estetic_anterior_qual');
        $anamnese->utiliz_cosmeticos = $request->input('utiliz_cosmeticos');
        $anamnese->utiliz_cosmeticos_qual = $request->input('utiliz_cosmeticos_qual');
        $anamnese->exp_sol = $request->input('exp_sol');
        $anamnese->uso_filtro = $request->input('uso_filtro');
        $anamnese->tabagismo = $request->input('tabagismo');
        $anamnese->bebida_alcoolica = $request->input('bebida_alcoolica');
        $anamnese->func_intestinal = $request->input('func_intestinal');
        $anamnese->func_intestinal_obs = $request->input('func_intestinal_obs');
        $anamnese->qualidade_sono = $request->input('qualidade_sono');
        $anamnese->qualidade_sono_obs = $request->input('qualidade_sono_obs');
        $anamnese->ingest_agua = $request->input('ingest_agua');
        $anamnese->alimentacao = $request->input('alimentacao');
        $anamnese->alimentos_pref = $request->input('alimentos_pref');
        $anamnese->prat_atv_fisica = $request->input('prat_atv_fisica');
        $anamnese->prat_atv_fisica_freq = $request->input('prat_atv_fisica_freq');
        $anamnese->uso_anticoncepcional = $request->input('uso_anticoncepcional');
        $anamnese->gestacao = $request->input('gestacao');
        $anamnese->gestacao_qtde = $request->input('gestacao_qtde');
        $anamnese->gravida = $request->input('gravida');

        $anamnese->save();

        return redirect()->route('atender.cliente', $anamnese->cliente_id)->withStatus(__('Anamnese editado com sucesso.'));
    }
}
