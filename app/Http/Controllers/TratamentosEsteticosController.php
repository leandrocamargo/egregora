<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\TratamentoEstetico;
use Illuminate\Http\Request;

class TratamentosEsteticosController extends Controller
{
    public function index(TratamentoEstetico $model)
    {
        return view('tratamentos-esteticos.index', ['tratamentos_esteticos' => $model]);
    }

    public function create(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrfail($id);

        return view('tratamentos-esteticos.create',compact('cliente'));
    }

    public function store(Request $request)
    {
        $tratamento_estetico = new TratamentoEstetico();
        $tratamento_estetico->cliente_id = $request->input('cliente_id');
        $tratamento_estetico->impl_dentario = $request->input('impl_dentario');
        $tratamento_estetico->impl_dentario_qual = $request->input('impl_dentario_qual');
        $tratamento_estetico->trat_dermato = $request->input('trat_dermato');
        $tratamento_estetico->trat_dermato_qual = $request->input('trat_dermato_qual');
        $tratamento_estetico->cir_plastica = $request->input('cir_plastica');
        $tratamento_estetico->cir_plastica_qual = $request->input('cir_plastica_qual');
        $tratamento_estetico->cir_reparadora = $request->input('cir_reparadora');
        $tratamento_estetico->cir_reparadora_qual = $request->input('cir_reparadora_qual');
        $tratamento_estetico->outra_cirurgia = $request->input('outra_cirurgia');
        $tratamento_estetico->outra_cirurgia_qual = $request->input('outra_cirurgia_qual');

        $tratamento_estetico->save();

        return redirect()->route('atender.cliente', $tratamento_estetico->cliente_id)->withStatus(__('Tratamento Estético criado com sucesso.'));
    }

    public function edit(TratamentoEstetico $tratamento_estetico_edit, $id)
    {
        $tratamento_estetico = $tratamento_estetico_edit->findOrFail($id);

        return view('tratamentos-esteticos.edit', compact('tratamento_estetico'));
    }

    public function update(Request $request, $id)
    {
        $tratamento_estetico = TratamentoEstetico::findOrFail($id);
        $tratamento_estetico->cliente_id = $request->input('cliente_id');
        $tratamento_estetico->impl_dentario = $request->input('impl_dentario');
        $tratamento_estetico->impl_dentario_qual = $request->input('impl_dentario_qual');
        $tratamento_estetico->trat_dermato = $request->input('trat_dermato');
        $tratamento_estetico->trat_dermato_qual = $request->input('trat_dermato_qual');
        $tratamento_estetico->cir_plastica = $request->input('cir_plastica');
        $tratamento_estetico->cir_plastica_qual = $request->input('cir_plastica_qual');
        $tratamento_estetico->cir_reparadora = $request->input('cir_reparadora');
        $tratamento_estetico->cir_reparadora_qual = $request->input('cir_reparadora_qual');
        $tratamento_estetico->outra_cirurgia = $request->input('outra_cirurgia');
        $tratamento_estetico->outra_cirurgia_qual = $request->input('outra_cirurgia_qual');

        $tratamento_estetico->save();

        return redirect()->route('atender.cliente', $tratamento_estetico->cliente_id)->withStatus(__('Tratamento Estético editado com sucesso.'));
    }
}
