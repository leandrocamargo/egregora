<?php

namespace App\Http\Controllers;

use App\Pacote;
use App\PacoteServico;
use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PacotesController extends  Controller
{
    public function index(Pacote $model)
    {
        return view('pacotes.index', ['pacotes' => $model->paginate(15)]);
    }

    public function listagem()
    {
        return Pacote::with('pacotesServicos')->get();
    }

    public function create()
    {
        $servicos = Servico::all();

        return view('pacotes.create', compact('servicos'));
    }

//    aqui grava só os campos da tabela pacote, falta os campos da tabela pacoteservico
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
            'nome_pacote' => 'required',
            'descricao' => 'required',
        ];

        $msg = [
            'nome_pacote.required' => 'O campo nome é obrigatório',
            'descricao.required' => 'O campo descrição é obrigatório',
        ];

        $validator = Validator::make($input, $rules, $msg);

        if (!$validator->fails()) {
            try{
                $imagem = $request->file('imagem');
                $file = $imagem->getClientOriginalName();
                $imagem->move(public_path('storage/pacote/'),$file);
                $input['imagem'] = 'http://' . $_SERVER['SERVER_NAME'] . ':8080' . '/storage/pacote/'.$file;
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            $pacote = Pacote::create($input);

            $valores       = $input['valor'];
            $sessoes       = $input['qtd_sessao'];
            $servicosArray = $input['id_servico'];

            if ($pacote) {
                foreach ($valores as $key => $valor)
                {
                    $servico = Servico::findOrFail($servicosArray[$key]);
                    $pacote->pacotesServicos()->attach($servico,
                        [
                            'qtd_sessao'   => $sessoes[$key],
                            'valor_sessao' => $valores[$key]
                        ]
                    );
                }
                return redirect('/pacote')->with(['success' => 'Cadastro com sucesso']);

            } else {

                return back()->with(['errors' => 'Erro ao Cadastrar']);
            }

        } else {

            return back()->withErrors($validator)->withInput();
        }
    }

    public function edit($id)
    {
        $pacote = Pacote::findOrFail($id);

        $pacotesServicos = $pacote->pacotesServicos;

        $servicos = Servico::all();

        return view('pacotes.edit', compact('pacote', 'servicos', 'pacotesServicos'));
    }

    public function update(Request $request, $id)
    {
        $pacote = Pacote::findOrFail($id);

        $input = $request->toArray();

        if($pacote){
            $validator = $pacote->validator($input);

            if($validator->passes()){

                $pacote->update($input);

                $valores            = $input['valor'];
                $sessoes            = $input['qtd_sessao'];
                $servicosArray      = $input['id_servico'];
                $servicosArrayPivot = $input['id_servicoIdPivot'];

                foreach ($valores as $key => $valor)
                {
                    $servico = Servico::findOrFail($servicosArray[$key]);

                    if (isset($servicosArrayPivot[$key])){
                        $pacoteServico = PacoteServico::findOrFail($servicosArrayPivot[$key]);
                        $pacoteServico->qtd_sessao   = $sessoes[$key];
                        $pacoteServico->valor_sessao = $valores[$key];
                        $pacoteServico->servico_id   = $servicosArray[$key];

                        $pacoteServico->save();
                    } else {
                        $pacote->pacotesServicos()->attach( $servico->id,
                            [
                                'qtd_sessao'   => $sessoes[$key],
                                'valor_sessao' => $valores[$key]
                            ]
                        );
                    }
                }

                if ($request->input('deletados') != ""){
                    $arrayDeletados = explode(',',$request->input('deletados'));

                    foreach ($arrayDeletados as $index){
                        $servicoId = Servico::findOrFail($index);
                        $pacotesServicos = PacoteServico::where('servico_id',$servicoId->id)->where('pacote_id',$id)->first();
                        $pacotesServicos->delete();
                    }
                }

                return redirect()->route('pacote.index')->withStatus(__('Pacote editado com sucesso.'));

            } else {

                return back()->withErrors($validator)->withInput();
            }

        } else {

            return redirect()->route('pacote')->with(['errors' => "Pacote não encontrado"]);
        }
    }

    public function destroy(Pacote $pacote)
    {
        $pacote->delete();

        return redirect()->route('pacote.index')->withStatus(__('Pacote deletado com sucesso.'));
    }

    public function getServico(Request $request)
    {
        $servico = Servico::where('id', $request->id)->first();

        return response()->json($servico);
    }
}
