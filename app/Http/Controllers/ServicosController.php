<?php

namespace App\Http\Controllers;

use App\Servico;
use Illuminate\Http\Request;

class ServicosController extends  Controller
{
    public function index(Servico $model)
    {
        return view('servicos.index', ['servicos' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('servicos.create');
    }

    public function store(Request $request)
    {
        $servico = new Servico();
        $servico->nome = $request->input('nome');
        $servico->descricao = $request->input('descricao');
        $servico->valor = $request->input('valor');

        $imagem = $request->file('imagem');
        $file = $imagem->getClientOriginalName();
        $imagem->move(public_path('storage/servico/'),$file);
        $servico->imagem = 'http://' . $_SERVER['SERVER_NAME'] . ':8080' . '/storage/servico/'.$file;

        $servico->save();

        return redirect()->route('servico.index')->withStatus(__('Serviço criado com sucesso.'));
    }

    public function edit(Servico $servico_edit, $id)
    {
        $servico = $servico_edit->findOrFail($id);

        return view('servicos.edit', compact('servico'));
    }

    public function update(Request $request, $id)
    {
        $servico = Servico::findOrFail($id);
        $servico->nome = $request->input('nome');
        $servico->descricao = $request->input('descricao');
        $servico->valor = $request->input('valor');

        $imagem = $request->file('imagem');
        $file = $imagem->getClientOriginalName();
        $imagem->move(public_path('storage/servico/'),$file);
        $servico->imagem = 'http://' . $_SERVER['SERVER_NAME'] . ':8080' . '/storage/servico/'.$file;

        $servico->save();

        return redirect()->route('servico.index')->withStatus(__('Serviço editado com sucesso.'));
    }

    public function destroy(Servico $servico)
    {
        $servico->delete();

        return redirect()->route('servico.index')->withStatus(__('Serviço deletado com sucesso.'));
    }

    public function getValor($id)
    {
        $getValor = Servico::select('valor')->where('id','=',$id)->first();

        return $getValor->valor;
    }

    public function listagem()
    {
        return Servico::all();
    }
}
