<?php

namespace App\Http\Controllers;

use App\Agenda;
use App\Anamnese;
use App\Avaliacao;
use App\Cliente;
use App\Contrato;
use App\ContratoServico;
use App\HistoricoMedico;
use App\Sessao;
use App\TratamentoEstetico;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class   SessoesController extends Controller
{
    public function index()
    {
        $atendimentos = Agenda::with('servico')
            ->orderBy('horario_inicio', 'DESC')
            ->paginate(10);

        return view('sessoes.index', compact('atendimentos'));
    }

    public function atender(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrFail($id);
        $avaliacao = Avaliacao::where('cliente_id', $cliente->id)->first();
        $anamnese = Anamnese::where('cliente_id', $cliente->id)->first();
        $historico_medico = HistoricoMedico::where('cliente_id', $cliente->id)->first();
        $tratamento_estetico = TratamentoEstetico::where('cliente_id', $cliente->id)->first();
        $contratos = Contrato::where('id_cliente', $cliente->id)->orderBy('data','DESC')->paginate(5);

        return view('sessoes.sessao', compact('cliente','avaliacao','anamnese', 'historico_medico','tratamento_estetico','contratos'));
    }

    //Funções do outro sistema da egregora - não ultilizado nesse até o momento
    public function atendimento($id)
    {
        $contrato  = Contrato::findOrfail($id);//where('id_cliente','=',$id)->pluck('id')->toArray();
        $cliente = Cliente::where('id', $contrato->id_cliente)->get()->toArray();
        $contratos = ContratoServico::with('contrato')
            ->with('servico')
            ->with('pacote')
            ->whereIn('id_contrato',$contrato)
            ->get();

        return view('sessoes.executar-sessao', compact('contrato', 'contratos', 'cliente'));
    }

    public function getSessoes($id)
    {
        //$cliente = Cliente::findOrfail($id);
        $contratoAberto = Contrato::findOrFail($id);
        $cliente = Cliente::where('id', $contratoAberto->id_cliente)->get()->toArray();

        return $contrato = Contrato::where('id_cliente','=',$cliente[0]['id'])
            ->with('cliente')
            ->where('status','<>','Aberto')
            ->get();
    }

    public function getSessoesByContrato($id)
    {
        $sessao = Sessao::with('profissional')
            ->with('contrato')
            ->with('servico')
            ->with('contratoServico')
            ->where('status','=',0)
            ->where('contrato_id','=',$id)->get()->toArray();

        return $sessao;
    }

    public function executarServico(Request $request)
    {
        $sessao = Sessao::findOrFail($request->input('id'));
        $sessao->status = true;
        $sessao->profissional_id = auth()->id();
        $sessao->data_execussao = Carbon::now()->format('Y-m-d');
        $sessao->save();

        $contratoservico = ContratoServico::findOrFail($request->input('contrato_servico')['id']);
        if ($contratoservico->id_pacote === null) {
            $contratoservico->executado = "Fechado";
        } else {
            $sessaoAux = Sessao::where('contrato_servico_id','=',$contratoservico->id)
                ->where('status','=',0)->get()->toArray();
            if(sizeof($sessaoAux) == 1) {
                $contratoservico->executado = "Fechado";
            }
        }

        $contratoservico->save();

        return $sessao;
    }

    public function setData($id)
    {
        $executados = ContratoServico::findOrFail($id);
        $executados->data = Carbon::now()->format('Y-m-d');
        $executados->executado = 'Sim';

        $executados->save();

        return $executados;
    }
}
