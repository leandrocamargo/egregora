<?php

namespace App\Http\Controllers;

use App\TipoAtendimento;
use Illuminate\Http\Request;

class TiposAtendimentosController extends Controller
{
    public function index(TipoAtendimento $model)
    {
        return view('tipos-atendimentos.index', ['tiposAtendimentos' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('tipos-atendimentos.create');
    }

    public function store(Request $request)
    {
        $tipo_atendimento = new TipoAtendimento();
        $tipo_atendimento->descricao = $request->input('descricao');
        $tipo_atendimento->color = $request->input('cor');

        $tipo_atendimento->save();

        return redirect()->route('tipo-atendimento.index')->withStatus(__('Atendimento criado com sucesso.'));
    }

    public function edit(TipoAtendimento $tipo_atendimento_edit, $id)
    {
        $tipo_atendimento = $tipo_atendimento_edit->findOrFail($id);

        return view('tipos-atendimentos.edit', compact('tipo_atendimento'));
    }

    public function update(Request $request, $id)
    {
        $tipo_atendimento = TipoAtendimento::findOrFail($id);
        $tipo_atendimento->descricao = $request->input('descricao');
        $tipo_atendimento->color = $request->input('cor');

        $tipo_atendimento->save();

        return redirect()->route('tipo-atendimento.index')->withStatus(__('Atendimento editado com sucesso.'));
    }

    public function destroy(TipoAtendimento $tipoAtendimento)
    {
        $tipoAtendimento->delete();

        return redirect()->route('tipo-atendimento.index')->withStatus(__('Atendimento deletado com sucesso.'));
    }
}
