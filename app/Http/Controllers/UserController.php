<?php

namespace App\Http\Controllers;

use App\Service\UsuarioService;
use App\Unidade;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $service;

    public function __construct(UsuarioService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $unidades = Unidade::all();

        return view('users.create', compact('unidades'));
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->service->create($request);

        return redirect()->route('usuario.index')->withStatus(__('Usuário criado com sucesso.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $usuario)
    {
        $unidades = Unidade::all();

        return view('users.edit', compact('usuario', 'unidades'));
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
//        $hasPassword = $request->get('password');
//        $user->update(
//            $request->merge([
//                'password' => Hash::make($request->get('password'))
//                ])->except([$hasPassword ? '' : 'password'])
//            );

        return redirect()->route('usuario.index')->withStatus(__('Usuário editado com sucesso.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $usuario)
    {
        $usuario->delete();

        return redirect()->route('usuario.index')->withStatus(__('Usuário deletado com sucesso.'));
    }

    public function getUser()
    {
        return $this->service->getUser();
    }
}
