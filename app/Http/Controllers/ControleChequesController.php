<?php

namespace App\Http\Controllers;

use App\ControleCheque;
use Illuminate\Http\Request;

class ControleChequesController extends Controller
{
    public function index(ControleCheque $model)
    {
        return view('controle-cheques.index', ['cheques' => $model->OrderBy('vencimento', 'ASC')->paginate(15)]);
    }

    public function create()
    {
        return view('controle-cheques.create');
    }

    public function store(Request $request)
    {
        $parcelamento = $request->input('numero_parcela');
        $data = $request->input('data_vencimento');
        $numeroCheque = $request->input('numero_cheque');

        for ($i=1; $i <= $parcelamento; $i++) {

            $cheque = new ControleCheque();
            $cheque->cliente_id = $request->input('cliente_id');
            $cheque->contrato_id = $request->input('contrato_id');
            $cheque->banco = $request->input('banco');

            if ($i == 1) {
                $cheque->cheque = $numeroCheque++;
                $cheque->vencimento = $data;
            }
            else {
                $cheque->vencimento = date('Y-m-d', strtotime('+1 month', strtotime($data)));
                $data = date('Y-m-d', strtotime('+1 month', strtotime($data)));
                $cheque->cheque = $numeroCheque++;
            }

            $cheque->parcelas = $i. "/" .$request->input('numero_parcela');
            $cheque->status = 'EM DIA';

            $cheque->save();
        };

        return redirect()->route('cheque.index')->withStatus(__('Controle criado com sucesso.'));
    }
}
