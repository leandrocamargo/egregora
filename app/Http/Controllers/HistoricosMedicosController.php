<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\HistoricoMedico;
use Illuminate\Http\Request;

class HistoricosMedicosController extends Controller
{
    public function index(HistoricoMedico $model)
    {
        return view('historico-medico.index', ['historico_medico' => $model]);
    }

    public function create(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrfail($id);

        return view('historico-medico.create',compact('cliente'));
    }

    public function store(Request $request)
    {
        $historico_medico = new HistoricoMedico();
        $historico_medico->cliente_id = $request->input('cliente_id');
        $historico_medico->trat_med_atual = $request->input('trat_med_atual');
        $historico_medico->trat_med_atual_medic = $request->input('trat_med_atual_medic');
        $historico_medico->antec_alergico = $request->input('antec_alergico');
        $historico_medico->antec_alergico_qual = $request->input('antec_alergico_qual');
        $historico_medico->port_marcapasso = $request->input('port_marcapasso');
        $historico_medico->alt_cardiacas = $request->input('alt_cardiacas');
        $historico_medico->alt_pres_arterial = $request->input('alt_pres_arterial');
        $historico_medico->dis_circulatorio = $request->input('dis_circulatorio');
        $historico_medico->dis_circulatorio_qual = $request->input('dis_circulatorio_qual');
        $historico_medico->dis_renal = $request->input('dis_renal');
        $historico_medico->dis_renal_qual = $request->input('dis_renal_qual');
        $historico_medico->dis_hormonal = $request->input('dis_hormonal');
        $historico_medico->dis_hormonal_qual = $request->input('dis_hormonal_qual');
        $historico_medico->dis_gastro = $request->input('dis_gastro');
        $historico_medico->dis_gastro_qual = $request->input('dis_gastro_qual');
        $historico_medico->epilepsia_convulsao = $request->input('epilepsia_convulsao');
        $historico_medico->epilepsia_convulsao_freq = $request->input('epilepsia_convulsao_freq');
        $historico_medico->labirintite = $request->input('labirintite');
        $historico_medico->alteracao_psi = $request->input('alteracao_psi');
        $historico_medico->alteracao_psi_qual = $request->input('alteracao_psi_qual');
        $historico_medico->estresse = $request->input('estresse');
        $historico_medico->estresse_obs = $request->input('estresse_obs');
        $historico_medico->ant_oncologicos = $request->input('ant_oncologicos');
        $historico_medico->ant_oncologicos_qual = $request->input('ant_oncologicos_qual');
        $historico_medico->diabetes = $request->input('diabetes');
        $historico_medico->diabetes_tipo = $request->input('diabetes_tipo');
        $historico_medico->outra_doenca = $request->input('outra_doenca');

        $historico_medico->save();

        return redirect()->route('atender.cliente', $historico_medico->cliente_id)->withStatus(__('Histórico Médico criado com sucesso.'));
    }

    public function edit(HistoricoMedico $historico_medico_edit, $id)
    {
        $historico_medico = $historico_medico_edit->findOrFail($id);

        return view('historico-medico.edit', compact('historico_medico'));
    }

    public function update(Request $request, $id)
    {
        $historico_medico = HistoricoMedico::findOrFail($id);
        $historico_medico->cliente_id = $request->input('cliente_id');
        $historico_medico->trat_med_atual = $request->input('trat_med_atual');
        $historico_medico->trat_med_atual_medic = $request->input('trat_med_atual_medic');
        $historico_medico->antec_alergico = $request->input('antec_alergico');
        $historico_medico->antec_alergico_qual = $request->input('antec_alergico_qual');
        $historico_medico->port_marcapasso = $request->input('port_marcapasso');
        $historico_medico->alt_cardiacas = $request->input('alt_cardiacas');
        $historico_medico->alt_pres_arterial = $request->input('alt_pres_arterial');
        $historico_medico->dis_circulatorio = $request->input('dis_circulatorio');
        $historico_medico->dis_circulatorio_qual = $request->input('dis_circulatorio_qual');
        $historico_medico->dis_renal = $request->input('dis_renal');
        $historico_medico->dis_renal_qual = $request->input('dis_renal_qual');
        $historico_medico->dis_hormonal = $request->input('dis_hormonal');
        $historico_medico->dis_hormonal_qual = $request->input('dis_hormonal_qual');
        $historico_medico->dis_gastro = $request->input('dis_gastro');
        $historico_medico->dis_gastro_qual = $request->input('dis_gastro_qual');
        $historico_medico->epilepsia_convulsao = $request->input('epilepsia_convulsao');
        $historico_medico->epilepsia_convulsao_freq = $request->input('epilepsia_convulsao_freq');
        $historico_medico->labirintite = $request->input('labirintite');
        $historico_medico->alteracao_psi = $request->input('alteracao_psi');
        $historico_medico->alteracao_psi_qual = $request->input('alteracao_psi_qual');
        $historico_medico->estresse = $request->input('estresse');
        $historico_medico->estresse_obs = $request->input('estresse_obs');
        $historico_medico->ant_oncologicos = $request->input('ant_oncologicos');
        $historico_medico->ant_oncologicos_qual = $request->input('ant_oncologicos_qual');
        $historico_medico->diabetes = $request->input('diabetes');
        $historico_medico->diabetes_tipo = $request->input('diabetes_tipo');
        $historico_medico->outra_doenca = $request->input('outra_doenca');

        $historico_medico->save();

        return redirect()->route('atender.cliente', $historico_medico->cliente_id)->withStatus(__('Histórico Médico editado com sucesso.'));
    }
}
