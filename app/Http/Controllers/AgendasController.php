<?php

namespace App\Http\Controllers;

use App\Agenda;
use App\Cliente;
use App\Contato;
use App\TipoAtendimento;
use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class AgendasController extends Controller
{
    public function index()
    {
        $events = [];
        $clientes  = Cliente::all();
        $servicos  = TipoAtendimento::all();
        $agendados = Agenda::all();
        $calendar  = Calendar::addEvents($events);

        return view('agenda.index', compact('calendar','clientes', 'servicos','agendados'));
    }

    public function getEventos()
    {
        $events = [];

        /*Query que busca os eventos relacionando com o id da unidade do usuário logado
        através do relacionamento do agendamento com o cliente*/
        $data = Agenda::whereHas('cliente', function($query) {
            $query->where('unidade_id','=',auth()->user()->unidade_id);
        })->get();

        if($data->count()){
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date.' +1 day')
                );
            }
        }

        $dadosAgenda = $data->toArray();
        $response = [];

        foreach ($dadosAgenda as $chave => $dados)
        {
            $arrayAux = [
                'title' => '',
                'start' => '',
                'end'   => '',
                'color' => '',
                'id'    => ''
            ];

            $cliente = Cliente::findOrFail($dados['cliente_id']['id']);
            //$tipoAtendimento = TipoAtendimento::findOrFail($dados['tipo_atendimento_id']['id']);

            $arrayAux['title'] = $cliente->nome;
            $arrayAux['start'] = $dados['horario_inicio'];
            $arrayAux['end']   = $dados['horario_final'];
            $arrayAux['color'] = $dados['tipo_atendimento_id']['color'];
            $arrayAux['id']    = $dados['id'];

            $response[$chave] = $arrayAux;
        }

        return $response;
    }

    public function create()
    {
        $servicos  = TipoAtendimento::all();

        return view('agenda.create', compact('servicos'));
    }

    public function store(Request $request)
    {
        if ($request['input-id'] == null) {
            $contato = new Contato();
            $contato->telefone = $request->input('telefone');
            $contato->celular = $request->input('celular');

            $contato->save();

            if (isset($contato->id)) {
                $cliente = new Cliente();
                $cliente->cpf = $request->input('cpf');
                $cliente->nome = $request->input('nome');
                $cliente->unidade_id = auth()->user()->unidade_id;
                $cliente->contato_id = $contato->id;

                $cliente->save();
            } else {
                return redirect()->route('agenda.index')->withStatusError(__('Não foi possível cadastrar o contato.'));
            }

            if (isset($cliente->id)) {
                $agendar = new Agenda();
                $agendar->cliente_id = $cliente->id;

                if ($request->input('horario_inicio') < date('d/m/Y H:i')) {

                    return redirect()->back()->withErrors(__('Não é possível agendar para uma data passada!'));
                }

                if ($request->input('horario_final') < date('d/m/Y H:i')) {

                    return redirect()->back()->withErrors(__('Não é possível agendar para uma data passada!'));
                }

                $data_inicio = str_replace('/', '-', $request->input('horario_inicio'));
                $data_inicio_convertida = date("Y-m-d H:i:s", strtotime($data_inicio));
                $agendar->horario_inicio = $data_inicio_convertida;

                $data_final = str_replace('/', '-', $request->input('horario_final'));
                $data_final_convertida = date("Y-m-d H:i:s", strtotime($data_final));
                $agendar->horario_final = $data_final_convertida;

                $agendar->consulta_tipo = $request->input('consulta_tipo');
                if ($request->input('tipo_atendimento') != "" || $request->input('tipo_atendimento') != null) {
                    $agendar->tipo_atendimento_id = $request->input('tipo_atendimento');
                } else {
                    $agendar->tipo_atendimento_id = null;
                }
                $agendar->status = Agenda::STATUS[0];
                $agendar->user_id = auth()->user()->id;

                $agendar->save();

                return redirect()->route('agenda.index')->withStatus(__('Agendamento criado com sucesso.'));
            } else {
                return redirect()->route('agenda.index')->withErrors(__('Não foi possível cadastrar o cliente.'));
            }
        } else {
            $agendar = new Agenda();
            $agendar->cliente_id = $request['input-id'];

            if ($request->input('horario_inicio') < date('d/m/Y H:i')) {

                return redirect()->back()->withErrors(__('Não é possível agendar para uma data passada!'));
            }

            if ($request->input('horario_final') < date('d/m/Y H:i')) {

                return redirect()->back()->withErrors(__('Não é possível agendar para uma data passada!'));
            }

            $data_inicio = str_replace('/', '-', $request->input('horario_inicio'));
            $data_inicio_convertida = date("Y-m-d H:i:s", strtotime($data_inicio));
            $agendar->horario_inicio = $data_inicio_convertida;

            $data_final = str_replace('/', '-', $request->input('horario_final'));
            $data_final_convertida = date("Y-m-d H:i:s", strtotime($data_final));
            $agendar->horario_final = $data_final_convertida;

            $agendar->consulta_tipo = $request->input('consulta_tipo');
            if ($request->input('tipo_atendimento') != "") {
                $agendar->tipo_atendimento_id = $request->input('tipo_atendimento');
            } else {
                $agendar->tipo_atendimento_id = null;
            }
            $agendar->status = Agenda::STATUS[0];
            $agendar->user_id = auth()->user()->id;

            $agendar->save();

            return redirect()->route('agenda.index')->withStatus(__('Agendamento criado com sucesso.'));
        }
    }

    public function edit($id)
    {
        $agenda = Agenda::findOrFail($id);

        return $agenda;
    }

    public function update(Request $request, $id)
    {
        $agendar = Agenda::findOrfail($id);

        $dataForInicial = explode(' ',$request['horario_inicio']);
        $dataInicial = explode('/', $dataForInicial[0]);
        $dataInicial = $dataInicial[2].'-'.$dataInicial[1].'-'.$dataInicial[0] . 'T' .$dataForInicial[1];

        $dataForFinal = explode(' ',$request['horario_final']);
        $dataFinal = explode('/', $dataForFinal[0]);
        $dataFinal = $dataFinal[2].'-'.$dataFinal[1].'-'.$dataFinal[0] . 'T' .$dataForFinal[1];

        $agendar->horario_inicio = $dataInicial;
        $agendar->horario_final  = $dataFinal;
        $agendar->consulta_tipo = $request->input('consulta_tipo');
        $agendar->tipo_atendimento_id = $request->input('tipo_atendimento');
        $agendar->status = Agenda::STATUS[0];
        $agendar->user_id = auth()->user()->id;

        $agendar->save();

        return redirect()->route('agenda.index')->withStatus(__('Agendamento editado com sucesso.'));
    }
}
