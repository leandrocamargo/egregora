<?php

namespace App\Http\Controllers;

use App\Avaliacao;
use App\Cliente;
use Illuminate\Http\Request;

class AvaliacoesController extends Controller
{
    public function index(Avaliacao $model)
    {
        return view('avaliacoes.index', ['avaliacoes' => $model]);
    }

    public function create(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrfail($id);

        return view('avaliacoes.create',compact('cliente'));
    }

    public function store(Request $request)
    {
        $avaliacao = new Avaliacao();
        $avaliacao->cliente_id = $request->input('cliente_id');
        $avaliacao->data = date('y-m-d');
        $avaliacao->peso = str_replace(',','.',$request->input('peso'));
        $avaliacao->altura = str_replace(',','.',$request->input('altura'));
        $avaliacao->porc_gordura = $request->input('porc_gordura');
        $avaliacao->porc_musculo = $request->input('porc_musculo');
        $avaliacao->observacao = $request->input('observacao');
        $avaliacao->med_acima = $request->input('med_acima');
        $avaliacao->med_umbigo = $request->input('med_umbigo');
        $avaliacao->med_abaixo = $request->input('med_abaixo');
        $avaliacao->med_quadril = $request->input('med_quadril');
        $avaliacao->med_entre_coxas = $request->input('med_entre_coxas');

        $imc1 = $avaliacao->altura * $avaliacao->altura;
        $imc2 = $avaliacao->peso / $imc1;
        $avaliacao->imc = $imc2;

        $avaliacao->save();

        return redirect()->route('atender.cliente', $avaliacao->cliente_id)->withStatus(__('Avaliação criado com sucesso.'));
    }

    public function edit(Avaliacao $avaliacao_edit, $id)
    {
        $avaliacao = $avaliacao_edit->findOrFail($id);

        return view('avaliacoes.edit', compact('avaliacao'));
    }

    public function update(Request $request, $id)
    {
        $avaliacao = Avaliacao::findOrFail($id);
        $avaliacao->cliente_id = $request->input('cliente_id');
        $avaliacao->altura = $request->input('altura');
        $avaliacao->peso = $request->input('peso');
        $avaliacao->porc_gordura = $request->input('porc_gordura');
        $avaliacao->porc_musculo = $request->input('porc_musculo');
        $avaliacao->observacao = $request->input('observacao');
        $avaliacao->med_acima = $request->input('med_acima');
        $avaliacao->med_umbigo = $request->input('med_umbigo');
        $avaliacao->med_abaixo = $request->input('med_abaixo');
        $avaliacao->med_quadril = $request->input('med_quadril');
        $avaliacao->med_entre_coxas = $request->input('med_entre_coxas');

        $imc1 = $avaliacao->altura * $avaliacao->altura;
        $imc2 = $avaliacao->peso / $imc1;
        $avaliacao->imc = $imc2;

        $avaliacao->save();

        return redirect()->route('atender.cliente', $avaliacao->cliente_id)->withStatus(__('Avaliação editada com sucesso.'));
    }

    public function detalhes(Cliente $cliente_id, $id)
    {
        $cliente = $cliente_id->findOrfail($id);
        $avaliacoes = Avaliacao::where('cliente_id',$cliente->id)->get()->toArray();

        return view('avaliacoes.index', compact('cliente', 'avaliacoes', 'response'));
    }

    public function carregandoChart($id)
    {
        $cliente = Cliente::findOrfail($id);
        $avaliacoesCliente = Avaliacao::where('cliente_id',$cliente->id)->orderBy('data','DESC')->get()->toArray();

        return $avaliacoesCliente;
    }
}
