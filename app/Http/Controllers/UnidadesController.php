<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Endereco;
use App\Service\UnidadeService;
use App\Unidade;
use Illuminate\Http\Request;

class UnidadesController extends Controller
{
    private $service;

    public function __construct(UnidadeService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    public function index()
    {
        $unidades = Unidade::with('contato')->with('endereco')->paginate(10);

        return view('units.index', compact('unidades'));
    }

    public function create()
    {
        return view('units.create');
    }

    public function store(Request $request)
    {
        $this->service->create($request);

        return redirect()->route('unidade.index')->withStatus(__('Unidade criado com sucesso.'));
    }

    public function edit(Unidade $unidadeEdit, $id)
    {
        $unidade = $unidadeEdit->findOrFail($id);

        return view('units.edit', compact('unidade'));
    }

    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);

        return redirect()->route('unidade.index')->withStatus(__('Unidade editado com sucesso.'));
    }

    public function destroy(Unidade $unidade)
    {
        dd($unidade);
        $unidade->delete();

        return redirect()->route('unidade.index')->withStatus(__('Unidade deletado com sucesso.'));
    }
}
