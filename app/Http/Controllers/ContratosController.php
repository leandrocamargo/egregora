<?php

namespace App\Http\Controllers;

use App\Contrato;
use App\FormaPagamento;
use Illuminate\Http\Request;

class ContratosController extends Controller
{
    public function index(Contrato $model)
    {
        return view('contratos.index', ['contratos' => $model->orderBy('data','DESC')->paginate(10)]);
    }

    public function create()
    {
        //
    }

    public function store()
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $contrato   = Contrato::findOrFail($id);
        $pagamentos = FormaPagamento::query()->get();
        $contratos  = Contrato::with('cliente')->get();

        return view('contratos.fechar',compact('contratos','contrato','pagamentos'));
    }

    public function destroy()
    {
        //
    }
}
