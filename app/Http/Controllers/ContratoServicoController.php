<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Contrato;
use App\ContratoServico;
use App\FormaPagamento;
use App\Servico;
use App\Sessao;
use App\Unidade;
use Carbon\Carbon;
use Http\Client\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ContratoServicoController extends Controller
{
    private function sanitizeString($string) {

        // matriz de entrada
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );

        // matriz de saída
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );

        // devolver a string
        return str_replace($what, $by, $string);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    public function create()
    {
        //
    }

    public function show($id)
    {
        $arrayServico = [];
        $arrayPacote = [];
        $contrato = [];

        $contratoServicos = ContratoServico::with('servico')
            ->with('pacote')
            ->with('contrato')
            ->where('id_contrato', '=', $id)
            ->get();

        //dd($contratoServicos->toArray());

        $contrato = $contratoServicos[0]['contrato']->toArray();
        $auxId = 0;
        $index = null;
        $quantidade = 1;
        $valorTotalServicos = 0;
        $valorTotalPacotes  = 0;

        foreach ($contratoServicos as $chave => $contratoServico)
        {
            if ($contratoServico->servico != null)
            {
                if ($auxId === $contratoServico->servico->id)
                {
                    $servico = [
                        "id" => $contratoServico->servico->id,
                        "nome" => $contratoServico->servico->nome,
                        "descricao" => $contratoServico->servico->descricao,
                        "valor" => $contratoServico->valor,
                        "color" => $contratoServico->servico->color,
                        "imagem" => $contratoServico->servico->imagem,
                        "quantidade" => $quantidade++,
                        "id_user" => $contratoServico->id_user,
                        "perfil_user" => $contratoServico->perfil_user,
                    ];

                    $arrayServico[$index] = $servico;
                    $valorTotalServicos = ($valorTotalServicos + $contratoServico->valor);
                } else {
                    $quantidade = 1;
                    if($index === null) {
                      $index = 0;
                    } else {
                      $index++;
                    }
                    $auxId = $contratoServico->servico->id;
                    $servico = [
                        "id" => $contratoServico->servico->id,
                        "nome" => $contratoServico->servico->nome,
                        "descricao" => $contratoServico->servico->descricao,
                        "valor" => $contratoServico->valor,
                        "color" => $contratoServico->servico->color,
                        "imagem" => $contratoServico->servico->imagem,
                        "quantidade" => $quantidade++,
                        "id_user" => $contratoServico->id_user,
                        "perfil_user" => $contratoServico->perfil_user,
                    ];

                    $arrayServico[$index] = $servico;
                    $valorTotalServicos = ($valorTotalServicos + $contratoServico->valor);
                }
            }
            if ($contratoServico['pacote'])
            {
                //dd($contratoServico->toArray());
                $arrayPacote[]  = [
                    "pacotes" => $contratoServico['pacote']->toArray(),
                    "id_user" => $contratoServico['id_user'],
                    "perfil_user" => $contratoServico['perfil_user']
                ];
                //dd($arrayPacote);
                $valorTotalPacotes  = ($valorTotalPacotes + $contratoServico->valor);
            }
        }

        $response = [
            'contrato'             => $contrato,
            'servicos'             => $arrayServico,
            'pacotes'              => $arrayPacote,
            'valor_total_servicos' => $valorTotalServicos,
            'valor_total_pacotes'  => $valorTotalPacotes
        ];

        return $response;
    }

    public function store(Request $request)
    {
        $contrato = new Contrato();
        $contrato->data = Carbon::now();
        $contrato->id_cliente  = $request->id_cliente['id'];
        $contrato->valor_total = $request->valor_total;
        $contrato->status = Contrato::STATUS_CONTRATO[1];

        $contrato->save();

        //if ($contrato) {
            foreach ($request->input('id_servicos') as $key => $value)
            {
                for ($i=0; $i<$value['quantidade']; $i++)
                {
                    $contratoServico = new ContratoServico();
                    $contratoServico->id_contrato = $contrato->id;
                    $contratoServico->id_servico  = $value['id'];
                    $contratoServico->valor       = $value['valor'];
                    $contratoServico->id_user     = $request->input('id_user');
                    $contratoServico->perfil_user = $request->input('perfil_user');

                    $contratoServico->save();
                }
            }

            {
                $contratoServico = new ContratoServico();
                $contratoServico->id_contrato = $contrato->id;
                $contratoServico->id_pacote   = $value['id'];
                $contratoServico->valor       = $value['valor_pacote'];
                $contratoServico->id_user     = $request->input('id_user');
                $contratoServico->perfil_user = $request->input('perfil_user');

                $contratoServico->save();
            }
        //} else {
            return $contrato; //view('atendimento.atendimento');
        //} echo "ERRO!!!";
    }

    public function fecharContrato (Request $request)
    {
        //dd($request->input('pacotes')[0]['pacotes']['valor_pacote']);
        try {
            return DB::transaction(function () use ($request) {
                // atualizando contrato

                $contrato = Contrato::findOrFail($request->input('contract')['id']);

                $contrato->status             = $request->input('contract')['status'];
                $contrato->num_parcelas       = $request->input('contract')['num_parcelas'];
                $contrato->forma_pagamento_id = $request->input('contract')['forma_pagamento_id'];
                $contrato->desconto           = $request->input('contract')['desconto'];
                $contrato->bandeira           = $request->input('contract')['bandeira'];
                $contrato->observacao         = $request->input('contract')['observacao'];
                $contrato->valor_total        = $request->input('total');
                $contrato->data_fechou        = Carbon::now();
                $contrato->numero_contrato    = rand(2,1000000);

                $contrato->save();

                // atualizando contrato-serviços
                DB::table('contrato_servicos')->where('id_contrato', '=', $contrato->id)->delete();

                // salvando pacotes
                foreach ($request->input('pacotes') as $pacote) {

                    //dd($pacote['pacotes']['valor_pacote']);
                    $contratoServico = new ContratoServico();

                    $contratoServico->valor       = $pacote['pacotes']['valor_pacote'];
                    $contratoServico->id_pacote   = $pacote['pacotes']['id'];
                    $contratoServico->id_contrato = $contrato->id;
                    $contratoServico->data        = Carbon::now()->format('Ymd');
                    $contratoServico->id_user     = $request->input('id_user');
                    $contratoServico->perfil_user = $request->input('perfil_user');
                    $contratoServico->executado   = "Aberto";

                    //dd($contratoServico);

                    $contratoServico->save();

                    if($contrato->status === "Fechado") {
                        if(is_array($pacote['pacotes']['pacotes_servicos'])) {
                            foreach ($pacote['pacotes']['pacotes_servicos'] as $servicos) {
                                for($i = 0; $i < $servicos['pivot']['qtd_sessao']; $i ++) {
                                    $this->saveSessao($contratoServico, "Pacote", $servicos['id']);
                                }
                            }
                        }
                    }
                }

                // salvando servicos
                if ($request->input('servicos')) {
                foreach ($request->input('servicos') as $servico) {

                    for ($i = 0; $i < $servico['quantidade']; $i++) {

                        $contratoServico = new ContratoServico();

                        $contratoServico->valor       = $servico['valor'];
                        $contratoServico->id_servico  = $servico['id'];
                        $contratoServico->id_contrato = $contrato->id;
                        $contratoServico->data        = Carbon::now()->format('Ymd');
                        $contratoServico->executado   = "Aberto";

                       // dd($contratoServico);

                        $contratoServico->save();

                        //dd($contratoServico);

                        if($contrato->status === "Fechado") {
                            $this->saveSessao($contratoServico, "Serviço", $contratoServico->id_servico);
                        }
                    }
                }}

//                Mail::send('emails.emails-docs', array(
//                    "dados"    => $dados,
//                    "usuario"  => $usuario,
//                ), function ($message) use ($usuario, $dados){
//                    $message->from($usuario)
//
//                        ->to($dados['funcionario']->contato->email)
//                        ->subject("Novo Documento Disponível");
//                });

                return $this->contratoPdf($request);

            });
        } catch (RequestException $exception) {

            return $exception->getMessage();
        }
    }

    public function update()
    {
        //
    }

    public function edit()
    {
        //
    }

    public function destroy()
    {
        //
    }

    private function saveSessao (ContratoServico $contratoServico, $tipo, $idServico)
    {
        $sessao = new Sessao();
        $sessao->contrato_servico_id = $contratoServico->id;
        $sessao->contrato_id         = $contratoServico->id_contrato;
        $sessao->origem              = $tipo;
        $sessao->servico_id          = $idServico;
        $sessao->save();
    }

    public function contratoPdf(Request $request)
    {
        //dd($request);
        $contrato = Contrato::findOrFail($request->input('contract')['id']);
        $contrato->num_parcelas       = $request->input('contract')['num_parcelas'];
        $contrato->forma_pagamento_id = $request->input('contract')['forma_pagamento_id'];
        $contrato->desconto           = $request->input('contract')['desconto'];
        $contrato->bandeira           = $request->input('contract')['bandeira'];
        $contrato->observacao         = $request->input('contract')['observacao'];
        $contrato->valor_total        = $request->input('total');

        //dd($contrato);

        $cliente = Cliente::with('endereco')->where('id','=',$contrato->id_cliente)->first();
        $unidade = Unidade::where('id','=',auth()->user()->unidade_id)->first();
        $formaDePagamento = FormaPagamento::findOrFail($request->input('contract')['forma_pagamento_id']);
        $servicosTabela = Servico::all();

        //dd($cliente['endereco']);

        foreach ($request->input('pacotes') as $pacote) {

            $contratoServico = new ContratoServico();
            $contratoServico->valor       = $pacote['pacotes']['valor_pacote'];
            $contratoServico->id_pacote   = $pacote['pacotes']['id'];
            $contratoServico->id_contrato = $contrato->id;
            $contratoServico->data        = Carbon::now()->format('Ymd');
            $contratoServico->executado   = "Aberto";

            if($contrato->status === "Fechado") {
                if(is_array($pacote['pacotes']['pacotes_servicos'])){
                    foreach ($pacote['pacotes']['pacotes_servicos'] as $servicos) {
                        for($i = 0; $i < $servicos['pivot']['qtd_sessao']; $i ++) {
                            $this->saveSessao($contratoServico, "Pacote", $servicos['id']);
                        }
                    }
                }
            }
        }

        foreach ($request->input('servicos') as $servico) {

            //dd($servico);

            for ($i = 0; $i < $servico['quantidade']; $i++) {

                $contratoServico = new ContratoServico();
                $contratoServico->valor       = $servico['valor'];
                $contratoServico->id_servico  = $servico['id'];
                $contratoServico->id_contrato = $contrato->id;
                $contratoServico->data        = Carbon::now()->format('Ymd');
                $contratoServico->executado   = "Aberto";
            }
        }

        //dd($request['pacotes']);
        if ($contrato->status === "Fechado"){
            $pdf = Pdf::loadView('contratos.pdf', compact('contrato',
                'contratoServico',
                'cliente',
                'unidade',
                'request',
                'formaDePagamento',
                'servicosTabela'
            ), [],
                [
                    'format' => 'A4',
                    'margin_left' => 10,
                    'margin_right' => 10,
                    'margin_top' => 10,
                    'margin_bottom' => 10,
                ])->save(public_path('/uploads/contrato/'. strtolower($this->sanitizeString($cliente->nome)) . $contrato->data.".pdf"));
        } else {
            $pdf = Pdf::loadView('orcamentos.pdf', compact(
                'contrato',
                'contratoServico',
                'cliente',
                'unidade',
                'request',
                'formaDePagamento',
                'servicosTabela'
            ),[],
                [
                    'format' => 'A4',
                    'margin_left' => 10,
                    'margin_right' => 10,
                    'margin_top' => 10,
                    'margin_bottom' => 10,
                ])->save(public_path('/uploads/orcamento/'. strtolower($this->sanitizeString($cliente->nome)) . $contrato->data.".pdf"));
        }
    }
}
