<?php

namespace App\Http\Controllers;

use App\ContratoServico;
use Illuminate\Http\Request;

class IndicacoesController extends Controller
{
    public function index()
    {
        $indicacoes = ContratoServico::with('usuario')
            ->with('contrato')
            ->with('pacote')
            ->with('servico')
            ->paginate(10);

        //dd($indicacoes);

        return view('indicacoes.index', ['indicacoes' => $indicacoes]);
    }

    public function store()
    {

    }
}
