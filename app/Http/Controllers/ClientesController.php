<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Contato;
use App\Endereco;
use App\Service\ClienteService;
use App\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClientesController extends Controller
{
    private $service;

    public function __construct(ClienteService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Display a listing of the users
     *
     *
     * @return \Illuminate\View\View
     */

    public function index()
    {
        $clientes = Cliente::with('endereco')->with('contato')->orderBy('id', 'DESC')->paginate(10);

        return view('clients.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('clients.create');
    }

//    Salvar um novo cliente
    public function store(Request $request)
    {
        $this->service->create($request);

        return redirect()->route('cliente.index')->withStatus(__('Cliente criado com sucesso.'));
    }

//    Chama tela para editar carregando os dados
    public function edit(Cliente $clienteEdit, $id)
    {
        $cliente = $clienteEdit->findOrFail($id);

        return view('clients.edit', compact('cliente'));
    }

//    Salva cliente editado
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);

        return redirect()->route('cliente.index')->withStatus(__('Cliente editado com sucesso.'));
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();

        return redirect()->route('cliente.index')->withStatus(__('Cliente deletado com sucesso.'));
    }

    public function getCliente($id)
    {
        return Cliente::findOrFail($id);
    }

    public function getClienteAutoComplete()
    {
        return Cliente::with('contato')->get();
    }
}
