<?php


namespace App\Http\Controllers;


use App\FormaPagamento;
use Illuminate\Http\Request;

class FormasPagamentosController extends Controller
{
    public function index(FormaPagamento $model)
    {
        return view('formas-pagamentos.index', ['formasPagamentos' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('formas-pagamentos.create');
    }

    public function store(Request $request)
    {
        $forma_pagamento = new FormaPagamento();
        $forma_pagamento->nome = $request->input('nome');
        $forma_pagamento->observacao = $request->input('observacao');

        if ($request->input('bandeira') != "" || $request->input('bandeira') != null) {
            $forma_pagamento->bandeira = 1;
        } else {
            $forma_pagamento->bandeira = null;
        }

        if ($request->input('parcela') != "" || $request->input('parcela') != null) {
            $forma_pagamento->parcela = 1;
        } else {
            $forma_pagamento->parcela = null;
        }

        $forma_pagamento->save();

        return redirect()->route('forma_pagamento.index')->withStatus(__('Forma de Pagamento criada com sucesso.'));
    }

    public function edit(FormaPagamento $forma_pagamento_edit, $id)
    {
        $forma_pagamento = $forma_pagamento_edit->findOrFail($id);

        return view('formas-pagamentos.edit', compact('forma_pagamento'));
    }

    public function update(Request $request, $id)
    {
        $forma_pagamento = FormaPagamento::findOrFail($id);
        $forma_pagamento->nome = $request->input('nome');
        $forma_pagamento->observacao = $request->input('observacao');

        if ($request->input('bandeira') != "" || $request->input('bandeira') != null) {
            $forma_pagamento->bandeira = 1;
        } else {
            $forma_pagamento->bandeira = null;
        }

        if ($request->input('parcela') != "" || $request->input('parcela') != null) {
            $forma_pagamento->parcela = 1;
        } else {
            $forma_pagamento->parcela = null;
        }

        $forma_pagamento->save();

        return redirect()->route('forma_pagamento.index')->withStatus(__('Forma de Pagamento editada com sucesso.'));
    }

    public function destroy(FormaPagamento $forma_pagamento)
    {
        $forma_pagamento->delete();

        return redirect()->route('forma_pagamento.index')->withStatus(__('Forma de Pagamento deletada com sucesso.'));
    }

    public function formaPagamento()
    {
        $formapagamento = FormaPagamento::all();

        return $formapagamento;
    }
}
