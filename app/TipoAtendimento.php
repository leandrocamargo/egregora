<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class TipoAtendimento extends Model
{
    use SoftDeletes;
    protected $table = "tipo_atendimento";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "id",
        "descricao",
        "color"
    ];

    const COLOR = [
        'red'    => 'Vermelho',
        'blue'   => 'Azul',
        'green'  => 'Verde',
        'purple' => 'Roxo',
        'coral'  => 'Coral',
        'gray'   => 'Cinza',
        'orange' => 'Laranja',
        'brown'  => 'Marrom',
        'violet' => 'Violeta',
        'navy'   => 'Azul Escuro',
        'yellow' => 'Amarelo'
    ];
}
