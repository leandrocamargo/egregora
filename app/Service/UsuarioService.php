<?php

namespace App\Service;

use App\Contato;
use App\Endereco;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsuarioService
{
    public function create(Request $request)
    {
        DB::transaction(function () use ($request): User {

            $endereco = Endereco::enderecoSave($request);
            $contato  = Contato::contatoSave($request);

            return User::usuarioSave($request, $endereco, $contato);
        });
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {

            $usuario  = User::findOrFail($id);
            $endereco = Endereco::enderecoUpdate($request, $usuario->endereco_id);
            $contato  = Contato::contatoUpdate($request, $usuario->contato_id);

            return User::usuarioUpdate($request, $id);
        });
    }

    public function getUser()
    {
        return Auth::user();
    }
}
