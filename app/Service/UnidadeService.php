<?php

namespace App\Service;

use App\Contato;
use App\Endereco;
use App\Unidade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnidadeService
{
    public function create(Request $request)
    {
        DB::transaction(function () use ($request): Unidade {

            $endereco = Endereco::enderecoSave($request);
            $contato  = Contato::contatoSave($request,'comercial');

            return Unidade::unidadeSave($request, $endereco, $contato);
        });
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {

            $unidade  = Unidade::findOrFail($id);
            $endereco = Endereco::enderecoUpdate($request, $unidade->endereco_id);
            $contato  = Contato::contatoUpdate($request, $unidade->contato_id,'comercial');

            return Unidade::unidadeUpdate($request, $id);
        });
    }
}