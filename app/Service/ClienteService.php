<?php

namespace App\Service;

use App\Cliente;
use App\Contato;
use App\Endereco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteService
{
    public function create(Request $request)
    {
        DB::transaction(function () use ($request): Cliente {

            $endereco = Endereco::enderecoSave($request);
            $contato  = Contato::contatoSave($request);

            return Cliente::clienteSave($request, $endereco, $contato);
        });
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {

            $cliente  = Cliente::findOrFail($id);

            if (isset($cliente->endereco_id)) {
                $endereco = Endereco::enderecoUpdate($request, $cliente->endereco_id);
            } else {
                $endereco = Endereco::enderecoSave($request);

                $cliente->endereco_id = $endereco->id;
            }

            $contato  = Contato::contatoUpdate($request, $cliente->contato_id);

            return Cliente::clienteUpdate($request, $id, $endereco, $contato);
        });
    }
}
