<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Avaliacao extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "avaliacao";

    protected $fillable = [
        'cliente_id',
        'data',
        'peso',
        'porc_gordura',
        'porc_musculo',
        'imc',
        'observacao',
        'med_acima',
        'med_umbigo',
        'med_abaixo',
        'med_quadril',
        'med_entre_coxas'
    ];

    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id','cliente_id')->withTrashed();
    }
}
