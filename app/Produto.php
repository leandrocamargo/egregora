<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Produto extends Model
{
    use SoftDeletes;
    protected $table = "produtos";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "nome",
        "descricao",
        "preco",
        "estoque",
        "imagem"
    ];
}
