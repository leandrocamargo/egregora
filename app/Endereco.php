<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Endereco extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'enderecos';

    protected $fillable = [
        'logradouro',
        'numero',
        'bairro',
        'complemento',
        'cep',
        'cidade',
        'estado'
    ];

    public static function enderecoSave (Request $request): Endereco
    {
        $endereco = new Endereco();

        $endereco->logradouro      =  $request->input('logradouro');
        $endereco->numero          =  $request->input('numero');
        $endereco->bairro          =  $request->input('bairro');
        $endereco->complemento     =  $request->input('complemento');
        $endereco->cep             =  $request->input('cep');
        $endereco->estado          =  $request->input('estado');
        $endereco->cidade          =  $request->input('cidade');

        $endereco->save();

        return $endereco;
    }

    public static function enderecoUpdate (Request $request, $endereco_id): Endereco
    {
        $endereco = Endereco::findOrFail($endereco_id);

        $endereco->logradouro      = $request->input('logradouro');
        $endereco->numero          = $request->input('numero');
        $endereco->bairro          = $request->input('bairro');
        $endereco->complemento     = $request->input('complemento');
        $endereco->cep             = $request->input('cep');
        $endereco->estado          = $request->input('estado');
        $endereco->cidade          = $request->input('cidade');

        $endereco->save();

        return $endereco;
    }
}
