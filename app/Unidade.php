<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Unidade extends Model
{
    use SoftDeletes;
    protected $table = 'unidades';
    protected $dates = ['deleted_at'];

    protected $fillable = array(
        'endereco_id',
        'contato_id',
        'nome',
        'descricao'
    );

    public static function unidadeSave (Request $request, Endereco $endereco, Contato $contato): Unidade
    {
        $unidade = new Unidade();
        $unidade->nome        = $request->input('nome');
        $unidade->descricao   = $request->input('descricao');
        $unidade->endereco_id = $endereco->id;
        $unidade->contato_id  = $contato->id;

        $unidade->save();

        return $unidade;
    }

    public static function unidadeUpdate (Request $request,$id)
    {
        $unidade = Unidade::findOrfail($id);
        $unidade->nome      = $request->input('nome');
        $unidade->descricao = $request->input('descricao');

        $unidade->save();

        return view('units.index');
    }

    public function usuarios()
    {
        return $this->hasManyThrough('App\Usuarios', 'App\Agenda', 'unidade', 'unidade_id')->withTrashed();
    }

    public function contato()
    {
        return $this->hasOne('App\Contato','id', 'contato_id')->withTrashed();
    }

    public function endereco()
    {
        return $this->hasOne('App\Endereco','id', 'endereco_id')->withTrashed();
    }

    public function validator($data)
    {
        $rules = array(
            'nome' => 'required',
            'descricao' => 'required'
        );

        $messages = array(
            'nome.required'=> 'O campo nome é obrigatório',
            'descricao.required'=> 'O campo descrição é obrigatório',
        );

        $validator = Validator::make($data, $rules, $messages);

        return $validator;
    }
}
