<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControleCheque extends Model
{
    use SoftDeletes;
    protected $table = "controle_cheque";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "cliente_id",
        "contrato_id",
        "parcelas",
        "vencimento",
        "status"
    ];
}
