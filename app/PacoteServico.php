<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class PacoteServico extends Model
{
    use SoftDeletes;
    protected $table = "pacotes_servicos";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "id_pacote",
        "id_servico",
        "qtd_sessao",
        "valor_sessao"
    ];

    public function gravarPacServ($dados)
    {
        $pacServ = $this->create($dados);

        if($pacServ) {

            return $pacServ->id;
        } else {
            return ['error' => 'Erro'];
        }
    }

    public function servico()
    {
        return $this->belongsToMany('App\Servico', "servicos", "id_servico")->withTrashed();
    }

    public function pacote()
    {
        return $this->belongsTo('App\Pacote', "id_pacote", "id")->withTrashed();
    }
}
