<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Indicacao extends Model
{
    use SoftDeletes;
    protected $table = "indicacoes";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "user_id",
        "contrato_id",
        "cliente_id",
        "servico_id",
        "pacote_id",
        "valor",
        "data_indicacao",
        "status",
        "pagamento"
    ];

    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function contrato()
    {
        return $this->belongsTo('App\Contrato', 'contrato_id', 'id');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente','cliente_id', 'id');
    }

    public function servico()
    {
        return $this->belongsTo('App\Servico', 'servico_id', 'id');
    }

    public function pacote()
    {
        return $this->belongsTo('App\Pacote', 'pacote_id', 'id');
    }
}
