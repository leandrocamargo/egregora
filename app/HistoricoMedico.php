<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class HistoricoMedico extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'historico_medico';

    protected $fillable = [
        'id_cliente',
        'trat_med_atual',
        'trat_med_atual_medic',
        'antec_alergico',
        'antec_alergico_qual',
        'port_marcapasso',
        'alt_cardiacas',
        'alt_pres_arterial',
        'dis_circulatorio',
        'dis_circulatorio_qual',
        'dis_renal',
        'dis_renal_qual',
        'dis_hormonal',
        'dis_hormonal_qual',
        'dis_gastro',
        'dis_gastro_qual',
        'epilepsia_convulsao',
        'epilepsia_convulsao_freq',
        'labirintite',
        'alteracao_psi',
        'alteracao_psi_qual',
        'estresse',
        'estresse_obs',
        'ant_oncologicos',
        'ant_oncologicos_qual',
        'diabetes',
        'diabetes_tipo',
        'outra_doenca'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente', 'cliente_id', 'id')->withTrashed();
    }
}
