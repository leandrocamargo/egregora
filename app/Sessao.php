<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessao extends Model
{
    protected $table = "sessoes";
    protected $fillable = [
        "origem",
        "id",
        "data_execussao",
        "status"
    ];

    public function contrato()
    {
        return $this->belongsTo(Contrato::class,'contrato_id')->withTrashed();
    }

    public function contratoServico()
    {
        return $this->belongsTo(ContratoServico::class,'contrato_servico_id')->withTrashed();
    }

    public function profissional()
    {
        return $this->belongsTo(User::class,'profissional_id')->withTrashed();
    }

    public function servico()
    {
        return $this->belongsTo(Servico::class,'servico_id')->withTrashed();
    }
}
