<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Pacote extends Model
{
    use SoftDeletes;
    protected $table = "pacotes";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "nome_pacote",
        "descricao",
        "valor_pacote",
        "bruto",
        "desconto",
        "imagem"
    ];

    public function pacoteserv()
    {
        return $this->hasMany('App\PacoteServico', "id_pacote", "id")->withTrashed();
    }

    public  function pacotesServicos()
    {
        return $this->belongsToMany(Servico::class,'pacotes_servicos', 'pacote_id','servico_id')->withPivot('valor_sessao','qtd_sessao','id','deleted_at')->withTrashed();
    }

    public function validator($data)
    {
        $rules = array(
            'nome_pacote' => 'required',
            'descricao' => 'required',
            'valor_pacote' => 'required',
            'bruto' => 'required',
        );
        $messages = array(
            'nome_pacote.required'=> 'O campo nome é obrigatório',
            'descricao.required'=> 'O campo descrição é obrigatório',
            'valor_pacote.required'=> 'O campo valor é obrigatório',
            'bruto.required'=> 'O campo bruto é obrigatório',
        );
        $validator = Validator::make($data, $rules, $messages);

        return $validator;
    }
}
