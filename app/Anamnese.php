<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Anamnese extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'anamneses';

    protected $fillable = [
        'cliente_id',
        'trat_estetic_anterior',
        'trat_estetic_anterior_qual',
        'utiliz_cosmeticos',
        'utiliz_cosmeticos_qual',
        'exp_sol',
        'uso_filtro',
        'tabagismo',
        'bebida_alcoolica',
        'func_intestinal',
        'func_intestinal_obs',
        'qualidade_sono',
        'qualidade_sono_obs',
        'ingest_agua',
        'alimentacao',
        'alimentos_pref',
        'prat_atv_fisica',
        'prat_atv_fisica_freq',
        'uso_anticoncepcional',
        'gestacao',
        'gestacao_qtde',
        'gravida'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente', 'cliente_id', 'id')->withTrashed();
    }
}
