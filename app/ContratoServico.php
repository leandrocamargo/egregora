<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class ContratoServico extends Model
{
    use SoftDeletes;
    protected $table = "contrato_servicos";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "id_contrato",
        "id_servico",
        "id_pacote",
        "id_user",
        "perfil_user",
        "valor",
        "executado",
        "data",
    ];

    public function contrato()
    {
        return $this->belongsTo(Contrato::class,'id_contrato')->with('cliente')->withTrashed();
    }

    public function pacote()
    {
        return $this->belongsTo(Pacote::class,'id_pacote')->with('pacotesServicos')->withTrashed();
    }

    public function servico()
    {
        return $this->belongsTo(Servico::class, 'id_servico')->orderBy('id')->withTrashed();
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'id_user')->withTrashed();
    }

    public function validator($data)
    {
        $rules = array (
            'id_contrato' => 'required',
            'id_servico' => 'required',
            'id_pacote' => 'required',
            'valor' => 'required',
        );

        $messages = array(
            'id_contrato.required'=> 'Este campo é obrigatório',
            'id_servico.required'=> 'Este campo é obrigatório',
            'id_pacote.required'=> 'Este campo é obrigatório',
            'valor.required'=> 'O campo valor é obrigatório',
        );

        $validator = Validator::make($data, $rules, $messages);

        return $validator;
    }
}
