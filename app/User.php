<?php

namespace App;

use Artesaos\Defender\Facades\Defender;
use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasDefender;

    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    const TIPO = [
        'MASTER',
        'ADMIN',
        'PROFISSIONAL',
        'CLIENTE',
        'ATENDIMENTO'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function usuarioSave (Request $request, Endereco $endereco, Contato $contato): User
    {
        $usuario = new User();
        $usuario->name        = $request->input('name');
        $usuario->email       = $request->input('email');
        $usuario->password    = bcrypt($request->input('password'));
        $usuario->tipo        = $request->input('tipo_usuario');
        $usuario->unidade_id  = $request->input('unidade');
        $usuario->endereco_id = $endereco->id;
        $usuario->contato_id  = $contato->id;

        $usuario->save();

        $usuario->attachRole(Defender::findRole($usuario->tipo));

        return $usuario;
    }

    public static function usuarioUpdate (Request $request,$id)
    {
        $usuario = User::findOrfail($id);
        $role = Defender::findRole($usuario->tipo);
        $usuario->detachRole($role);
        $usuario->name        = $request->input('name');
        $usuario->email       = $request->input('email');
        $usuario->tipo        = $request->input('tipo_usuario');
        $usuario->unidade_id  = $request->input('unidade');
        $usuario->attachRole(Defender::findRole($usuario->tipo));
        if ($request->input('password') == null || $request->input('password') == "" ){

        }else {
            $usuario->password = Hash::make($request->input('password'));
        }

        $usuario->password = bcrypt($request->input('password'));

        $usuario->save();

        return view('users.index');
    }

    public function contato()
    {
        return $this->hasOne(Contato::class, 'id','contato_id')->withTrashed();
    }

    public function endereco()
    {
        return $this->hasOne(Endereco::class, 'id','endereco_id')->withTrashed();
    }

    public function unidade()
    {
        return $this->hasOne(Unidade::class,'id','unidade_id')->withTrashed();
    }
}
