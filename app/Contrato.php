<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Contrato extends Model
{
    use SoftDeletes;
    protected $table = "contrato";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "data",
        "num_parcelas",
        "valor_total",
        "desconto",
        "bandeira",
        "valor_parcela",
        "observacao",
        "status"
    ];

    const STATUS_CONTRATO = ['Fechado','Aberto','Análise','Cancelado','Expirado'];
    const BANDEIRA = ['Visa','Mastercard','Cielo','American Express'];

    public function cliente()
    {
        return $this->hasOne(Cliente::class,'id','id_cliente')->with('unidade')->withTrashed();
    }

    public function formaPagamento()
    {
        return $this->hasOne(FormaPagamento::class,'id', 'forma_pagamento_id')->withTrashed();
    }

    public function formaPagamentobyName($nome)
    {
        return $this->hasOne(FormaPagamento::class,'id', 'forma_pagamento_id')->where('nome',$nome)->withTrashed();
    }
}
