<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Contato extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'contatos';

    protected $fillable = [
        'telefone',
        'email',
        'celular',
        'tipo',
        'ativo'
    ];

    public static function contatoSave(Request $request, $tipo=""): Contato
    {
        $contato = new Contato();
        $contato->telefone = $request->input('telefone');
        $contato->email    = $request->input('email');
        $contato->celular  = $request->input('celular');
        $contato->tipo     = $tipo == "" ? $request->input('tipo') : $tipo;
        $contato->ativo    = true;

        $contato->save();

        return $contato;
    }

    public static function contatoUpdate(Request $request, $id_contato, $tipo=""): Contato
    {
        $contato = Contato::findOrFail($id_contato);
        $contato->telefone = $request->input('telefone');
        $contato->email    = $request->input('email');
        $contato->celular  = $request->input('celular');
        $contato->tipo     = $tipo == "" ? $request->input('tipo') : $tipo;

        $contato->save();

        return $contato;
    }
}
