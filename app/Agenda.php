<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agenda extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'agenda';
    protected $fillable = [
        'cliente_id',
        'tipo_atendimento_id',
        'horario_inicio',
        'horario_final',
        'status',
        'consulta_tipo',
    ];

    const CONSULTA_TIPO = ['Avaliação','Avaliação+Sessão','Sessão','Retorno'];
    const STATUS = ['Agendado','Aguardando','Cancelado','Não Compareceu'];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente','cliente_id', 'id')
            ->with('contato')
            ->with('endereco')
            ->with('unidade');
    }

    public function servico()
    {
        return $this->belongsTo('App\TipoAtendimento', 'tipo_atendimento_id', 'id')->withTrashed();
    }

    public function toArray()
    {
        return [
            "id"             => (int)$this->id,
            'horario_inicio' => $this->horario_inicio,
            'horario_final'  => $this->horario_final,
            'status'         => $this->status,
            'consulta_tipo'  => $this->consulta_tipo,
            'cliente_id'     => $this->cliente->toArray(),
            'tipo_atendimento_id'     => $this->servico ? $this->servico->toArray() : null,
        ];
    }
}
