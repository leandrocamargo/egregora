CREATE DATABASE  IF NOT EXISTS `neo_egregora` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `neo_egregora`;
-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: neo_egregora
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.19.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned DEFAULT NULL,
  `tipo_atendimento_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `horario_inicio` datetime NOT NULL,
  `horario_final` datetime NOT NULL,
  `status` enum('Agendado','Aguardando','Cancelado','Final') COLLATE utf8mb4_unicode_ci NOT NULL,
  `consulta_tipo` enum('Avaliação+Sessão','Pre-Consulta','Retorno','Avaliacao','Sessão') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agendamento_cliente_id_foreign` (`cliente_id`),
  KEY `agendamento_tipo_atendimento_id_foreign` (`tipo_atendimento_id`),
  KEY `agendamento_user_id_foreign` (`user_id`),
  CONSTRAINT `agendamento_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `agendamento_tipo_atendimento_id_foreign` FOREIGN KEY (`tipo_atendimento_id`) REFERENCES `tipo_atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `agendamento_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
INSERT INTO `agenda` VALUES (1,11,1,1,'2019-10-23 11:00:00','2019-10-23 12:00:00','Agendado','Avaliação+Sessão',NULL,'2019-10-23 16:00:47','2019-10-23 16:00:47'),(2,12,1,1,'2019-10-16 14:00:00','2019-10-16 15:00:00','Agendado','Sessão',NULL,'2019-10-23 17:46:07','2019-10-23 17:46:07'),(3,13,1,1,'2019-10-23 09:00:00','2019-10-23 10:00:00','Agendado','Avaliacao',NULL,'2019-10-23 18:04:31','2019-10-23 18:04:31'),(4,14,1,1,'2019-10-16 17:00:00','2019-10-16 18:00:00','Agendado','Avaliacao',NULL,'2019-10-23 18:15:20','2019-10-23 18:15:20');
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anamneses`
--

DROP TABLE IF EXISTS `anamneses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anamneses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `trat_estetic_anterior` tinyint(1) NOT NULL,
  `trat_estetic_anterior_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utiliz_cosmeticos` tinyint(1) NOT NULL,
  `utiliz_cosmeticos_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_sol` tinyint(1) NOT NULL,
  `uso_filtro` tinyint(1) NOT NULL,
  `tabagismo` tinyint(1) NOT NULL,
  `bebida_alcoolica` enum('Nunca','Raro','Sempre') COLLATE utf8mb4_unicode_ci NOT NULL,
  `func_intestinal` enum('Reg','Irreg') COLLATE utf8mb4_unicode_ci NOT NULL,
  `func_intestinal_obs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualidade_sono` enum('Boa','Reg','Pess') COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualidade_sono_obs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingest_agua` enum('menos2','mais2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `alimentacao` enum('Boa','Reg','Pess') COLLATE utf8mb4_unicode_ci NOT NULL,
  `alimentos_pref` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prat_atv_fisica` tinyint(1) NOT NULL,
  `prat_atv_fisica_freq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uso_anticoncepcional` tinyint(1) NOT NULL,
  `gestacao` tinyint(1) NOT NULL,
  `gestacao_qtde` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gravida` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `anamneses_cliente_id_foreign` (`cliente_id`),
  CONSTRAINT `anamneses_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anamneses`
--

LOCK TABLES `anamneses` WRITE;
/*!40000 ALTER TABLE `anamneses` DISABLE KEYS */;
INSERT INTO `anamneses` VALUES (1,12,0,'2',0,'3',0,0,0,'Raro','Irreg','4','Reg','5','mais2','Reg','6',0,'7',0,0,'8','0',NULL,'2019-10-28 18:53:08','2019-10-28 20:49:58'),(2,12,0,'2',0,'3',0,0,0,'Raro','Irreg','4','Reg','5','mais2','Reg','6',0,'7',0,0,'8','0',NULL,'2019-10-28 20:44:56','2019-10-28 20:44:56'),(3,12,0,'2',0,'3',0,0,0,'Raro','Irreg','4','Reg','5','mais2','Reg','6',0,'7',0,0,'8','0',NULL,'2019-10-28 20:47:27','2019-10-28 20:47:27'),(4,11,1,'1',1,'2',1,1,1,'Nunca','Irreg','1','Boa','1','menos2','Boa','1',1,'1',1,1,'1','1',NULL,'2019-10-29 21:37:08','2019-10-29 21:37:08');
/*!40000 ALTER TABLE `anamneses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliacao`
--

DROP TABLE IF EXISTS `avaliacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `peso` double(8,2) NOT NULL,
  `altura` double(8,2) NOT NULL,
  `porc_gordura` double(8,2) NOT NULL,
  `porc_musculo` double(8,2) NOT NULL,
  `imc` double(8,2) NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `med_acima` double(8,2) NOT NULL,
  `med_umbigo` double(8,2) NOT NULL,
  `med_abaixo` double(8,2) NOT NULL,
  `med_quadril` double(8,2) NOT NULL,
  `med_entre_coxas` double(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avaliacao_cliente_id_foreign` (`cliente_id`),
  CONSTRAINT `avaliacao_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacao`
--

LOCK TABLES `avaliacao` WRITE;
/*!40000 ALTER TABLE `avaliacao` DISABLE KEYS */;
INSERT INTO `avaliacao` VALUES (1,12,'2019-10-29',2.00,2.00,2.00,2.00,2.00,'2',2.00,2.00,2.00,2.00,2.00,NULL,'2019-10-29 16:57:02','2019-10-29 17:07:53'),(2,12,'2019-10-29',7.00,7.00,7.00,7.00,7.00,'7',7.00,7.00,7.00,7.00,7.00,NULL,'2019-10-29 23:03:17','2019-10-29 23:03:17'),(3,12,'2019-10-29',77.00,77.00,77.00,77.00,77.00,'77',77.00,77.00,77.00,77.00,77.00,NULL,'2019-10-29 23:28:01','2019-10-29 23:28:01'),(4,11,'2019-10-29',777.00,777.00,777.00,777.00,777.00,'777',777.00,777.00,777.00,777.00,777.00,NULL,'2019-10-29 23:34:35','2019-10-29 23:34:35');
/*!40000 ALTER TABLE `avaliacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_nasc` date DEFAULT NULL,
  `profissao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado_civil` enum('Solteiro(a)','Casado(a)','Viuvo(a)','Divorciado(a)') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endereco_id` int(10) unsigned DEFAULT NULL,
  `unidade_id` int(10) unsigned NOT NULL,
  `contato_id` int(10) unsigned NOT NULL,
  `ativo` tinyint(1) DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clientes_cpf_unique` (`cpf`),
  KEY `clientes_unidade_id_foreign` (`unidade_id`),
  KEY `clientes_contato_id_foreign` (`contato_id`),
  KEY `clientes_endereco_id_foreign` (`endereco_id`),
  CONSTRAINT `clientes_contato_id_foreign` FOREIGN KEY (`contato_id`) REFERENCES `contatos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clientes_endereco_id_foreign` FOREIGN KEY (`endereco_id`) REFERENCES `enderecos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clientes_unidade_id_foreign` FOREIGN KEY (`unidade_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Maria Silva Cardoso','123.456.789-11','1987-05-02','Lar','Casado(a)',5,1,5,NULL,NULL,'2019-10-11 21:57:11','2019-10-11 23:28:54'),(3,'Andreia Santos','456.987.321-55','1972-08-05','Lar','Casado(a)',7,1,7,NULL,NULL,'2019-10-11 22:01:20','2019-10-11 22:01:20'),(4,'Teresa','698.523.697-41','1971-05-09','Lar','Casado(a)',8,1,8,NULL,'2019-10-11 23:34:11','2019-10-11 22:02:50','2019-10-11 23:34:11'),(6,'Teresa','123.654.123-78',NULL,NULL,NULL,NULL,1,56,1,'2019-11-18 23:48:39','2019-10-23 15:56:28','2019-11-18 23:48:39'),(9,'Teresa','159.357.852-65',NULL,NULL,NULL,NULL,1,59,1,NULL,'2019-10-23 15:58:21','2019-10-23 15:58:21'),(11,'Teresa','357.654.258-12',NULL,NULL,NULL,NULL,1,61,1,NULL,'2019-10-23 16:00:47','2019-10-23 16:00:47'),(12,'Betina','111.111.111-11',NULL,NULL,NULL,NULL,1,62,1,NULL,'2019-10-23 17:46:07','2019-10-23 17:46:07'),(13,'Jorgita','999.999.999-99',NULL,NULL,NULL,NULL,1,63,1,NULL,'2019-10-23 18:04:31','2019-10-23 18:04:31'),(14,'Joana','777.777.777-77',NULL,NULL,NULL,NULL,1,64,1,NULL,'2019-10-23 18:15:20','2019-10-23 18:15:20');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
INSERT INTO `contatos` VALUES (1,'(86) 5649-1342','user@user.com.br','(15) 4 5979-3339',NULL,1,NULL,'2019-10-11 15:55:14','2019-10-14 21:51:14'),(5,'(11) 1111-1111','maria@maria.com','(11) 1 1111-1111',NULL,1,NULL,'2019-10-11 21:57:11','2019-10-11 21:57:11'),(7,'(11) 1111-1111','deia@email.com.br','(11) 1 1111-1111',NULL,1,NULL,'2019-10-11 22:01:20','2019-10-11 22:01:20'),(8,'(11) 1111-1111','maria@maria.com','(11) 1 1111-1111',NULL,1,NULL,'2019-10-11 22:02:50','2019-10-11 22:02:50'),(35,'(11) 1111-1111','deia@email.com.br','(11) 1 1111-1111',NULL,1,NULL,'2019-10-14 19:43:25','2019-10-14 19:43:25'),(45,'(11) 1111-1111','deia123@email.com.br','(11) 1 1111-1111',NULL,1,NULL,'2019-10-14 19:54:45','2019-10-14 19:54:45'),(46,'(77) 7777-7777','deia7775@email.com.br','(77) 7 7777-7777',NULL,1,NULL,'2019-10-14 19:57:04','2019-10-14 22:33:18'),(48,'(11) 7777-7777','deia@email.com','(11) 9 7777-7777',NULL,1,NULL,'2019-10-14 23:15:20','2019-10-14 23:15:20'),(52,'(11) 7777-7777','tere@email.com','(11) 9 7777-7777',NULL,1,NULL,'2019-10-14 23:30:27','2019-10-14 23:30:27'),(53,'(11) 7777-7777','mama@email','(11) 7 9999-9999',NULL,1,NULL,'2019-10-14 23:36:44','2019-10-14 23:36:44'),(54,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:50:17','2019-10-23 15:50:17'),(55,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:55:17','2019-10-23 15:55:17'),(56,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:56:28','2019-10-23 15:56:28'),(57,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:56:33','2019-10-23 15:56:33'),(58,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:57:21','2019-10-23 15:57:21'),(59,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 15:58:21','2019-10-23 15:58:21'),(60,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 16:00:00','2019-10-23 16:00:00'),(61,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 16:00:47','2019-10-23 16:00:47'),(62,'(11) 1111-1111',NULL,'(11) 9 1111-1111',NULL,NULL,NULL,'2019-10-23 17:46:07','2019-10-23 17:46:07'),(63,'(11) 1111-1111',NULL,'(11) 1 1111-1111',NULL,NULL,NULL,'2019-10-23 18:04:31','2019-10-23 18:04:31'),(64,'(11) 1111-1111',NULL,'(11) 1 1111-1111',NULL,NULL,NULL,'2019-10-23 18:15:20','2019-10-23 18:15:20');
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `desconto` decimal(8,2) DEFAULT '0.00',
  `forma_pagamento_id` int(10) unsigned DEFAULT NULL,
  `bandeira` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_parcelas` int(11) DEFAULT '1',
  `valor_total` double(8,2) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contrato_id_cliente_foreign` (`id_cliente`),
  KEY `contrato_forma_pagamento_id_foreign` (`forma_pagamento_id`),
  CONSTRAINT `contrato_forma_pagamento_id_foreign` FOREIGN KEY (`forma_pagamento_id`) REFERENCES `forma_pagamento` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `contrato_id_cliente_foreign` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` VALUES (3,1,'2019-10-23',0.00,1,NULL,1,1000.00,NULL,NULL,NULL,'2019-10-29 14:14:00',NULL),(4,12,'2019-11-19',0.00,NULL,NULL,1,0.00,'Aberto',NULL,NULL,'2019-11-19 23:29:07','2019-11-19 23:29:07'),(5,12,'2019-11-19',0.00,1,NULL,1,2500.00,'Fechado',NULL,NULL,'2019-11-19 23:29:14','2019-11-21 23:49:36'),(6,11,'2019-11-21',0.00,NULL,NULL,1,2500.00,'Aberto',NULL,NULL,'2019-11-21 19:52:27','2019-11-21 19:52:27'),(7,11,'2019-11-21',0.00,NULL,NULL,1,2500.00,'Aberto',NULL,NULL,'2019-11-21 19:54:07','2019-11-21 19:54:07'),(8,11,'2019-11-21',0.00,NULL,NULL,1,2500.00,'Aberto',NULL,NULL,'2019-11-21 19:54:56','2019-11-21 19:54:56'),(9,11,'2019-11-21',0.00,1,NULL,1,2500.00,'Fechado',NULL,NULL,'2019-11-21 19:55:51','2019-11-22 17:27:32'),(10,11,'2019-11-21',0.00,1,NULL,1,2500.00,'Fechado',NULL,NULL,'2019-11-21 20:01:02','2019-11-22 17:24:44'),(11,11,'2019-11-21',0.00,1,NULL,1,2500.00,'Fechado',NULL,NULL,'2019-11-21 20:06:36','2019-11-22 17:15:48'),(12,12,'2019-11-22',0.00,4,'Visa',10,2850.00,'Fechado','Teste',NULL,'2019-11-22 15:52:03','2019-11-22 16:26:41'),(13,13,'2019-11-22',0.00,NULL,NULL,1,2550.00,'Aberto',NULL,NULL,'2019-11-22 17:39:48','2019-11-22 17:39:48'),(14,14,'2019-11-22',0.00,NULL,NULL,1,3500.00,'Aberto',NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48');
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato_servicos`
--

DROP TABLE IF EXISTS `contrato_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato_servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_contrato` int(10) unsigned NOT NULL,
  `id_servico` int(10) unsigned DEFAULT NULL,
  `id_pacote` int(10) unsigned DEFAULT NULL,
  `valor` double(8,2) NOT NULL,
  `executado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contrato_servicos_id_contrato_foreign` (`id_contrato`),
  KEY `contrato_servicos_id_servico_foreign` (`id_servico`),
  KEY `contrato_servicos_id_pacote_foreign` (`id_pacote`),
  CONSTRAINT `contrato_servicos_id_contrato_foreign` FOREIGN KEY (`id_contrato`) REFERENCES `contrato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contrato_servicos_id_pacote_foreign` FOREIGN KEY (`id_pacote`) REFERENCES `pacotes` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `contrato_servicos_id_servico_foreign` FOREIGN KEY (`id_servico`) REFERENCES `servicos` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato_servicos`
--

LOCK TABLES `contrato_servicos` WRITE;
/*!40000 ALTER TABLE `contrato_servicos` DISABLE KEYS */;
INSERT INTO `contrato_servicos` VALUES (2,6,NULL,25,2500.00,NULL,NULL,NULL,'2019-11-21 19:52:27','2019-11-21 19:52:27'),(3,7,NULL,25,2500.00,NULL,NULL,NULL,'2019-11-21 19:54:07','2019-11-21 19:54:07'),(4,8,NULL,25,2500.00,NULL,NULL,NULL,'2019-11-21 19:54:56','2019-11-21 19:54:56'),(21,5,NULL,25,2500.00,'Aberto','2019-11-21',NULL,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(27,12,NULL,25,2500.00,'Aberto','2019-11-22',NULL,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(28,12,3,NULL,100.00,'Aberto','2019-11-22',NULL,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(29,12,4,NULL,70.00,'Aberto','2019-11-22',NULL,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(30,12,5,NULL,50.00,'Aberto','2019-11-22',NULL,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(31,12,6,NULL,130.00,'Aberto','2019-11-22',NULL,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(33,11,NULL,25,2500.00,'Aberto','2019-11-22',NULL,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(37,10,NULL,25,2500.00,'Aberto','2019-11-22',NULL,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(38,9,NULL,25,2500.00,'Aberto','2019-11-22',NULL,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(39,13,5,NULL,50.00,NULL,NULL,NULL,'2019-11-22 17:39:48','2019-11-22 17:39:48'),(40,13,NULL,25,2500.00,NULL,NULL,NULL,'2019-11-22 17:39:48','2019-11-22 17:39:48'),(41,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(42,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(43,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(44,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(45,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(46,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(47,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(48,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(49,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(50,14,3,NULL,100.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48'),(51,14,NULL,25,2500.00,NULL,NULL,NULL,'2019-11-22 17:40:48','2019-11-22 17:40:48');
/*!40000 ALTER TABLE `contrato_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controle_cheque`
--

DROP TABLE IF EXISTS `controle_cheque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controle_cheque` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned DEFAULT NULL,
  `contrato_id` int(10) unsigned DEFAULT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cheque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vencimento` datetime NOT NULL,
  `parcelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('EM DIA','COMPENSADO','EM ATRASO','DEVOLVIDO','CANCELADO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `controle_cheque_cliente_id_foreign` (`cliente_id`),
  KEY `controle_cheque_contrato_id_foreign` (`contrato_id`),
  CONSTRAINT `controle_cheque_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `controle_cheque_contrato_id_foreign` FOREIGN KEY (`contrato_id`) REFERENCES `contrato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controle_cheque`
--

LOCK TABLES `controle_cheque` WRITE;
/*!40000 ALTER TABLE `controle_cheque` DISABLE KEYS */;
INSERT INTO `controle_cheque` VALUES (3,1,3,'Bradesco','123','2019-10-28 00:00:00','1/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(4,1,3,'Bradesco','124','2019-11-28 00:00:00','2/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(5,1,3,'Bradesco','125','2019-12-28 00:00:00','3/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(6,1,3,'Bradesco','126','2020-01-28 00:00:00','4/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(7,1,3,'Bradesco','127','2020-02-28 00:00:00','5/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(8,1,3,'Bradesco','128','2020-03-28 00:00:00','6/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(9,1,3,'Bradesco','129','2020-04-28 00:00:00','7/7','EM DIA',NULL,'2019-10-23 16:16:03','2019-10-23 16:16:03'),(10,1,3,'Santander','37','2019-11-07 00:00:00','1/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(11,1,3,'Santander','38','2019-12-07 00:00:00','2/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(12,1,3,'Santander','39','2020-01-07 00:00:00','3/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(13,1,3,'Santander','40','2020-02-07 00:00:00','4/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(14,1,3,'Santander','41','2020-03-07 00:00:00','5/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(15,1,3,'Santander','42','2020-04-07 00:00:00','6/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45'),(16,1,3,'Santander','43','2020-05-07 00:00:00','7/7','EM DIA',NULL,'2019-10-23 16:16:45','2019-10-23 16:16:45');
/*!40000 ALTER TABLE `controle_cheque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enderecos`
--

DROP TABLE IF EXISTS `enderecos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enderecos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logradouro` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complemento` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cep` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cidade` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enderecos`
--

LOCK TABLES `enderecos` WRITE;
/*!40000 ALTER TABLE `enderecos` DISABLE KEYS */;
INSERT INTO `enderecos` VALUES (1,'53486 Naomi Plain Apt. 385',2545,'Merlintown','1','05050-230','East Olaf','SP',NULL,'2019-10-11 15:55:14','2019-10-14 21:51:14'),(5,'Praça Vinte e Dois de Novembro',77,'Centro','1','09310-100','Mauá','SP',NULL,'2019-10-11 21:57:11','2019-10-11 21:57:11'),(7,'Avenida Barão de Mauá',77,'Vila Bocaina','1','09310-000','Mauá','SP',NULL,'2019-10-11 22:01:20','2019-10-11 22:01:20'),(8,'Avenida Portugal',77,'Jardim Pilar','1','09370-000','Mauá','SP',NULL,'2019-10-11 22:02:50','2019-10-11 22:02:50'),(35,'Avenida Barão de Mauá',77,'Vila Bocaina','1','09310-000','Mauá','SP',NULL,'2019-10-14 19:43:25','2019-10-14 19:43:25'),(45,'Avenida Barão de Mauá',777,'Vila Bocaina','1','09310-000','Mauá','SP',NULL,'2019-10-14 19:54:45','2019-10-14 19:54:45'),(46,'Rua Monte Castelo',775,'Vila Guarani','2','09310-150','Mauá','SP',NULL,'2019-10-14 19:57:04','2019-10-14 22:33:18'),(48,'Rua Monte Castelo',77,'Vila Guarani','1','09310-150','Mauá','SP',NULL,'2019-10-14 23:15:20','2019-10-14 23:15:20'),(52,'Avenida Portugal',77,'Jardim Pilar','1','09370-000','Mauá','SP',NULL,'2019-10-14 23:30:27','2019-10-14 23:30:27'),(53,'Rua Rio Branco',77,'Vila Augusto','7','09310-110','Mauá','SP',NULL,'2019-10-14 23:36:44','2019-10-14 23:36:44');
/*!40000 ALTER TABLE `enderecos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma_pagamento`
--

DROP TABLE IF EXISTS `forma_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma_pagamento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bandeira` tinyint(1) DEFAULT '0',
  `parcela` tinyint(1) DEFAULT '0',
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma_pagamento`
--

LOCK TABLES `forma_pagamento` WRITE;
/*!40000 ALTER TABLE `forma_pagamento` DISABLE KEYS */;
INSERT INTO `forma_pagamento` VALUES (1,'Dinheiro',NULL,NULL,'Dinheiro',NULL,'2019-10-23 16:13:35','2019-10-23 16:44:36'),(2,'Débito',NULL,NULL,'Débito','2019-10-23 16:42:59','2019-10-23 16:39:29','2019-10-23 16:42:59'),(3,'Débito',1,NULL,'Débito',NULL,'2019-10-23 16:42:04','2019-10-23 16:44:14'),(4,'Cartão',1,1,'Cartão',NULL,'2019-10-23 16:43:15','2019-10-23 16:43:15');
/*!40000 ALTER TABLE `forma_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historico_medico`
--

DROP TABLE IF EXISTS `historico_medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico_medico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `trat_med_atual` tinyint(1) NOT NULL,
  `trat_med_atual_medic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `antec_alergico` tinyint(1) NOT NULL,
  `antec_alergico_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port_marcapasso` tinyint(1) NOT NULL,
  `alt_cardiacas` tinyint(1) NOT NULL,
  `alt_pres_arterial` tinyint(1) NOT NULL,
  `dis_circulatorio` tinyint(1) NOT NULL,
  `dis_circulatorio_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_renal` tinyint(1) NOT NULL,
  `dis_renal_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_hormonal` tinyint(1) NOT NULL,
  `dis_hormonal_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_gastro` tinyint(1) NOT NULL,
  `dis_gastro_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `epilepsia_convulsao` tinyint(1) NOT NULL,
  `epilepsia_convulsao_freq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labirintite` tinyint(1) NOT NULL,
  `alteracao_psi` tinyint(1) NOT NULL,
  `alteracao_psi_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estresse` tinyint(1) NOT NULL,
  `estresse_obs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ant_oncologicos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ant_oncologicos_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diabetes` tinyint(1) NOT NULL,
  `diabetes_tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outra_doenca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `historico_medico_cliente_id_foreign` (`cliente_id`),
  CONSTRAINT `historico_medico_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historico_medico`
--

LOCK TABLES `historico_medico` WRITE;
/*!40000 ALTER TABLE `historico_medico` DISABLE KEYS */;
INSERT INTO `historico_medico` VALUES (1,12,0,'2',0,'2',0,0,0,0,'2',0,'2',0,'2',0,'2',0,'2',0,0,'2',0,'2','0','2',0,'2','2',NULL,'2019-10-28 22:16:15','2019-10-28 23:29:56');
/*!40000 ALTER TABLE `historico_medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2019_02_08_161729_create_endereco_table',1),(3,'2019_02_08_161806_create_contatos_table',1),(4,'2019_02_08_161832_create_pacotes_table',1),(5,'2019_02_08_161849_create_produtos_table',1),(6,'2019_02_08_161909_create_forma_pagamento_table',1),(7,'2019_02_08_162204_create_servicos_table',1),(8,'2019_02_08_171227_create_unidades_table',1),(9,'2019_02_08_171237_create_clientes_table',1),(10,'2019_02_08_171238_create_tratamentos_esteticos_table',1),(11,'2019_02_08_171616_create_contrato_table',1),(12,'2019_02_08_172213_create_avaliacao_table',1),(13,'2019_02_08_172523_create_anamneses_table',1),(14,'2019_02_08_173020_create_historico_medico_table',1),(15,'2019_03_08_161909_create_pacotes_servicos_table',1),(16,'2019_03_08_162043_create_contrato_servicos_table',1),(17,'2019_09_05_122721_create_tipo_atendimento_table',1),(18,'2019_10_12_000000_create_users_table',1),(19,'2019_10_13_161101_create_defender_roles_table',1),(20,'2019_10_13_161102_create_defender_permissions_table',1),(21,'2019_10_13_161103_create_defender_role_user_table',1),(22,'2019_10_13_161104_create_defender_permission_user_table',1),(23,'2019_10_13_161105_create_defender_permission_role_table',1),(24,'2019_11_03_021636_create_agendamento_table',1),(25,'2020_01_09_135230_create_sessoes_table',1),(26,'2019_10_22_180827_create_controle_cheque_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacotes`
--

DROP TABLE IF EXISTS `pacotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_pacote` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_pacote` double(8,2) NOT NULL,
  `desconto` decimal(8,2) DEFAULT NULL,
  `bruto` double NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacotes`
--

LOCK TABLES `pacotes` WRITE;
/*!40000 ALTER TABLE `pacotes` DISABLE KEYS */;
INSERT INTO `pacotes` VALUES (1,'gfgf','gfgf',990.00,10.00,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-01 21:25:40','2019-11-01 20:51:31','2019-11-01 21:25:40'),(2,'ewewe','ewewe',923.00,77.00,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-01 21:27:31','2019-11-01 20:52:28','2019-11-01 21:27:31'),(3,'rteste','teste',3000.00,700.00,3700,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-01 21:27:35','2019-11-01 21:09:46','2019-11-01 21:27:35'),(4,'rere','rerer',990.00,10.00,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-01 21:27:38','2019-11-01 21:10:30','2019-11-01 21:27:38'),(5,'e','wew',990.00,10.00,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 17:22:09','2019-11-01 21:12:07','2019-11-04 17:22:09'),(6,'teste','teste',3000.00,700.00,3700,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 17:22:07','2019-11-01 21:13:53','2019-11-04 17:22:07'),(7,'QQQ','QQQ',-1210.00,10.00,-1200,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 17:22:05','2019-11-01 21:30:23','2019-11-04 17:22:05'),(8,'teste','teste',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 21:48:51','2019-11-04 17:22:29','2019-11-04 21:48:51'),(9,'errer','rere',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 21:48:49','2019-11-04 21:38:29','2019-11-04 21:48:49'),(10,'rteste','teste',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 22:03:46','2019-11-04 21:49:09','2019-11-04 22:03:46'),(11,'rere','rere',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 22:41:23','2019-11-04 22:40:32','2019-11-04 22:41:23'),(12,'rere','rere',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 22:41:21','2019-11-04 22:41:04','2019-11-04 22:41:21'),(13,'rere','rere',2700.00,NULL,2700,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 23:04:36','2019-11-04 22:41:51','2019-11-04 23:04:36'),(14,'rere','rerer',1700.00,NULL,1700,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-04 23:24:05','2019-11-04 23:08:54','2019-11-04 23:24:05'),(15,'Teste','teste',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/medida.png','2019-11-12 16:27:48','2019-11-04 23:26:04','2019-11-12 16:27:48'),(16,'teste','teste',1000.00,NULL,1000,'http://127.0.0.1:8000/storage/pacote/teste.jpg','2019-11-12 16:27:50','2019-11-08 23:22:18','2019-11-12 16:27:50'),(17,'Egreg Lipo','Egreg Lipo',5000.00,NULL,5000,'http://127.0.0.1:8080/storage/pacote/bg.png','2019-11-18 20:47:39','2019-11-12 16:28:25','2019-11-18 20:47:39'),(18,'Teste edição','Teste edição',7000.00,NULL,7000,'http://127.0.0.1:8080/storage/pacote/bg.png','2019-11-18 20:47:40','2019-11-18 20:25:32','2019-11-18 20:47:40'),(19,'rtrer','rere',0.00,0.00,0,'http://127.0.0.1:8080/storage/pacote/bg.png','2019-11-18 21:14:05','2019-11-18 21:10:17','2019-11-18 21:14:05'),(20,'rerer','rerer',3000.00,500.00,3500,'http://127.0.0.1:8080/storage/pacote/bg.png','2019-11-18 21:17:22','2019-11-18 21:15:06','2019-11-18 21:17:22'),(21,'rerer','rerer',2000.00,500.00,2500,'http://127.0.0.1:8080/storage/pacote/bg.png','2019-11-18 21:18:40','2019-11-18 21:17:58','2019-11-18 21:18:40'),(22,'rerere','rerere',3000.00,500.00,3500,'http://127.0.0.1:8000/storage/pacote/bg.png','2019-11-18 21:36:20','2019-11-18 21:19:09','2019-11-18 21:36:20'),(23,'rerere','rerere',2600.00,NULL,2600,'http://127.0.0.1:8000/storage/pacote/bg.png','2019-11-18 21:36:18','2019-11-18 21:19:50','2019-11-18 21:36:18'),(24,'Teste','Teste',3000.00,500.00,3500,'http://127.0.0.1:8000/storage/pacote/bg.png','2019-11-18 21:36:16','2019-11-18 21:23:26','2019-11-18 21:36:16'),(25,'Teste de Edição','Teste de Edição',2500.00,260.00,2760,'http://127.0.0.1:8000/storage/pacote/bg.png',NULL,'2019-11-18 21:36:58','2019-11-18 21:44:25');
/*!40000 ALTER TABLE `pacotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacotes_servicos`
--

DROP TABLE IF EXISTS `pacotes_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacotes_servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pacote_id` int(10) unsigned NOT NULL,
  `servico_id` int(10) unsigned NOT NULL,
  `qtd_sessao` int(11) NOT NULL,
  `valor_sessao` double(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pacotes_servicos_pacote_id_foreign` (`pacote_id`),
  KEY `pacotes_servicos_servico_id_foreign` (`servico_id`),
  CONSTRAINT `pacotes_servicos_pacote_id_foreign` FOREIGN KEY (`pacote_id`) REFERENCES `pacotes` (`id`),
  CONSTRAINT `pacotes_servicos_servico_id_foreign` FOREIGN KEY (`servico_id`) REFERENCES `servicos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacotes_servicos`
--

LOCK TABLES `pacotes_servicos` WRITE;
/*!40000 ALTER TABLE `pacotes_servicos` DISABLE KEYS */;
INSERT INTO `pacotes_servicos` VALUES (1,1,1,10,100.00,NULL,NULL,NULL),(2,2,1,10,100.00,NULL,NULL,NULL),(3,5,1,10,100.00,NULL,NULL,NULL),(4,6,1,10,100.00,NULL,NULL,NULL),(5,6,1,7,100.00,NULL,NULL,NULL),(6,6,1,20,100.00,NULL,NULL,NULL),(7,7,1,1,100.00,NULL,NULL,'2019-11-04 17:22:01'),(8,5,1,9,100.00,NULL,NULL,NULL),(9,5,1,10,100.00,NULL,NULL,'2019-11-04 17:20:35'),(10,7,1,1,100.00,NULL,NULL,'2019-11-04 17:22:01'),(11,7,1,1,100.00,NULL,NULL,'2019-11-04 17:22:01'),(12,8,1,10,100.00,NULL,NULL,NULL),(13,8,1,7,100.00,NULL,NULL,NULL),(14,8,1,10,100.00,NULL,NULL,NULL),(15,9,1,10,100.00,NULL,NULL,NULL),(16,9,1,7,100.00,NULL,NULL,NULL),(17,9,1,7,100.00,NULL,NULL,NULL),(18,9,1,10,100.00,NULL,NULL,NULL),(19,10,1,10,100.00,NULL,NULL,NULL),(20,10,1,7,100.00,NULL,NULL,NULL),(21,10,1,10,100.00,NULL,NULL,NULL),(22,11,1,10,100.00,NULL,NULL,NULL),(23,12,1,10,100.00,NULL,NULL,NULL),(24,13,1,10,100.00,NULL,NULL,NULL),(25,14,1,10,100.00,NULL,NULL,NULL),(26,14,1,7,100.00,NULL,NULL,NULL),(27,15,1,10,100.00,'2019-11-12 16:27:36',NULL,'2019-11-12 16:27:36'),(28,15,1,10,100.00,NULL,NULL,'2019-11-04 23:40:41'),(29,15,1,10,100.00,NULL,NULL,'2019-11-04 23:40:41'),(30,16,1,10,100.00,NULL,NULL,NULL),(31,17,1,10,100.00,'2019-11-12 16:29:00',NULL,'2019-11-12 16:29:00'),(32,17,2,10,120.00,'2019-11-12 16:29:07',NULL,'2019-11-12 16:29:07'),(33,17,3,5,100.00,'2019-11-12 16:28:38',NULL,'2019-11-12 16:28:38'),(34,17,3,10,100.00,'2019-11-18 20:41:51',NULL,'2019-11-18 20:41:51'),(35,17,1,20,100.00,'2019-11-18 20:41:51',NULL,'2019-11-18 20:41:51'),(36,18,1,10,100.00,NULL,NULL,NULL),(37,18,1,10,100.00,NULL,NULL,NULL),(38,18,1,10,100.00,NULL,NULL,NULL),(39,18,1,10,100.00,NULL,NULL,NULL),(40,18,1,10,100.00,NULL,NULL,NULL),(41,18,1,10,100.00,NULL,NULL,NULL),(42,18,1,10,100.00,NULL,NULL,NULL),(43,18,1,10,100.00,NULL,NULL,NULL),(44,18,1,10,100.00,NULL,NULL,NULL),(45,18,1,10,100.00,NULL,NULL,NULL),(46,18,1,10,100.00,NULL,NULL,NULL),(47,18,1,10,100.00,NULL,NULL,NULL),(48,18,1,10,100.00,NULL,NULL,NULL),(49,18,1,10,100.00,NULL,NULL,NULL),(50,17,1,10,100.00,'2019-11-18 20:41:51',NULL,'2019-11-18 20:41:51'),(51,17,1,10,100.00,'2019-11-18 20:41:51',NULL,'2019-11-18 20:41:51'),(52,17,1,10,100.00,'2019-11-18 20:41:51',NULL,'2019-11-18 20:41:51'),(53,17,1,30,100.00,NULL,NULL,'2019-11-18 20:47:21'),(54,17,1,10,100.00,NULL,NULL,NULL),(55,19,1,10,100.00,'2019-11-18 21:11:28',NULL,'2019-11-18 21:11:28'),(56,19,1,10,100.00,'2019-11-18 21:11:28',NULL,'2019-11-18 21:11:28'),(57,19,1,10,100.00,'2019-11-18 21:11:28',NULL,'2019-11-18 21:11:28'),(58,19,1,10,100.00,'2019-11-18 21:11:28',NULL,'2019-11-18 21:11:28'),(59,19,1,5,100.00,'2019-11-18 21:14:01',NULL,'2019-11-18 21:14:01'),(60,19,1,5,100.00,'2019-11-18 21:14:01',NULL,'2019-11-18 21:14:01'),(61,19,1,10,100.00,'2019-11-18 21:14:01',NULL,'2019-11-18 21:14:01'),(62,19,1,10,100.00,NULL,NULL,NULL),(63,19,1,5,100.00,NULL,NULL,'2019-11-18 21:13:10'),(64,20,1,10,100.00,'2019-11-18 21:16:39',NULL,'2019-11-18 21:16:39'),(65,20,1,5,100.00,'2019-11-18 21:16:39',NULL,'2019-11-18 21:16:39'),(66,20,1,5,100.00,'2019-11-18 21:16:39',NULL,'2019-11-18 21:16:39'),(67,20,1,5,100.00,'2019-11-18 21:16:39',NULL,'2019-11-18 21:16:39'),(68,20,1,5,100.00,NULL,NULL,NULL),(69,20,1,10,100.00,NULL,NULL,NULL),(70,20,1,10,100.00,NULL,NULL,NULL),(71,20,1,5,100.00,NULL,NULL,NULL),(72,21,1,10,100.00,'2019-11-18 21:18:16',NULL,'2019-11-18 21:18:16'),(73,21,1,10,100.00,NULL,NULL,NULL),(74,21,1,5,100.00,NULL,NULL,'2019-11-18 21:18:16'),(75,21,1,5,100.00,NULL,NULL,NULL),(76,22,1,10,100.00,NULL,NULL,NULL),(77,22,1,10,100.00,NULL,NULL,NULL),(78,22,1,10,100.00,NULL,NULL,NULL),(79,22,1,5,100.00,NULL,NULL,NULL),(80,23,1,5,100.00,NULL,NULL,'2019-11-18 21:21:11'),(81,23,2,5,120.00,NULL,NULL,'2019-11-18 21:20:48'),(82,23,3,5,100.00,'2019-11-18 21:21:55',NULL,'2019-11-18 21:21:55'),(83,23,1,10,100.00,NULL,NULL,NULL),(84,23,3,10,100.00,NULL,NULL,NULL),(85,24,1,7,100.00,'2019-11-18 21:31:49',NULL,'2019-11-18 21:31:49'),(86,24,2,5,120.00,'2019-11-18 21:28:00',NULL,'2019-11-18 21:28:00'),(87,24,3,10,100.00,NULL,NULL,'2019-11-18 21:31:49'),(88,24,1,5,100.00,NULL,NULL,'2019-11-18 21:27:03'),(89,24,2,5,120.00,NULL,NULL,'2019-11-18 21:31:49'),(90,24,2,10,120.00,NULL,NULL,NULL),(91,25,1,5,100.00,'2019-11-18 21:38:38',NULL,'2019-11-18 21:38:38'),(92,25,2,5,120.00,NULL,NULL,'2019-11-18 21:37:22'),(93,25,3,5,100.00,'2019-11-18 21:38:38',NULL,'2019-11-18 21:38:38'),(94,25,4,5,70.00,NULL,NULL,'2019-11-18 21:39:32'),(95,25,5,5,50.00,NULL,NULL,'2019-11-18 21:39:32'),(96,25,1,5,100.00,'2019-11-18 21:44:25',NULL,'2019-11-18 21:44:25'),(97,25,3,5,100.00,'2019-11-18 21:40:35',NULL,'2019-11-18 21:40:35'),(98,25,6,12,130.00,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pacotes_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '-1',
  `expires` timestamp NULL DEFAULT NULL,
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `user_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '-1',
  `expires` timestamp NULL DEFAULT NULL,
  KEY `permission_user_user_id_index` (`user_id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `readable_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preco` double(8,2) NOT NULL,
  `estoque` int(11) NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (1,'Oleo','Massagem',100.00,10,NULL,NULL,'2019-10-23 19:02:32','2019-10-23 19:02:32');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_user_user_id_index` (`user_id`),
  KEY `role_user_role_id_index` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(19,2),(20,2),(12,2),(2,2),(2,1),(20,5),(20,4),(20,1),(22,1),(22,2),(22,4),(26,5),(27,5);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'MASTER',NULL,NULL),(2,'ADMIN',NULL,NULL),(3,'RECEPCAO',NULL,NULL),(4,'CLIENTE',NULL,NULL),(5,'PROFISSIONAL',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'Massagem','Massagem','100',NULL,NULL,NULL,NULL,'2019-10-31 21:49:56','2019-10-31 21:49:56'),(2,'Egreg Lipo','Egreg Lipo','120',NULL,NULL,NULL,NULL,'2019-11-08 23:34:39','2019-11-08 23:34:39'),(3,'Clareamento','teste','100',NULL,NULL,'http://127.0.0.1:8000/storage/servico/bg.png',NULL,'2019-11-12 16:25:38','2019-11-18 21:34:55'),(4,'Limpeza de Pele','Limpeza de Pele','70',NULL,NULL,'http://127.0.0.1:8000/storage/servico/bg.png',NULL,'2019-11-18 21:35:17','2019-11-18 21:35:17'),(5,'Detox','Detox','50',NULL,NULL,'http://127.0.0.1:8000/storage/servico/bg.png',NULL,'2019-11-18 21:35:37','2019-11-18 21:35:37'),(6,'Hidratação','Hidratação','130',NULL,NULL,'http://127.0.0.1:8000/storage/servico/bg.png',NULL,'2019-11-18 21:36:06','2019-11-18 21:36:06');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessoes`
--

DROP TABLE IF EXISTS `sessoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profissional_id` int(10) unsigned DEFAULT NULL,
  `contrato_id` int(10) unsigned DEFAULT NULL,
  `contrato_servico_id` int(10) unsigned DEFAULT NULL,
  `servico_id` int(10) unsigned DEFAULT NULL,
  `data_execussao` date DEFAULT NULL,
  `origem` enum('Serviço','Pacote') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sessoes_profissional_id_foreign` (`profissional_id`),
  KEY `sessoes_contrato_id_foreign` (`contrato_id`),
  KEY `sessoes_contrato_servico_id_foreign` (`contrato_servico_id`),
  KEY `sessoes_servico_id_foreign` (`servico_id`),
  CONSTRAINT `sessoes_contrato_id_foreign` FOREIGN KEY (`contrato_id`) REFERENCES `contrato` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sessoes_contrato_servico_id_foreign` FOREIGN KEY (`contrato_servico_id`) REFERENCES `contrato_servicos` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sessoes_profissional_id_foreign` FOREIGN KEY (`profissional_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sessoes_servico_id_foreign` FOREIGN KEY (`servico_id`) REFERENCES `servicos` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1462 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessoes`
--

LOCK TABLES `sessoes` WRITE;
/*!40000 ALTER TABLE `sessoes` DISABLE KEYS */;
INSERT INTO `sessoes` VALUES (612,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(613,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(614,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(615,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(616,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(617,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(618,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(619,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(620,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(621,NULL,5,21,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(622,NULL,5,21,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(623,NULL,5,21,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(624,NULL,5,21,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(625,NULL,5,21,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(626,NULL,5,21,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(627,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(628,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(629,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(630,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(631,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(632,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(633,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(634,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(635,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(636,NULL,5,21,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(637,NULL,5,21,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(638,NULL,5,21,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(639,NULL,5,21,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(640,NULL,5,21,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(641,NULL,5,21,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(642,NULL,5,21,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(643,NULL,5,21,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(644,NULL,5,21,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(645,NULL,5,21,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(646,NULL,5,21,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(647,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(648,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(649,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(650,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(651,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(652,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(653,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(654,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(655,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(656,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(657,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(658,NULL,5,21,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(659,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(660,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(661,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(662,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(663,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(664,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(665,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(666,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(667,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(668,NULL,5,NULL,1,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(669,NULL,5,NULL,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(670,NULL,5,NULL,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(671,NULL,5,NULL,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(672,NULL,5,NULL,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(673,NULL,5,NULL,2,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(674,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(675,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(676,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(677,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(678,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(679,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(680,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(681,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(682,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(683,NULL,5,NULL,3,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(684,NULL,5,NULL,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(685,NULL,5,NULL,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(686,NULL,5,NULL,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(687,NULL,5,NULL,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(688,NULL,5,NULL,4,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(689,NULL,5,NULL,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(690,NULL,5,NULL,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(691,NULL,5,NULL,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(692,NULL,5,NULL,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(693,NULL,5,NULL,5,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(694,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(695,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(696,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(697,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(698,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(699,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(700,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(701,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(702,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(703,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(704,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(705,NULL,5,NULL,6,NULL,'Pacote',0,'2019-11-21 23:49:36','2019-11-21 23:49:36'),(706,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(707,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(708,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(709,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(710,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(711,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(712,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(713,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(714,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(715,NULL,12,27,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(716,NULL,12,27,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(717,NULL,12,27,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(718,NULL,12,27,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(719,NULL,12,27,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(720,NULL,12,27,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(721,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(722,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(723,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(724,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(725,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(726,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(727,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(728,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(729,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(730,NULL,12,27,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(731,NULL,12,27,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(732,NULL,12,27,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(733,NULL,12,27,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(734,NULL,12,27,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(735,NULL,12,27,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(736,NULL,12,27,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(737,NULL,12,27,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(738,NULL,12,27,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(739,NULL,12,27,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(740,NULL,12,27,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(741,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(742,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(743,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(744,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(745,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(746,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(747,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(748,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(749,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(750,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(751,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(752,NULL,12,27,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(753,NULL,12,28,3,NULL,'Serviço',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(754,NULL,12,29,4,NULL,'Serviço',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(755,NULL,12,30,5,NULL,'Serviço',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(756,NULL,12,31,6,NULL,'Serviço',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(757,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(758,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(759,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(760,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(761,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(762,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(763,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(764,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(765,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(766,NULL,12,NULL,1,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(767,NULL,12,NULL,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(768,NULL,12,NULL,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(769,NULL,12,NULL,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(770,NULL,12,NULL,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(771,NULL,12,NULL,2,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(772,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(773,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(774,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(775,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(776,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(777,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(778,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(779,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(780,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(781,NULL,12,NULL,3,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(782,NULL,12,NULL,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(783,NULL,12,NULL,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(784,NULL,12,NULL,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(785,NULL,12,NULL,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(786,NULL,12,NULL,4,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(787,NULL,12,NULL,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(788,NULL,12,NULL,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(789,NULL,12,NULL,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(790,NULL,12,NULL,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(791,NULL,12,NULL,5,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(792,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(793,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(794,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(795,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(796,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(797,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(798,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(799,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(800,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(801,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(802,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(803,NULL,12,NULL,6,NULL,'Pacote',0,'2019-11-22 16:26:41','2019-11-22 16:26:41'),(898,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(899,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(900,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(901,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(902,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(903,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(904,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(905,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(906,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(907,NULL,11,33,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(908,NULL,11,33,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(909,NULL,11,33,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(910,NULL,11,33,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(911,NULL,11,33,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(912,NULL,11,33,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(913,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(914,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(915,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(916,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(917,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(918,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(919,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(920,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(921,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(922,NULL,11,33,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(923,NULL,11,33,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(924,NULL,11,33,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(925,NULL,11,33,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(926,NULL,11,33,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(927,NULL,11,33,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(928,NULL,11,33,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(929,NULL,11,33,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(930,NULL,11,33,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(931,NULL,11,33,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(932,NULL,11,33,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(933,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(934,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(935,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(936,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(937,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(938,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(939,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(940,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(941,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(942,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(943,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(944,NULL,11,33,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(945,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(946,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(947,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(948,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(949,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(950,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(951,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(952,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(953,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(954,NULL,11,NULL,1,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(955,NULL,11,NULL,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(956,NULL,11,NULL,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(957,NULL,11,NULL,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(958,NULL,11,NULL,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(959,NULL,11,NULL,2,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(960,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(961,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(962,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(963,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(964,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(965,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(966,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(967,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(968,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(969,NULL,11,NULL,3,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(970,NULL,11,NULL,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(971,NULL,11,NULL,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(972,NULL,11,NULL,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(973,NULL,11,NULL,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(974,NULL,11,NULL,4,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(975,NULL,11,NULL,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(976,NULL,11,NULL,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(977,NULL,11,NULL,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(978,NULL,11,NULL,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(979,NULL,11,NULL,5,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(980,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(981,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(982,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(983,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(984,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(985,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(986,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(987,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(988,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(989,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(990,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(991,NULL,11,NULL,6,NULL,'Pacote',0,'2019-11-22 17:15:48','2019-11-22 17:15:48'),(1274,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1275,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1276,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1277,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1278,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1279,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1280,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1281,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1282,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1283,NULL,10,37,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1284,NULL,10,37,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1285,NULL,10,37,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1286,NULL,10,37,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1287,NULL,10,37,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1288,NULL,10,37,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1289,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1290,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1291,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1292,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1293,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1294,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1295,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1296,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1297,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1298,NULL,10,37,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1299,NULL,10,37,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1300,NULL,10,37,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1301,NULL,10,37,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1302,NULL,10,37,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1303,NULL,10,37,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1304,NULL,10,37,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1305,NULL,10,37,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1306,NULL,10,37,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1307,NULL,10,37,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1308,NULL,10,37,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1309,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1310,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1311,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1312,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1313,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1314,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1315,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1316,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1317,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1318,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1319,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1320,NULL,10,37,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1321,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1322,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1323,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1324,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1325,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1326,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1327,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1328,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1329,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1330,NULL,10,NULL,1,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1331,NULL,10,NULL,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1332,NULL,10,NULL,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1333,NULL,10,NULL,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1334,NULL,10,NULL,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1335,NULL,10,NULL,2,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1336,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1337,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1338,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1339,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1340,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1341,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1342,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1343,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1344,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1345,NULL,10,NULL,3,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1346,NULL,10,NULL,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1347,NULL,10,NULL,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1348,NULL,10,NULL,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1349,NULL,10,NULL,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1350,NULL,10,NULL,4,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1351,NULL,10,NULL,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1352,NULL,10,NULL,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1353,NULL,10,NULL,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1354,NULL,10,NULL,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1355,NULL,10,NULL,5,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1356,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1357,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1358,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1359,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1360,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1361,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1362,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1363,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1364,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1365,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1366,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1367,NULL,10,NULL,6,NULL,'Pacote',0,'2019-11-22 17:24:44','2019-11-22 17:24:44'),(1368,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1369,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1370,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1371,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1372,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1373,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1374,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1375,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1376,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1377,NULL,9,38,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1378,NULL,9,38,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1379,NULL,9,38,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1380,NULL,9,38,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1381,NULL,9,38,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1382,NULL,9,38,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1383,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1384,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1385,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1386,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1387,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1388,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1389,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1390,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1391,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1392,NULL,9,38,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1393,NULL,9,38,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1394,NULL,9,38,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1395,NULL,9,38,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1396,NULL,9,38,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1397,NULL,9,38,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1398,NULL,9,38,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1399,NULL,9,38,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1400,NULL,9,38,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1401,NULL,9,38,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1402,NULL,9,38,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1403,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1404,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1405,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1406,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1407,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1408,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1409,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1410,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1411,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1412,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1413,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1414,NULL,9,38,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1415,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1416,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1417,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1418,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1419,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1420,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1421,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1422,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1423,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1424,NULL,9,NULL,1,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1425,NULL,9,NULL,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1426,NULL,9,NULL,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1427,NULL,9,NULL,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1428,NULL,9,NULL,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1429,NULL,9,NULL,2,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1430,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1431,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1432,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1433,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1434,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1435,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1436,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1437,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1438,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1439,NULL,9,NULL,3,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1440,NULL,9,NULL,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1441,NULL,9,NULL,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1442,NULL,9,NULL,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1443,NULL,9,NULL,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1444,NULL,9,NULL,4,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1445,NULL,9,NULL,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1446,NULL,9,NULL,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1447,NULL,9,NULL,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1448,NULL,9,NULL,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1449,NULL,9,NULL,5,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1450,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1451,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1452,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1453,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1454,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1455,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1456,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1457,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1458,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1459,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1460,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32'),(1461,NULL,9,NULL,6,NULL,'Pacote',0,'2019-11-22 17:27:32','2019-11-22 17:27:32');
/*!40000 ALTER TABLE `sessoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_atendimento`
--

DROP TABLE IF EXISTS `tipo_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_atendimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_atendimento`
--

LOCK TABLES `tipo_atendimento` WRITE;
/*!40000 ALTER TABLE `tipo_atendimento` DISABLE KEYS */;
INSERT INTO `tipo_atendimento` VALUES (1,'Egreg Lipo 23','blue','2019-11-18 23:55:35','2019-10-23 15:46:43','2019-11-18 23:55:35'),(2,'EgregLipo 77','blue','2019-11-18 23:55:39','2019-11-18 23:29:13','2019-11-18 23:55:39'),(3,'EgregLipo777','yellow',NULL,'2019-11-18 23:29:33','2019-11-18 23:29:33');
/*!40000 ALTER TABLE `tipo_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tratamentos_esteticos`
--

DROP TABLE IF EXISTS `tratamentos_esteticos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tratamentos_esteticos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `impl_dentario` tinyint(1) NOT NULL,
  `impl_dentario_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trat_dermato` tinyint(1) NOT NULL,
  `trat_dermato_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cir_plastica` tinyint(1) NOT NULL,
  `cir_plastica_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cir_reparadora` tinyint(1) NOT NULL,
  `cir_reparadora_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outra_cirurgia` tinyint(1) NOT NULL,
  `outra_cirurgia_qual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tratamentos_esteticos_cliente_id_foreign` (`cliente_id`),
  CONSTRAINT `tratamentos_esteticos_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tratamentos_esteticos`
--

LOCK TABLES `tratamentos_esteticos` WRITE;
/*!40000 ALTER TABLE `tratamentos_esteticos` DISABLE KEYS */;
INSERT INTO `tratamentos_esteticos` VALUES (1,12,0,'2',0,'2',0,'2',0,'2',0,'2',NULL,'2019-10-29 15:33:44','2019-10-29 15:48:16');
/*!40000 ALTER TABLE `tratamentos_esteticos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `endereco_id` int(10) unsigned NOT NULL,
  `contato_id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unidades_endereco_id_foreign` (`endereco_id`),
  KEY `unidades_contato_id_foreign` (`contato_id`),
  CONSTRAINT `unidades_contato_id_foreign` FOREIGN KEY (`contato_id`) REFERENCES `contatos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `unidades_endereco_id_foreign` FOREIGN KEY (`endereco_id`) REFERENCES `enderecos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,1,1,'Matriz','Matriz',NULL,'2019-10-11 13:03:00',NULL),(2,1,1,'Unidade Mauá','Unidade Mauá',NULL,'2019-10-11 13:03:00',NULL);
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `endereco_id` int(10) unsigned NOT NULL,
  `unidade_id` int(10) unsigned NOT NULL,
  `contato_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_endereco_id_foreign` (`endereco_id`),
  KEY `users_unidade_id_foreign` (`unidade_id`),
  KEY `users_contato_id_foreign` (`contato_id`),
  CONSTRAINT `users_contato_id_foreign` FOREIGN KEY (`contato_id`) REFERENCES `contatos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_endereco_id_foreign` FOREIGN KEY (`endereco_id`) REFERENCES `enderecos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_unidade_id_foreign` FOREIGN KEY (`unidade_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,1,1,'Admin','admin@admin.com.br',NULL,'$2y$10$z0ut4va.zOcHUqy8s7zLWePJ7VlONdsIXS.BtWNhxJvIfg9XFIK.C','MASTER',1,NULL,NULL,'2019-10-11 15:55:14','2019-10-11 15:55:14'),(2,1,1,1,'User Matriz master','user@user.com.br',NULL,'$2y$10$DuGtNaRzjD20MzhsTYqETu4t94NWLt89ofamVDxkWLbHaTE2vjM0i','MASTER',1,NULL,'2019-10-14 22:58:19','2019-10-11 15:55:14','2019-10-14 22:58:19'),(12,35,1,35,'Andreia Santos','deia@email.com.br',NULL,'$2y$10$2rfEkH3jXI5CEEgs7rck6OJoYs0mOi..2KfIP4vLKXX5m1jwS3Bxi','ADMIN',NULL,NULL,'2019-10-14 22:57:37','2019-10-14 19:43:25','2019-10-14 22:57:37'),(19,45,1,45,'Andreia Santos','deia123@email.com.br',NULL,'$2y$10$28/VLF5BSU0B1jpYeHRDn.RNJXJ8e3keO4JB9xfHu2h0nN3tVqrCW','ADMIN',NULL,NULL,'2019-10-14 22:55:30','2019-10-14 19:54:45','2019-10-14 22:55:30'),(20,46,1,46,'Andreia Santos Oliveira','deia7775@email.com.br',NULL,'$2y$10$60Vq8eIhFG.x5v9U9fn1NeICrMNaNikunGrSVQ7hQ79m71QbvM3nK','MASTER',NULL,NULL,'2019-10-14 22:54:09','2019-10-14 19:57:04','2019-10-14 22:54:09'),(22,48,2,48,'Andreia Santos','deia@email.com',NULL,'$2y$10$0s0DtAENGai1gSzaT1J0F.CciorrPZe9TF78c3kLfTytBXxpyvEq6','CLIENTE',NULL,NULL,'2019-10-14 23:36:53','2019-10-14 23:15:20','2019-10-14 23:36:53'),(26,52,1,52,'Teresa','tere@email.com',NULL,'$2y$10$rFaSzpmRSnNaRbowkbIususrbkeuYrI9rZFt3rMfcxjG0dCbBZ50a','PROFISSIONAL',NULL,NULL,NULL,'2019-10-14 23:30:27','2019-10-14 23:30:27'),(27,53,2,53,'Maria Silva','mama@email',NULL,'$2y$10$OyZ4rRTSorQ2TZSpNTFVpO.5k4dmhMdO.4xLRh/Kvoq6H9j95LZre','PROFISSIONAL',NULL,NULL,NULL,'2019-10-14 23:36:44','2019-10-14 23:36:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-22 12:19:08
