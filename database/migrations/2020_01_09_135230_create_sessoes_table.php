<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessoes', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('profissional_id')->unsigned()->nullable();

            $table->foreign('profissional_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')
                ->references('id')
                ->on('contrato')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('contrato_servico_id')->unsigned()->nullable();
            $table->foreign('contrato_servico_id')
                ->references('id')
                ->on('contrato_servicos')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('servico_id')->unsigned()->nullable();
            $table->foreign('servico_id')
                ->references('id')
                ->on('servicos')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->date('data_execussao')->nullable();
            $table->enum('origem',['Serviço','Pacote']);
            $table->boolean('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessoes');
    }
}
