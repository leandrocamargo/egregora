<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControleChequeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_cheque', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('cliente_id')->unsigned()->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('contrato')->onDelete('cascade')->onUpdate('cascade');

            $table->string('banco');
            $table->string('cheque');

            $table->dateTime('vencimento');
            $table->string('parcelas');
            $table->enum('status', ['EM DIA','COMPENSADO','EM ATRASO','DEVOLVIDO', '2º DEVOLVIDO','CANCELADO']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_cheque');
    }
}
