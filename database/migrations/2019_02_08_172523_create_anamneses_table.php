<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnamnesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anamneses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->boolean('trat_estetic_anterior');
            $table->string('trat_estetic_anterior_qual');
            $table->boolean('utiliz_cosmeticos');
            $table->string('utiliz_cosmeticos_qual');
            $table->boolean('exp_sol');
            $table->boolean('uso_filtro');
            $table->boolean('tabagismo');
            $table->enum('bebida_alcoolica',['Nunca','Raro','Sempre']);
            $table->enum('func_intestinal',['Reg','Irreg']);
            $table->string('func_intestinal_obs');
            $table->enum('qualidade_sono',['Boa','Reg','Pess']);
            $table->string('qualidade_sono_obs');
            $table->enum('ingest_agua',['menos2','mais2']);
            $table->enum('alimentacao',['Boa','Reg','Pess']);
            $table->string('alimentos_pref');
            $table->boolean('prat_atv_fisica');
            $table->string('prat_atv_fisica_freq');
            $table->boolean('uso_anticoncepcional');
            $table->boolean('gestacao');
            $table->string('gestacao_qtde');
            $table->string('gravida');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('anamneses');
    }
}
