<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_servicos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_contrato')->unsigned();
            $table->foreign('id_contrato')->references('id')->on('contrato')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('id_servico')->unsigned()->nullable();
            $table->foreign('id_servico')->references('id')->on('servicos')->onDelete('set null')->onUpdate('set null');

            $table->integer('id_pacote')->unsigned()->nullable();
            $table->foreign('id_pacote')->references('id')->on('pacotes')->onDelete('set null')->onUpdate('set null');

            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('set null')->onUpdate('set null');

            $table->enum('perfil_user', ['PROFISSIONAL', 'RECEPCAO'])->default('PROFISSIONAL');

            $table->float('valor');
            $table->string('executado')->nullable();
            $table->date('data')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_servicos');
    }
}
