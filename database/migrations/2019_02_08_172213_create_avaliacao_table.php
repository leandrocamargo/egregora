<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->date('data');
            $table->float('peso');
            $table->float('altura');
            $table->float('porc_gordura');
            $table->float('porc_musculo');
            $table->float('imc');
            $table->text('observacao');
            $table->float('med_acima');
            $table->float('med_umbigo');
            $table->float('med_abaixo');
            $table->float('med_quadril');
            $table->float('med_entre_coxas');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacao');
    }
}
