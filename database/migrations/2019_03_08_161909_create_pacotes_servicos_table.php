<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacotesServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacotes_servicos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pacote_id')->unsigned();
            $table->foreign('pacote_id')->references('id')->on('pacotes')->OnDelete('cascade')->OnUpdate('cascade');

            $table->integer('servico_id')->unsigned();
            $table->foreign('servico_id')->references('id')->on('servicos')->OnDelete('cascade')->OnUpdate('cascade');

            $table->integer('qtd_sessao');
            $table->float('valor_sessao');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacotes_servicos');
    }
}
