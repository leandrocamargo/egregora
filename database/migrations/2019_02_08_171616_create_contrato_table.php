<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration
{

    public function up()
    {
        Schema::create('contrato', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('numero_contrato')->nullable();

            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->date('data');
            $table->decimal('desconto',8,2)->default(0);

            $table->integer('forma_pagamento_id')->unsigned()->nullable();
            $table->foreign('forma_pagamento_id')
                ->references('id')
                ->on('forma_pagamento')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->string('bandeira',50)->nullable();

            $table->integer('num_parcelas')->default(1);
            $table->float('valor_total');
            $table->string('status');
            $table->string('observacao')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato');
    }
}
