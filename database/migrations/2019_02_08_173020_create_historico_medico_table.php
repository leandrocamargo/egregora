<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoMedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_medico', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->boolean('trat_med_atual');
            $table->string('trat_med_atual_medic');
            $table->boolean('antec_alergico');
            $table->string('antec_alergico_qual');
            $table->boolean('port_marcapasso');
            $table->boolean('alt_cardiacas');
            $table->boolean('alt_pres_arterial');
            $table->boolean('dis_circulatorio');
            $table->string('dis_circulatorio_qual');
            $table->boolean('dis_renal');
            $table->string('dis_renal_qual');
            $table->boolean('dis_hormonal');
            $table->string('dis_hormonal_qual');
            $table->boolean('dis_gastro');
            $table->string('dis_gastro_qual');
            $table->boolean('epilepsia_convulsao');
            $table->string('epilepsia_convulsao_freq');
            $table->boolean('labirintite');
            $table->boolean('alteracao_psi');
            $table->string('alteracao_psi_qual');
            $table->boolean('estresse');
            $table->string('estresse_obs');
            $table->string('ant_oncologicos');
            $table->string('ant_oncologicos_qual');
            $table->boolean('diabetes');
            $table->string('diabetes_tipo');
            $table->string('outra_doenca');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_medico');
    }
}
