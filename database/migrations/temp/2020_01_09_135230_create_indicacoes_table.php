<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicacoes', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')
                ->references('id')
                ->on('contrato')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('cliente_id')->unsigned()->nullable();
            $table->foreign('cliente_id')
                ->references('id')
                ->on('clientes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('servico_id')->unsigned()->nullable();
            $table->foreign('servico_id')
                ->references('id')
                ->on('servicos')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->double('valor');

            $table->integer('pacote_id')->unsigned()->nullable();
            $table->foreign('pacote_id')
                ->references('id')
                ->on('pacotes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->date('data_indicacao')->nullable();
            $table->string('status')->nullable();
            $table->string('pagamento')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessoes');
    }
}
