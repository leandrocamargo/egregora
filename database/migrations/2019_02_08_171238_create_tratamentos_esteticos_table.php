<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTratamentosEsteticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tratamentos_esteticos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');

            $table->boolean('impl_dentario');
            $table->string('impl_dentario_qual');
            $table->boolean('trat_dermato');
            $table->string('trat_dermato_qual');
            $table->boolean('cir_plastica');
            $table->string('cir_plastica_qual');
            $table->boolean('cir_reparadora');
            $table->string('cir_reparadora_qual');
            $table->boolean('outra_cirurgia');
            $table->string('outra_cirurgia_qual');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tratamentos_esteticos');
    }
}
