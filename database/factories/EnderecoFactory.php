<?php

use Faker\Generator as Faker;

$factory->define(App\Endereco::class, function (Faker $faker) {
    return [
        'logradouro'       => $faker->streetAddress,
        'numero'           => '2545',
        'bairro'           => $faker->city,
        'cidade'           => $faker->city,
        'cep'              => '05050-230',
        'complemento'      => '',
        'estado'           => 'SP'
    ];
});