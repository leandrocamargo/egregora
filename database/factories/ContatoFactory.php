<?php

use Faker\Generator as Faker;

$factory->define(App\Contato::class, function (Faker $faker) {
    return [
        'telefone'         => $faker->phoneNumber,
        'email'            => $faker->email,
        'celular'          => $faker->phoneNumber,
        'tipo'             => "",
        'ativo'            => true
    ];
});