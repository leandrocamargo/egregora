<?php

use Illuminate\Database\Seeder;

class UnidadesSeeder extends Seeder
{
    public function run()
    {
        $this->create();

        DB::table('unidades')->insert([
            'nome'        => 'Mauá',
            'endereco_id' => 1,
            'contato_id'  => 1,
            'descricao'   => 'Unidade Mauá',
            'created_at'  => '2019-11-11 11:00:00'
        ]);
    }

    public function create()
    {
        factory(\App\Endereco::class, 1)->create();
        factory(\App\Contato::class, 1)->create();
    }
}
