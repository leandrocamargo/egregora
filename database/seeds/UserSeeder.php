<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        \App\User::create(array(
            'endereco_id' => 1,
            'contato_id'  => 1,
            'unidade_id'  => 1,
            'name'        => 'Admin',
            'email'       => 'admin@admin.com.br',
            'password'    => bcrypt('123456'),
            'tipo'        => 'Admin',
            'ativo'       => 1
        ));
    }
}
