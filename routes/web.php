<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('usuario', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    //rotas relacionadas a agenda
    Route::resource('agenda', 'AgendasController', ['except' => ['show']]);
    Route::get('agendados', 'AgendasController@getEventos');
    Route::post('agendar', 'AgendasController@store')->name('agenda.store');
    Route::get('agendado/{id}','AgendasController@edit')->name('agenda.edit');
    Route::put('agendado/edit/{id}','AgendasController@update')->name('agenda.update');

    //rotas relacionadas a atendimentos
    Route::resource('anamnese', 'AnamnesesController', ['except' => ['show']]);
    Route::get('anamnese/create/{id}','AnamnesesController@create')->name('anamnese.create');

    //rotas relacionadas a avaliação
    Route::resource('avaliacao', 'AvaliacoesController', ['except' => ['show']]);
    Route::get('avaliacao/create/{id}', 'AvaliacoesController@create')->name('avaliacao.create');
    Route::get('avaliacao/detalhes/{id}', 'AvaliacoesController@detalhes')->name('avaliacao.detalhes');
    Route::get('avaliacao/ajax/chart/{id}', 'AvaliacoesController@carregandoChart');

    //rotas relacionadas ao controle de cheques
    Route::resource('cheque', 'ControleChequesController', ['except' => ['show']]);

    //Rotas relacionadas ao contrato-serviço
    Route::get('/contrato-servico/show/{id}', 'ContratoServicoController@show')->name('contrato.servico.executar');
    Route::post('/contrato-servico/fechar', 'ContratoServicoController@fecharContrato')->name('contrato.fechar');
    Route::post('/contrato-servico/adicionar', 'ContratoServicoController@store')->name('contrato.servico');

    //rotas relacionadas ao clientes
	Route::resource('cliente', 'ClientesController', ['except' => ['show']]);

    Route::get('/getCliente/{id}', 'ClientesController@getCliente')->name('clientes.get');
    Route::get('/getClienteAutoComplete', 'ClientesController@getClienteAutoComplete');

	//rotas relacionadas ao contrato
    Route::resource('contratos', 'ContratosController', ['except' => ['show']]);

    Route::get('/contrato/fechar/{id}', 'ContratoController@show')->name('contrato.show');
    Route::post('/contrato/adicionar', 'ContratoController@store')->name('contrato.store');
    Route::put('/contrato/fechamento/{id}', 'ContratoController@update')->name('contrato.update');

    //rotas relacionadas a formas de pagamentos
    Route::resource('forma_pagamento', 'FormasPagamentosController', ['except' => ['show']]);

    Route::get('/formapagamentos-ajax', 'FormasPagamentosController@formaPagamento');

    //rotas relacionadas com historico medico
    Route::resource('historico_medico', 'HistoricosMedicosController', ['except' => ['show']]);
    Route::get('historico_medico/create/{id}', 'HistoricosMedicosController@create')->name('historico_medico.create');

    //rotas relacionadas as indicações
    Route::resource('indicacao', 'IndicacoesController', ['except' => ['show']]);

    //rotas relacionadas a pacotes
    Route::resource('pacote', 'PacotesController', ['except' => ['show']]);

    Route::get('/pacotes/create', 'PacotesController@create')->name('pacotes.adicionar');
    Route::get('/pacotes-lista', 'PacotesController@listagem')->name('pacotes.listagem');
    Route::post('/pacotes/adicionar', 'PacotesController@store')->name('pacotes.adicionar.post');
    Route::post('/pacotes/index/{id}', 'PacotesController@destroy')->name('pacotes.index.post');
    Route::put('/pacotes/editar-cadastro/{id}', 'PacotesController@update')->name('pacotes.editar.post');
    Route::post('/pacote-servico', 'PacotesController@getServico');

    //rotas relacionadas a produtos
    Route::resource('produto', 'ProdutosController', ['except' => ['show']]);

    //rotas relacionadas aos protocolos
    Route::get('/protocolo/{id}', 'ProtocolosController@protocolo')->name('recomendar.protocolo');

    //rotas relacionadas a servicos
    Route::resource('servico', 'ServicosController', ['except' => ['show']]);

    Route::get('/servicos/{id}', 'ServicosController@getValor');
    Route::get('/servicos-lista', 'ServicosController@listagem')->name('servicos.listagem');

    //rotas relacionadas a sessao
    Route::resource('sessao', 'SessoesController', ['except' => ['show']]);
    Route::get('atender_sessao/{id}', 'SessoesController@atender')->name('atender.cliente');
    Route::get('listar-sessoes/{id}', 'SessoesController@listarSessoes')->name('listar.sessoes');

    //Rotas do sistema egregora antigo
    Route::get('/executar-sessoes/{id}', 'SessoesController@atendimento')->name('sessao.atendimento');
    Route::get('/get-sessoes/{id}', 'SessoesController@getSessoes')->name('sessao.get.sessoes');
    Route::get('/get-sessoes-by-contrato/{id}', 'SessoesController@getSessoesByContrato')->name('sessao.get.sessoes.by.contrato');
    Route::get('/sessoes/executado/{id}', 'SessoesController@setData')->name('sessao.setdata');
    Route::post('/sessoes/executado', 'SessoesController@executarServico')->name('sessao.executar');

    //rotas relacionadas a tipos de atendimentos
    Route::resource('tipo-atendimento', 'TiposAtendimentosController', ['except' => ['show']]);

    //rotas relacionadas ao tratamento estetico
    Route::resource('tratamento_estetico', 'TratamentosEsteticosController', ['except' => ['show']]);
    Route::get('tratamento_estetico/create/{id}', 'TratamentosEsteticosController@create')->name('tratamento_estetico.create');

    //rotas relacionadas a unidades
    Route::resource('unidade', 'UnidadesController', ['except' => ['show']]);

    Route::get('/get-user', 'UserController@getUser');

	//Rota PageController já criada por Tim Creative - mante-la abaixo de todas as outras rotas
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);
});


