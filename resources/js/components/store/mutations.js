import swal from 'sweetalert';

const SET_LIST_PACOTES = (state, lista) => {
    state.list = lista;
};

const SET_LIST_SERVICOS = (state, listaServicos) => {
    state.listServices = listaServicos.listaServicos;
};

const SET_LIST_RECOMENDACOES = (state, lista) => {
    state.recomendacoesList.push(
        {
            pacotes: lista,
            id_user    : state.user.id,
            perfil_user: state.user.tipo,
        }
        );
};

const SET_USER = (state, data) => {
  state.user = data;
};

const SET_LIST_PACOTES_CONTRATO = (state, lista) => {
    state.recomendacoesList  = lista.listaPacotes;
};

const SET_LIST_SERVICO_CONTRATO = (state, lista) => {
    state.recomendacoesListServicos = lista.listaServicos;
    for (let index in state.recomendacoesListServicos)  {
        state.recomendacoesListServicos[index].valor = formatMoney(state.recomendacoesListServicos[index].valor);
    }
};

const ATUALIZAR_LIST_RECOMENDACOES_SERVICOS = (state, obj) => {
    let recomendacao = obj.obj;
    let index = state.recomendacoesListServicos.findIndex(function (index_) {
       if (recomendacao.id == index_.id)  {
         return index_
       }
    });

    state.recomendacoesListServicos.splice(index,1,recomendacao);

    state.totalGeral = 0;

    for (let index in state.recomendacoesListServicos)  {
        state.totalGeral +=  parseFloat(trataValor(state.recomendacoesListServicos[index].valor)
            * state.recomendacoesListServicos[index].quantidade);

        state.recomendacoesListServicos[index].valor = formatMoney(trataValor(state.recomendacoesListServicos[index].valor));
    }
    state.totalGeral = (state.totalGeral + state.totalPacotes);
};

const REMOVE_PACOTE = (state, obj) => {
    let recomendacao = obj.obj;
    let index = state.recomendacoesList.findIndex(function (index_) {
        if (recomendacao.id == index_.id)  {
            return index_
        }
    });

    state.recomendacoesList.splice(index,1);

    state.totalPacotes = (state.totalPacotes - recomendacao.valor_pacote);

    state.totalGeral = (state.totalGeral - recomendacao.valor_pacote);
};

const REMOVE_SERVICO = (state, obj) => {
    let recomendacaoServico = obj.obj;
    let index = state.recomendacoesListServicos.findIndex(function (index_) {
        if (recomendacaoServico.id == index_.id)  {
            return index_
        }
    });

    state.recomendacoesListServicos.splice(index,1);

    state.totalGeral = (state.totalGeral - (trataValor(recomendacaoServico.valor) * recomendacaoServico.quantidade));
};

const trataValor = (valor) => {
    valor     = valor.replace('R','');
    valor     = valor.replace('$','');
    valor     = valor.replace(' ','');
    valor     = valor.replace('.','');
    valor     = valor.replace(',','.');

    return parseFloat(valor);
};

const SET_LIST_SERVICOS_RECOMENDADOS = (state, listaServico) => {
    let servico = {
        color:      listaServico.color,
        descricao:  listaServico.descricao,
        id:         listaServico.id,
        imagem:     listaServico.imagem,
        nome:       listaServico.nome,
        valor:      formatMoney(listaServico.valor),
        quantidade: 1,
    };

    state.recomendacoesListServicos.push(servico);
};

const SET_TOTAL_PACOTE = (state, valorPacote) => {
    state.totalPacotes += parseFloat(valorPacote.valor);
};

const SET_TOTAL_SERVICO = (state, valorServico) => {
    state.totalServicos += parseFloat(valorServico);
};

const SET_TOTAL_GERAL = (state, valor) => {
    state.totalGeral += parseFloat(valor.valor);
};

const LESS_TOTAL_GERAL = (state, valor) => {
    state.totalGeral += state.totalGeral - parseFloat(valor.valor);
};

//Salvar recomendação de protocolo no atendimento/consulta
const SALVAR_RECOMENDACAO = async (state) => {
    let _url_atual = state.url_atual;
    let novo_url   = _url_atual.slice(0,22);

    for (let index in state.recomendacoesListServicos)  {
        state.recomendacoesListServicos[index].valor = trataValor(state.recomendacoesListServicos[index].valor);
    }

    const array = {
        id_cliente : state.clienteId,
        id_servicos: state.recomendacoesListServicos,
        id_pacotes : state.recomendacoesList,
        id_user: state.user.id,
        perfil_user: state.user.tipo,
        valor_total: state.totalGeral
    };

    let response = (await axios.post('/contrato-servico/adicionar', array));

    if(response.data.valor_total <= 0){
        swal({
            title:  "Obrigatório",
            text:   "Selecionar no mínimo 1 protocolo",
            icon:   "warning",
        })
    } else if(response.status == 201 || response.status == 200){
        swal({
            title:  "Recomendações",
            text:   "Salvas com sucesso!",
            icon:   "success",
        })
            .then(() => {
                window.location.href = novo_url + 'sessao';
            });
        } else {
        swal ( "Oops" ,  "Não foi possível salvar o contrato!" ,  "error" );
    }
};

const SET_FORMA_DE_PAGAMENTO = (state, obj) => {
    state.contrato.num_parcelas = obj;
};

const SET_NUM_DE_PARCELAS = (state, obj) => {
    state.contrato.forma_pagamento_id = obj;
};

const SET_BANDEIRA = (state, obj) => {
    state.contrato.bandeira = obj;
};

const SET_OBSERVACAO = (state, obj) => {
    state.contrato.observacao = obj;
};

//Salvar/fechar contrato após atendimento/consulta na recepção/financeiro
const FECHAR_CONTRATO = (state, status) => {
    let fechar_url_atual = state.url_atual;
    let fechar_novo_url = fechar_url_atual.slice(0,22);
    state.contrato.status   = status;
    state.contrato.desconto = parseFloat(state.desconto).toFixed(2);

    for (let index in state.recomendacoesListServicos)  {
        state.recomendacoesListServicos[index].valor = trataValor(state.recomendacoesListServicos[index].valor);
    }

    const data =  {
        contract   : state.contrato,
        pacotes    : state.recomendacoesList,
        servicos   : state.recomendacoesListServicos,
        total      : state.totalGeral,
        desconto   : state.desconto,
    };

    axios.post('/contrato-servico/fechar',data).then(response => {
        if (response.status == 201 || response.status == 200) {
            if (data.contract.status == 'Fechado') {
                swal ({
                    title:  "Contrato",
                    text:   "Emitido com sucesso!",
                    icon:   "success",
                })
                    .then(() => {
                        window.location.href = fechar_novo_url+'contratos';
                    })
            } else {
                swal ({
                    title:  "Orçamento",
                    text:   "Emitido com sucesso!",
                    icon:   "success",
                })
                    .then(() => {
                        window.location.href = fechar_novo_url+'contratos';
                    })
            }
        }
        else {
            swal("Oops!", "Não foi possível salvar o contrato!", "error");
        }
    })
};

const SET_CLIENTE = (state, obj) => {
    state.clienteId = obj.getCliente;
};

const formatMoney = (money) => {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(money);
};

const SET_DESCONTO = (state, desconto) => {
  state.desconto   = desconto;
};

const GET_CONTRATO = (state, contrato) => {
    state.contrato = contrato;
    state.cliente = contrato.cliente;
};

export default{
    SET_LIST_PACOTES,
    SET_DESCONTO,
    SET_LIST_SERVICOS,
    SET_LIST_RECOMENDACOES,
    LESS_TOTAL_GERAL,
    ATUALIZAR_LIST_RECOMENDACOES_SERVICOS,
    REMOVE_PACOTE,
    REMOVE_SERVICO,
    SET_LIST_SERVICOS_RECOMENDADOS,
    SET_TOTAL_GERAL,
    SET_TOTAL_PACOTE,
    SET_TOTAL_SERVICO,
    SALVAR_RECOMENDACAO,
    SET_CLIENTE,
    SET_LIST_SERVICO_CONTRATO,
    SET_LIST_PACOTES_CONTRATO,
    GET_CONTRATO,
    FECHAR_CONTRATO,
    SET_FORMA_DE_PAGAMENTO,
    SET_NUM_DE_PARCELAS,
    SET_BANDEIRA,
    SET_OBSERVACAO,
    SET_USER
};
