const getBandeira    = state => state.contrato.bandeira;
const getNumParcelas = state => state.contrato.num_parcelas;
const user           = state => state.user;

export default {
    getBandeira,
    getNumParcelas,
    user
}
