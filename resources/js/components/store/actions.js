import axios from "axios";

const getListaPacotes = async({commit}, obj) => {
    let lista = await (axios.get('/pacotes-lista'));
    lista = lista.data;
    commit('SET_LIST_PACOTES', { lista });
};

const getUser = async ({commit}) => {
    let data = await (axios.get('/get-user'));
    commit('SET_USER',  data.data);
};

const atualizarRecomendacoesListServicos = ({commit}, obj) => {
    commit('ATUALIZAR_LIST_RECOMENDACOES_SERVICOS', {obj})
};

const removePacote = ({commit}, obj) => {
    commit('REMOVE_PACOTE', {obj})
};

const removeServico = ({commit}, obj) => {
    commit('REMOVE_SERVICO', {obj})
};

const getListaServicos = async({commit}, obj) => {
    let listaServicos = await (axios.get('/servicos-lista'));
    listaServicos = listaServicos.data;
    commit('SET_LIST_SERVICOS', { listaServicos});
};

const setPacoteInRecomendacao = ({ commit }, pacote) => {
    console.log('pacote', pacote)
    commit('SET_LIST_RECOMENDACOES',pacote);
    let valor = pacote.valor_pacote;
    commit('SET_TOTAL_GERAL', { valor });
    commit('SET_TOTAL_PACOTE', { valor });
};

const setServicoInRecomendacao = ({ commit }, servico) => {
    commit('SET_LIST_SERVICOS_RECOMENDADOS', servico);
    let valor = servico.valor;
    commit('SET_TOTAL_GERAL', { valor });
};

const salvarRecomendacoes = ({commit}) => {
    commit('SALVAR_RECOMENDACAO');
};

const salvarContrato = ({commit}, obj) => {
    commit('SET_FORMA_DE_PAGAMENTO', obj.num_parcelas);
    commit('SET_NUM_DE_PARCELAS',    obj.forma_de_pagamento_id);
    commit('SET_BANDEIRA',           obj.bandeira);
    commit('FECHAR_CONTRATO',        obj.status_contrato);
    commit('SET_OBSERVACAO',         obj.observacao);
};

const setDesconto = ({commit}, desconto) => {
  let desconto_ = desconto.target.value;
  commit('SET_DESCONTO',desconto_)
};

/*Recebe o id do cliente a partir da url protocolo, separa da url com a função split
e seta o id na url getCliente para buscar os dados completos do cliente
e setar o nome no input da tela de recomendações
*/
const getCliente = async ({commit}) => {
    let url_atual = window.location.href;
    let id = url_atual.split("/protocolo/");
    const getCliente = (await axios.get(`/getCliente/${id[1]}`)).data;
    commit('SET_CLIENTE', {getCliente});
};

/*Envia os dados salvos da indicação de protocolo para o fechamento de contrato*/
const getContrato = async ({commit}, id_) => {
    let url_atual = window.location.href;
    let id = url_atual.split("/", 5);
    let response = (await axios.get(`/contrato-servico/show/${id[4]}`)).data;
    let listaServicos = response.servicos;
    let listaPacotes = response.pacotes;
    let valor = response.valor_total_pacotes;

    commit('SET_LIST_SERVICO_CONTRATO', {listaServicos});
    commit('SET_LIST_PACOTES_CONTRATO', {listaPacotes});
    commit('SET_TOTAL_PACOTE', {valor});
    commit('SET_DESCONTO', response.contrato.desconto);
    valor = response.valor_total_servicos;
    commit('SET_TOTAL_SERVICO', {valor});
    valor = response.contrato.valor_total;
    commit('SET_TOTAL_GERAL', {valor});
    commit('GET_CONTRATO', response.contrato);
};

export default {
    getListaPacotes,
    getListaServicos,
    setDesconto,
    atualizarRecomendacoesListServicos,
    setPacoteInRecomendacao,
    setServicoInRecomendacao,
    removePacote,
    removeServico,
    salvarRecomendacoes,
    getCliente,
    getContrato,
    salvarContrato,
    getUser
};
