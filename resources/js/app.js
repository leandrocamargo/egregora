/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./service');
require('../lib/jQuery-Mask-Plugin/jquery.mask.min');
require('fullcalendar/dist/fullcalendar');
require('./calendar');
require('./pacote_create');
require('./chart_avaliacao');
require('./autocomplete');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('recomendacao', require('./components/Recomendacao.vue').default);
Vue.component('fechar-recomendacao', require('./components/Fechar-Recomendacao').default);
Vue.component('contrato', require('./components/Contrato').default);
Vue.component('sessao', require('./components/Sessoes').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
