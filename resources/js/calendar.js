import { Calendar } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import moment from 'moment';

window.getEventos = async function() {
    var dados;
    var _url = '/agendados';
    await $.ajax({
        url: _url,
        method: "GET",
        success: function (response) {
            dados = response
        }
    });

    return dados;
};

//Fullcalendar Agendados
document.addEventListener('DOMContentLoaded', async function() {
    var calendarEl = document.getElementById('calendar');
    var modal = document.getElementById('modalAgendar');
    var eventos = await getEventos();
    var calendar = new Calendar(calendarEl, {
        defaultView: 'timeGridDay',
        locale: 'pt-br',
        hiddenDays: [ 0 ],
        minTime:'07:00:00',
        maxTime:'21:00:00',
        allDaySlot: false,
        eventTextColor:'white',
        buttonText:{
            today:    'Hoje',
            month:    'Mês',
            week:     'Semana',
            day:      'Dia',
        },
        plugins: [timeGridPlugin, dayGridPlugin, interactionPlugin],

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'timeGridDay,timeGridWeek,dayGridMonth'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        selectable: true, // habilita a funcão de clicar na data e realizar acão
        eventStartEditable: false, // desabilita o drag-on-drop

        //funcão de evento no click na data do calendario para adicionar evento - desabilitado 17/02/20
        // select: function(info) {
        //     $("#modalCadastrar").modal();
        // },

        eventClick: function(info){
            $("#modalDetalhes").modal();
            getCliente(info.event.id);
        },

        events: eventos
    });

    calendar.render();
});

//envia os dados para views.agenda.modal-detalhes
window.getCliente = async function(id) {
    var resposta;
    var _urlCliente = '/agendado/' + id;
    await $.ajax({
        url: _urlCliente,
        method: "GET",
        success: function (response) {
            resposta = response
        }
    });

    setValores(resposta);
};

window.setValores = async function(dados) {
    $('#modalDetalhes').val(dados.id);
    $('#modalDetalhes #cliente').text(dados.cliente_id.nome);
    $('#modalDetalhes #cliente').val(dados.cliente_id.nome);
    $('#modalDetalhes #cpf').text(dados.cliente_id.cpf);
    $('#modalDetalhes #cpf').val(dados.cliente_id.cpf);
    $('#modalDetalhes #telefone').text(dados.cliente_id.contato.telefone);
    $('#modalDetalhes #telefone').val(dados.cliente_id.contato.telefone);
    $('#modalDetalhes #celular').text(dados.cliente_id.contato.celular);
    $('#modalDetalhes #celular').val(dados.cliente_id.contato.celular);
    $('#modalDetalhes #horario_inicio').text(moment(dados.horario_inicio).format('DD/MM/YYYY HH:mm'));
    $('#modalDetalhes #horario_inicio').val(moment(dados.horario_inicio).format('DD/MM/YYYY HH:mm'));
    $('#modalDetalhes #horario_final').text(moment(dados.horario_final).format('DD/MM/YYYY HH:mm'));
    $('#modalDetalhes #horario_final').val(moment(dados.horario_final).format('DD/MM/YYYY HH:mm'));
    $('#consulta_tipo').text(dados.consulta_tipo);
    $('#consulta_tipo').val(dados.consulta_tipo);
    $('#servico').text(dados.tipo_atendimento_id.descricao);
    $('#servico').val(dados.tipo_atendimento_id.descricao);
    $('#status').text(dados.status);
    $('#status').val(dados.status);

    $("#editAgenda").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "/agendado/edit/" + dados.id,
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    location.reload();
                } else {
                    $("#msg-edit").html(response['msg']);
                }
            }
        })
    });
};
