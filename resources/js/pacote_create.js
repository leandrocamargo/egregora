$(document).ready(function () {
    var valorUnitario = 0;
    var qtdAnterior;
    var totalBrutoAtual = parseFloat($('#bruto').val());

    window.arrayDeletados = [];

    if(totalBrutoAtual){
        var total = totalBrutoAtual
    } else {
        var total = 0
    };

    $('#servico').on('change', function () {
        $.ajax({
            url: '/servicos/' + $('#servico').val(),
            method: 'GET',
            success: function (response) {
                $('#valorLabel').addClass('active');
                $('#valor').val(response);
            }
        });
    });

    $('#adicionarCreate').on('click',function () {
        var _html = $('.slot .row').html();
        var _select = $('.slot .row select').val();
        _html += '<input type="hidden" name="id_servico[]" value="'+_select+'">';

        $('<div class="row">'+_html+'</div>').appendTo('#servicos_list').find('input').attr('readonly', true);

        $('#servicos_list .row').find('input#valor').last().attr('name', 'valor[]');
        $('#servicos_list .row').find('input#qtd_sessao').last().attr('name', 'qtd_sessao[]');

        // colocar os valores dentro da input
        var _inputs = $('.slot .row').find('input');

        $(_inputs).each(function(index,value){
            _val = $(value).val();
            _name = $(value).attr('id');

            if(_name == 'valor'){
                valorUnitario = _val;
            }

            if(_name == 'qtd_sessao'){
                total += (_val*valorUnitario);
            }

            $('#servicos_list input[name="'+_name+'[]"]').last().val(_val);
            $('#servicos_list select').last().attr('disabled','disabled');
            $('.editar').last().removeClass('d-none');
            $('.salvar').last().removeClass('d-none');
            $('.excluir').last().removeClass('d-none');
        });

        $('#bruto').val(total).focus();

        $('#servicos_list select').last().val(_select);

        $('.slot .row input, .slot .row select').val('');

        $('.serv').on('change',function () {
            var _valor = $(this).val();
            var _that = $(this);
            $.ajaxSetup({
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
                url: "/pacote-servico",
                method: 'post',
                data: {'id': _valor},
                success: function(response){
                    _that.parent().parent().parent().find('input[name="valor[]"]').val(response.valor).focus();
                },
            });
            return false;
        });
    });

    window.edit = function (_element) {
        qtdAnterior = $(_element).parent().parent().parent().find('input[name="qtd_sessao[]"]').val();
        $(_element).parent().parent().parent().find('input[name="qtd_sessao[]"]').attr('readonly', false);
        $(_element).parent().parent().parent().find('input[name="valor[]"]').attr('readonly', false);
        $(_element).parent().parent().parent().find('select').removeAttr('disabled');
    };

    window.salvar = function (_element) {
        var _val = $(_element).parent().parent().parent().find('input[name="valor[]"]').val();
        var _totalBruto = $('#bruto').val();
        var _totalAnterior = (_val*qtdAnterior);
        var _qtdAtual = $(_element).parent().parent().parent().find('input[name="qtd_sessao[]"]').val();

        $(_element).parent().parent().parent().find('input').attr('readonly', true);
        $(_element).parent().parent().parent().find('select').attr('disabled');
        $('#servicos_list select').attr('disabled','disabled');

        valorUnitario = _val;

        _totalBruto -= _totalAnterior;

        _totalBruto += (_val*_qtdAtual);
        total = _totalBruto;

        $('#bruto').val(_totalBruto);
    };

    window.deletar = function (_element, id){
        window.arrayDeletados.push(id);
        $('#deletados').val(window.arrayDeletados.join());
        var _val = $(_element).parent().parent().parent().find('input[name="valor[]"]').val();
        var _totalBruto = $('#bruto').val();
        var _qtdAtual = $(_element).parent().parent().parent().find('input[name="qtd_sessao[]"]').val();
        var totalParcial = (_val*_qtdAtual);

        _totalBruto -= totalParcial;
        total = _totalBruto;
        $('#bruto').val(total);

        $(_element).parent().parent().parent().remove();
    };

    window.aplicar = function () {
        var _totalBruto = $('#bruto').val();
        var desconto = $('#desconto').val();
        var liquido = _totalBruto - desconto;
        $('#liquido').val(liquido).focus();
    };
});
