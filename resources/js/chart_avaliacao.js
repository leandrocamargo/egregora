$(document).ready(function() {
    var DataAvaliacao = new Array();
    var PesoChart = new Array();
    var borderColorPeso = new Array();

    var DataImc = new Array();
    var imcChart = new Array();
    var backGroundColorImc = new Array();
    var borderColorImc = new Array();

    var clienteId = $('#clienteId').val();

    var ctxPeso = document.getElementById('graficoAvaliacao').getContext('2d');
    var gradientFillPeso = ctxPeso.createLinearGradient(0, 250, 0, 50);
    gradientFillPeso.addColorStop(0, "rgba(152, 251, 152, 0)");
    gradientFillPeso.addColorStop(1, hexToRGB('#18ce0f',0.4));

    var ctxImc = document.getElementById('imcGrafico').getContext('2d');
    var gradientFillImc = ctxImc.createLinearGradient(0, 250, 0, 50);
    gradientFillImc.addColorStop(0, "rgba(54, 162, 235, 0)");
    gradientFillImc.addColorStop(1, hexToRGB('#2CA8FF', 0.6));

    function Dataset() {
        this.dataset = {
            label: '',
            data: [],
            backgroundColor: '',
            borderColor: '',
            borderWidth: 1
        }
    }

    if(clienteId != undefined) {
            fetch('/avaliacao/ajax/chart/' + clienteId)
                .then(response => response.json())
                .catch(error => console.error("Erro:" + error))
                .then(response => {

                    let dataGordura = new Dataset;
                    dataGordura.dataset.label = 'Gordura';
                    dataGordura.dataset.backgroundColor = 'rgba(255, 35, 35, 0.2)';
                    dataGordura.dataset.borderColor = 'rgba(255, 35, 35, 1)';

                    let dataMusculo = new Dataset;
                    dataMusculo.dataset.label = 'Musculo';
                    dataMusculo.dataset.backgroundColor = 'rgba(255, 206, 86, 0.2)';
                    dataMusculo.dataset.borderColor = 'rgba(255, 206, 86, 1)';

                    let dataMedAcima = new Dataset;
                    dataMedAcima.dataset.label = 'Acima';
                    dataMedAcima.dataset.backgroundColor = 'rgba(210, 105, 30, 0.2)';
                    dataMedAcima.dataset.borderColor = 'rgba(210, 105, 30, 1)';

                    let dataMedUmbigo = new Dataset;
                    dataMedUmbigo.dataset.label = 'Umbigo';
                    dataMedUmbigo.dataset.backgroundColor = 'rgba(75, 0, 130, 0.2)';
                    dataMedUmbigo.dataset.borderColor = 'rgba(75, 0, 130, 1)';

                    let dataMedAbaixo = new Dataset;
                    dataMedAbaixo.dataset.label = 'Abaixo';
                    dataMedAbaixo.dataset.backgroundColor = 'rgba(255, 215, 0, 0.2)';
                    dataMedAbaixo.dataset.borderColor = 'rgba(255, 215, 0, 1)';

                    let dataMedQuadril = new Dataset;
                    dataMedQuadril.dataset.label = 'Quadril';
                    dataMedQuadril.dataset.backgroundColor = 'rgba(0, 0, 255, 0.2)';
                    dataMedQuadril.dataset.borderColor = 'rgba(0, 0, 255, 1)';

                    let dataMedCoxa = new Dataset;
                    dataMedCoxa.dataset.label = 'Entre Coxa';
                    dataMedCoxa.dataset.backgroundColor = 'rgba(105, 105, 105, 0.2)';
                    dataMedCoxa.dataset.borderColor = 'rgba(105, 105, 105, 1)';

                    $.each(response, function (index, values) {
                        var dataValue = new Date(values.data);
                        var formataData = dataValue.toLocaleDateString();

                        DataAvaliacao.push(formataData);
                        PesoChart.push(values.peso);
                        borderColorPeso.push('rgba(50, 205, 50, 1)');
                        graficoAvaliacao.update();

                        DataImc.push(formataData);
                        imcChart.push(values.imc);
                        backGroundColorImc.push(gradientFillImc);
                        borderColorImc.push('rgba(54, 162, 235, 1)');
                        imcGrafico.update();

                        dataGordura.dataset.data.push(values.porc_gordura);
                        dataMusculo.dataset.data.push(values.porc_musculo);
                        graficoPorcentagem.data.labels.push(formataData);

                        dataMedAcima.dataset.data.push(values.med_acima);
                        dataMedUmbigo.dataset.data.push(values.med_umbigo);
                        dataMedAbaixo.dataset.data.push(values.med_abaixo);
                        dataMedQuadril.dataset.data.push(values.med_quadril);
                        dataMedCoxa.dataset.data.push(values.med_entre_coxas);
                        graficoMedidas.data.labels.push(formataData);
                    });

                    graficoPorcentagem.data.datasets.push(
                        dataGordura.dataset,
                        dataMusculo.dataset);

                    graficoPorcentagem.update();

                    graficoMedidas.data.datasets.push(
                        dataMedAcima.dataset,
                        dataMedUmbigo.dataset,
                        dataMedAbaixo.dataset,
                        dataMedQuadril.dataset,
                        dataMedCoxa.dataset);

                    graficoMedidas.update();
                });
        } else {
        fetch('/home')
    }

    window.graficoAvaliacao = new Chart(ctxPeso, {
        type: 'line',
        data: {
            labels: DataAvaliacao,
            datasets: [{
                label: 'Peso',
                borderColor: borderColorPeso,
                pointBorderColor: "#FFF",
                pointBackgroundColor: "#18ce0f",
                pointBorderWidth: 2,
                pointHoverRadius: 4,
                pointHoverBorderWidth: 1,
                pointRadius: 4,
                fill: true,
                backgroundColor: gradientFillPeso,
                borderWidth: 2,
                data: PesoChart
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });

    window.imcGrafico = new Chart(ctxImc, {
        type: 'bar',
        data: {
            labels: DataImc,
            datasets: [{
                label: 'IMC',
                data: imcChart,
                fill: false,
                backgroundColor: backGroundColorImc,
                borderColor: borderColorImc,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('graficoPorcentagem').getContext('2d');
    window.graficoPorcentagem = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('graficoMedidas').getContext('2d');
    window.graficoMedidas = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
});
