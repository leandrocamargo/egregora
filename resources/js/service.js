$(document).ready(function() {

    window.setData = function (id) {
        var url = 'sessoes/executado/' + id;
        $.ajax({
            url: url,
            method: 'get',
            success: function (response) {
                window.location.reload();
            }
        })
    };

    //Início Mascara para preenchimento do peso e altura em  avaliação
    $('#altura').mask('0,00');
    window.mascara_peso = function() {
         var v = this.value,
             integer = v.split(',')[0];

         v = v.replace(/\D/, "");
         v = v.replace(/^[0]+/, "");

         if (v.length <= 3 || !integer) {
             if (v.length === 1) v = '0,00' + v;
             if (v.length === 2) v = '0,0' + v;
             if (v.length === 3) v = '0,' + v;
         } else {
             v = v.replace(/^(\d{1,})(\d{3})$/, "$1,$2");
             console.log('ae');
         }

         this.value = v;
    };

    $(document).on('keyup', '#peso', mascara_peso);
    //Fim Mascara para preenchimento do peso em  avaliação

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#input-logradouro").val("");
        $("#input-bairro").val("");
        $("#input-cidade").val("");
        $("#input-estado").val("");
    }
    //Quando o campo cep perde o foco.
    $("#input-cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#input-logradouro").val("...");
                $("#input-bairro").val("...");
                $("#input-cidade").val("...");
                $("#input-estado").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#input-logradouro").val(dados.logradouro).focus();
                        $("#input-bairro").val(dados.bairro).focus();
                        $("#input-cidade").val(dados.localidade).focus();
                        $("#input-estado").val(dados.uf).focus();
                        $('#input-numero').focus();
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });

    $('#input-cep').mask('00000-000');
    $('#input-telefone').mask('(00) 0000-0000');
    $('#input-celular').mask('(00) 0 0000-0000');
    $('#input-cpf').mask('000.000.000-00');
    $('#input-data_nasc').mask('00/00/0000');

    $("#addAgenda").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "/agendar",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    location.reload();
                } else {
                    $("#msg-cad").html(response['msg']);
                }
            }
        })
    });

    //Modal em agenda/modal-detalhes.blade
    $('.btn-mostra-modal-editar').on("click", function(){
        $('.detalhesAgendamento').slideToggle();
        $('.editarAgendamento').slideToggle();
    });

    $('.btn-cancela-modal-editar').on("click", function(){
        $('.editarAgendamento').slideToggle();
        $('.detalhesAgendamento').slideToggle();
    });

    //Mascara para o campo data e hora
    window.DataHora = function(evento, objeto) {
        var keypress = (window.event) ? event.keyCode : evento.which;
        campo = eval(objeto);
        if (campo.value == '00/00/0000 00:00') {
            campo.value = "";
        }

        caracteres = '0123456789';
        separacao1 = '/';
        separacao2 = ' ';
        separacao3 = ':';
        conjunto1 = 2;
        conjunto2 = 5;
        conjunto3 = 10;
        conjunto4 = 13;
        conjunto5 = 16;
        if ((caracteres.search(String.fromCharCode(keypress)) != -1) && campo.value.length < (19)) {
            if (campo.value.length == conjunto1)
                campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto2)
                campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto3)
                campo.value = campo.value + separacao2;
            else if (campo.value.length == conjunto4)
                campo.value = campo.value + separacao3;
        } else {
            event.returnValue = false;
        }
    };

    //esconder barra de status após 3  segundos
    setTimeout(function () {
        $('#alert_success').hide('slow');
    }, 3000);

    setTimeout(function () {
        $('#alert_errors').hide('slow');
    }, 3000);
});
