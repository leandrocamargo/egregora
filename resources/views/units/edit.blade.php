@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Editar cliente',
    'activePage' => 'cliente',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Unidades') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('unidade.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('unidade.update', $unidade) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações da Unidade') }}</h6>
                                <div class="row">
                                    <div class="col-md-8 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Nome') }}</label>
                                            <input type="text" name="nome" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('nome', $unidade->nome) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('descricao') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-descricao">{{ __('Descrição') }}</label>
                                            <input type="text" name="descricao" descricao="descricao" id="input-descricao" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" placeholder="{{ __('Descrição') }}" value="{{ old('descricao', $unidade->descricao) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'descricao'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de endereço--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Endereço do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('cep') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cep">{{ __('CEP') }}</label>
                                            <input type="text" name="cep" cep="cep" id="input-cep" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" value="{{ old('cep', $unidade->endereco->cep) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cep'])
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="form-group{{ $errors->has('logradouro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-logradouro">{{ __('Logradouro') }}</label>
                                            <input type="text" name="logradouro" logradouro="logradouro" id="input-logradouro" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" value="{{ old('logradouro', $unidade->endereco->logradouro) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'logradouro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('numero') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero">{{ __('Numero') }}</label>
                                            <input type="text" name="numero" numero="numero" id="input-numero" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero') }}" value="{{ old('numero', $unidade->endereco->numero) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('bairro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-bairro">{{ __('Bairro') }}</label>
                                            <input type="text" name="bairro" bairro="bairro" id="input-bairro" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" value="{{ old('bairro', $unidade->endereco->bairro) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'bairro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('complemento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-complemento">{{ __('Complemento') }}</label>
                                            <input type="text" name="complemento" complemento="complemento" id="input-complemento" class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" value="{{ old('complemento', $unidade->endereco->complemento) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'complemento'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cidade">{{ __('Cidade') }}</label>
                                            <input type="text" name="cidade" cidade="cidade" id="input-cidade" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Cidade') }}" value="{{ old('cidade', $unidade->endereco->cidade) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estado">{{ __('Estado') }}</label>
                                            <input type="text" name="estado" estado="estado" id="input-estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado') }}" value="{{ old('estado', $unidade->endereco->estado) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'estado'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de contato--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Contato do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('E-mail') }}</label>
                                            <input type="text" name="email" email="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-mail') }}" value="{{ old('email', $unidade->contato->email) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'email'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                            <input type="text" name="telefone" telefone="telefone" id="input-telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone', $unidade->contato->telefone) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'telefone'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                            <input type="text" name="celular" celular="celular" id="input-celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular', $unidade->contato->celular) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'celular'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
