@extends('layouts.app', [
    'namePage' => 'Dashboard Avaliação',
    'class' => 'sidebar-mini',
    'activePage' => 'sessao',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h5 class="mb-0">{{ $cliente->nome }}</h5>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $cliente->id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <input id="clienteId" type="hidden" value="{{ $cliente->id }}">
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 style="text-align: center">IMC</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="imcGrafico" width="400" height="200"></canvas>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 style="text-align: center">Peso</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="graficoAvaliacao" width="400" height="200"></canvas>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 style="text-align: center">Porcentagem</h5>
                </div>
                    <div class="card-body">
                        <canvas id="graficoPorcentagem" width="400" height="200"></canvas>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 style="text-align: center">Medidas</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="graficoMedidas" width="400" height="200"></canvas>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>
    </div>
@endsection
