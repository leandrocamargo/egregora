@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Avaliação',
    'activePage' => 'avaliacao',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Avaliações') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $avaliacao->cliente_id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('avaliacao.update', $avaliacao) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações da Avaliação') }}</h6>

                                <div class="col-lg-4 col-sm-4 col-xs-12" id="form0">
                                    <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                        <label for="exampleFormControlSelect1">Cliente</label>
                                        <input name="cliente_id" class="form-control" id="exampleFormControlSelect1" value="{{ $avaliacao->cliente->nome }}" disabled>
                                        <input type="hidden" name="cliente_id" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" id="exampleFormControlSelect1" value="{{ $avaliacao->cliente_id }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cliente_id'])
                                    </div>
                                </div>

                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">1- Peso*</label><br />
                                        <input type="text"  name="peso" value="{{$avaliacao->peso}}" class="form-control{{ $errors->has('peso') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'peso'])
                                    </div>
                                </div>

                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">Altura*</label><br />
                                        <input type="text"  name="altura" value="{{$avaliacao->altura}}" class="form-control{{ $errors->has('altura') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'altura'])
                                    </div>
                                </div>

                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">2- Porcentagem Gordura*</label><br />
                                        <input type="text"  name="porc_gordura" value="{{$avaliacao->porc_gordura}}" class="form-control{{ $errors->has('porc_gordura') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'porc_gordura'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">3- Porcentagem Musculo*</label><br />
                                        <input type="text"  name="porc_musculo" value="{{$avaliacao->porc_musculo}}" class="form-control{{ $errors->has('porc_musculo') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'porc_musculo'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">5- Observação*</label><br />
                                        <input type="text"  name="observacao" value="{{$avaliacao->observacao}}" class="form-control{{ $errors->has('observacao') ? ' is-invalid' : '' }}">
                                    </div>

                                    @include('alerts.feedback', ['field' => 'observacao'])
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">6- Medida Acima*</label><br />
                                        <input type="text"  name="med_acima" value="{{$avaliacao->med_acima}}" class="form-control{{ $errors->has('med_acima') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'med_acima'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">7- Medida Umbigo*</label><br />
                                        <input type="text"  name="med_umbigo" value="{{$avaliacao->med_umbigo}}" class="form-control{{ $errors->has('med_umbigo') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'med_umbigo'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">8- Medida Abaixo*</label><br />
                                        <input type="text"  name="med_abaixo" value="{{$avaliacao->med_abaixo}}" class="form-control{{ $errors->has('med_abaixo') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'med_abaixo'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">9- Medida Quadril*</label><br />
                                        <input type="text"  name="med_quadril" value="{{$avaliacao->med_quadril}}" class="form-control{{ $errors->has('med_quadril') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'med_quadril'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">10- Medida entre Coxas*</label><br />
                                        <input type="text"  name="med_entre_coxas" value="{{$avaliacao->med_entre_coxas}}" class="form-control{{ $errors->has('med_entre_coxas') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'med_entre_coxas'])
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection






