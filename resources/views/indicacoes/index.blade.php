@extends('layouts.app', [
    'namePage' => 'Indicações',
    'class' => 'sidebar-mini',
    'activePage' => 'indicacoes',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('Indicações') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('Profissional') }}</th>
                                <th>{{ __('Cliente') }}</th>
                                <th>{{ __('Serviço') }}</th>
                                <th>{{ __('Pacote') }}</th>
                                <th>{{ __('Valor') }}</th>
                                <th>{{ __('Data Indicação') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Data Fechamento') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Ação') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($indicacoes as $indicacao)
                                <tr>
                                    <td>{{$indicacao->usuario->name}}</td>
                                    <td>{{$indicacao->contrato->cliente->nome}}</td>

                                    @if(isset($indicacao->servico->nome))
                                        <td>{{$indicacao->servico->nome}}</td>
                                    @else
                                        <td></td>
                                    @endif

                                    @if(isset($indicacao->pacote->nome_pacote))
                                        <td>{{$indicacao->pacote->nome_pacote}}</td>
                                    @else
                                        <td></td>
                                    @endif

                                    <td style="text-align: center">R$ {{$indicacao->valor}}</td>

                                    <td>{{ $indicacao->created_at->format('d/m/Y H:i') }}</td>
                                    <td>{{$indicacao->contrato->status}}</td>

                                    @if(isset($indicacao->contrato->data_fechou))
                                        <td style="text-align: center">{{ date('d/m/Y', strtotime($indicacao->contrato->data_fechou)) }}</td>
                                    @else
                                        <td></td>
                                    @endif

                                    <td class="text-right">
                                        <a type="button" href="#" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            {{ $indicacoes->links() }}
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".delete-button").click(function(){
                var clickedButton = $( this );
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        clickedButton.parents(".delete-form").submit();
                    }
                })

            });
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function() {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function(e) {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function() {
                alert('You clicked on Like button');
            });
        });
    </script>
@endpush


