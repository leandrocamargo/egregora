@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Formas de Pagamentos',
    'activePage' => 'forma-pagamento',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Formas de Pagamentos') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('forma_pagamento.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('forma_pagamento.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações da Forma de Pagamento') }}</h6>
                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Nome') }}</label>
                                            <input type="text" name="nome" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('nome') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>

                                    <div class="input-group col-md-2 m-4">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="checkbox" aria-label="Checkbox for following text input" name="bandeira" id="input-bandeira">
                                            </div>
                                        </div>
                                        <label type="text" class="form-control" aria-label="Text input with checkbox">{{ __('Bandeira') }}</label>
                                    </div>

                                    <div class="input-group col-md-2 m-4">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="checkbox" aria-label="Checkbox for following text input" name="parcela" id="input-parcela">
                                            </div>
                                        </div>
                                        <label type="text" class="form-control" aria-label="Text input with checkbox">{{ __('Parcela') }}</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group{{ $errors->has('observacao') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-observacao">{{ __('Observação') }}</label>
                                            <input type="text" name="observacao" observacao="observacao" id="input-observacao" class="form-control{{ $errors->has('observacao') ? ' is-invalid' : '' }}" placeholder="{{ __('Observação') }}" value="{{ old('observacao') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'observacao'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




