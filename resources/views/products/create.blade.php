@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Cadastro de Produtos',
    'activePage' => 'produto',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Produtos') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('produto.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('produto.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do Produto') }}</h6>
                                <div class="row">
                                    <div class="col-md-8 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Nome') }}</label>
                                            <input type="text" name="nome" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('nome') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('preco') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-preco">{{ __('Preço') }}</label>
                                            <input type="text" name="preco" preco="preco" id="input-preco" class="form-control{{ $errors->has('preco') ? ' is-invalid' : '' }}" placeholder="{{ __('Preço') }}" value="{{ old('preco') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'preco'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('estoque') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estoque">{{ __('Estoque') }}</label>
                                            <input type="number" name="estoque" estoque="estoque" id="input-estoque" class="form-control{{ $errors->has('estoque') ? ' is-invalid' : '' }}" placeholder="{{ __('Estoque') }}" value="{{ old('estoque') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'estoque'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 pr-1">
                                        <div class="form-group{{ $errors->has('descricao') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-descricao">{{ __('Descrição') }}</label>
                                            <input type="tex" name="descricao" descricao="descricao" id="input-descricao" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" placeholder="{{ __('Descrição') }}" value="{{ old('descricao') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'descricao'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-control-label" for="input-imagem">{{ __('Imagem') }}</label>
                                        <input type="file" name="imagem" imagem="imagem" id="input-imagem" class="form-control{{ $errors->has('imagem') ? ' is-invalid' : '' }}" placeholder="{{ __('Imagem') }}" value="{{ old('imagem') }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'imagem'])
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



