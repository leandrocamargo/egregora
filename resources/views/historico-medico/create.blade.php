@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Histórico Médico',
    'activePage' => 'historico-medico',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Histórico Médico') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $cliente->id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('historico_medico.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações para o Histórico Médico') }}</h6>

                                <div class="col-lg-4 col-sm-4 col-xs-12" id="form0">
                                    <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                        <label for="exampleFormControlSelect1">Cliente</label>
                                        <input name="cliente_id" class="form-control" id="exampleFormControlSelect1" value="{{ $cliente->nome }}" disabled>
                                        <input type="hidden" name="cliente_id" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" id="exampleFormControlSelect1" value="{{ $cliente->id }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cliente_id'])
                                    </div>
                                </div>

                                <div id="form01">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">1- Tratamento médico atual*</label><br />
                                            <label for="tratMedSim" class="radio-inline"><input type="radio" name="trat_med_atual" id="tratMedS" value="1" class="radio-inline"> Sim</label>
                                            <label for="tratMedNao" class="radio-inline"><input type="radio" name="trat_med_atual" id="tratMedN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'trat_med_atual'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="medicUso">
                                        <div class="form-group">
                                            <label for="medicUso">Medicamento em uso:</label>
                                            <input type="text" class="form-control" name="trat_med_atual_medic" value="">

                                            @include('alerts.feedback', ['field' => 'trat_med_atual_medic'])
                                        </div>
                                    </div>
                                </div>

                                <div id="form02">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">2- Antecedentes Alergicos*</label><br />
                                            <label for="antSim" class="radio-inline"><input type="radio" name="antec_alergico" id="antS" value="1" class="radio-inline"> Sim</label>
                                            <label for="antNao" class="radio-inline"><input type="radio" name="antec_alergico" id="antN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'antec_alergico'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="antqual">
                                        <div class="form-group">
                                            <label for="antqual">Qual</label>
                                            <input type="text" class="form-control" name="antec_alergico_qual" value="">

                                            @include('alerts.feedback', ['field' => 'antec_alergico_qual'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form03">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">3- Portador de Marcapasso?*</label><br />
                                            <label for="marcSim" class="radio-inline"><input type="radio" name="port_marcapasso" id="marc" value="1" class="radio-inline"> Sim</label>
                                            <label for="marcNao" class="radio-inline"><input type="radio" name="port_marcapasso" id="marc" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'port_marcapasso'])
                                        </div>
                                    </div>
                                </div>

                                <div id="form04">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">4- Alterações Cardíacas*</label><br />
                                            <label for="altSim" class="radio-inline"><input type="radio" name="alt_cardiacas" id="alt" value="1" class="radio-inline"> Sim</label>
                                            <label for="altNao" class="radio-inline"><input type="radio" name="alt_cardiacas" id="alt" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'alt_cardiacas'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form05">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">5- Alteração na pressão arterial*</label><br />
                                            <label for="pressaoSim" class="radio-inline"><input type="radio" name="alt_pres_arterial" id="pressao" value="1" class="radio-inline"> Sim</label>
                                            <label for="pressaoNao" class="radio-inline"><input type="radio" name="alt_pres_arterial" id="pressao" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'alt_pres_arterial'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form06">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">6- Distúrbio Circulatório*</label><br />
                                            <label for="distSim" class="radio-inline"><input type="radio" name="dis_circulatorio" id="distS" value="1" class="radio-inline"> Sim</label>
                                            <label for="distNao" class="radio-inline"><input type="radio" name="dis_circulatorio" id="distN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'dis_circulatorio'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="dist">
                                        <div class="form-group">
                                            <label for="dist">Qual:</label>
                                            <input type="text" class="form-control" name="dis_circulatorio_qual" value="">

                                            @include('alerts.feedback', ['field' => 'dis_circulatorio_qual'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form07">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">7- Distúrbio Renal*</label><br />
                                            <label for="distRSim" class="radio-inline"><input type="radio" name="dis_renal" id="distRs" value="1" class="radio-inline"> Sim</label>
                                            <label for="distRNao" class="radio-inline"><input type="radio" name="dis_renal" id="distRn" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'dis_renal'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disRqual">
                                        <div class="form-group">
                                            <label for="disRqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_renal_qual" value="">

                                            @include('alerts.feedback', ['field' => 'dis_renal_qual'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form08">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">8- Distúrbio Hormonal*</label><br />
                                            <label for="distHSim" class="radio-inline"><input type="radio" name="dis_hormonal" id="distHs" value="1" class="radio-inline"> Sim</label>
                                            <label for="distHNao" class="radio-inline"><input type="radio" name="dis_hormonal" id="distHn" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'dis_hormonal'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disHqual">
                                        <div class="form-group">
                                            <label for="disHqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_hormonal_qual" value="">

                                            @include('alerts.feedback', ['field' => 'dis_hormonal_qual'])
                                        </div>
                                    </div>
                                </div>

                                <div id="form09">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">9- Distúrbio Gastro-Intestinal*</label><br />
                                            <label for="distGSim" class="radio-inline"><input type="radio" name="dis_gastro" id="distGs" value="1" class="radio-inline"> Sim</label>
                                            <label for="distGNao" class="radio-inline"><input type="radio" name="dis_gastro" id="distGn" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'dis_gastro'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disGqual">
                                        <div class="form-group">
                                            <label for="disGqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_gastro_qual" value="">

                                            @include('alerts.feedback', ['field' => 'dis_gastro_qual'])
                                        </div>
                                    </div>
                                </div>

                                <div id="form010">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">10- Epilepsia/Convulsões*</label><br />
                                            <label for="epconv" class="radio-inline"><input type="radio" name="epilepsia_convulsao" id="epconvS" value="1" class="radio-inline"> Sim</label>
                                            <label for="epconv" class="radio-inline"><input type="radio" name="epilepsia_convulsao" id="epconvN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'epilepsia_convulsao'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="epconvFreq">
                                        <div class="form-group">
                                            <label for="epconvFreq">Frequência:</label>
                                            <input type="text" class="form-control" name="epilepsia_convulsao_freq" value="">

                                            @include('alerts.feedback', ['field' => 'epilepsia_convulsao_freq'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form011">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">11- Labirintite *</label><br />
                                            <label for="labSim" class="radio-inline"><input type="radio" name="labirintite" id="labirintite" value="1" class="radio-inline"> Sim</label>
                                            <label for="labNao" class="radio-inline"><input type="radio" name="labirintite" id="labirintite" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'labirintite'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form012">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">12- Alterações Psicológicas/Psiquiátricas *</label><br />
                                            <label for="altPSim" class="radio-inline"><input type="radio" name="alteracao_psi" id="altPs" value="1" class="radio-inline"> Sim</label>
                                            <label for="altPNao" class="radio-inline"><input type="radio" name="alteracao_psi" id="altPn" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'alteracao_psi'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="altPQual">
                                        <div class="form-group">
                                            <label for="altPQual">Qual:</label>
                                            <input type="text" class="form-control" name="alteracao_psi_qual" value="">

                                            @include('alerts.feedback', ['field' => 'alteracao_psi_qual'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form013">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">13- Estresse *</label><br />
                                            <label for="estresseSim" class="radio-inline"><input type="radio" name="estresse" id="estresse" value="1" class="radio-inline"> Sim</label>
                                            <label for="estresseNao" class="radio-inline"><input type="radio" name="estresse" id="estresse" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'estresse'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="estresseObs">
                                        <div class="form-group">
                                            <label for="estresseObs">Obs:</label>
                                            <input type="text" class="form-control" name="estresse_obs" value="">

                                            @include('alerts.feedback', ['field' => 'estresse_obs'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form014">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">14- Antecedentes Oncológicos*</label><br />
                                            <label for="antOncSim" class="radio-inline"><input type="radio" name="ant_oncologicos" id="antOncS" value="1" class="radio-inline"> Sim</label>
                                            <label for="antOncNao" class="radio-inline"><input type="radio" name="ant_oncologicos" id="antOncN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'ant_oncologicos'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="ant_onc_qual">
                                        <div class="form-group">
                                            <label for="ant_oncologicos_qual">Qual:</label>
                                            <input type="text" class="form-control" name="ant_oncologicos_qual" value="">

                                            @include('alerts.feedback', ['field' => 'ant_oncologicos_qual'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form015">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">15- Diabetes*</label><br />
                                            <label for="diabeteSim" class="radio-inline"><input type="radio" name="diabetes" id="diabetesS" value="1" class="radio-inline"> Sim</label>
                                            <label for="diabeteNao" class="radio-inline"><input type="radio" name="diabetes" id="diabetesN" value="0" class="radio-inline"> Não</label>

                                            @include('alerts.feedback', ['field' => 'diabetes'])
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="diabetes_tipo">
                                        <div class="form-group">
                                            <label for="diabetes_tipo">Tipo:</label>
                                            <input type="text" class="form-control" name="diabetes_tipo" value="">

                                            @include('alerts.feedback', ['field' => 'diabetes_tipo'])
                                        </div>
                                    </div>
                                </div>
                                <div id="form016">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">16- Alguma outra doença?*</label><br />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="outra_doenca">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="outra_doenca" value="">

                                            @include('alerts.feedback', ['field' => 'outra_doenca'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





