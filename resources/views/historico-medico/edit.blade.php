@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Histórico Médico',
    'activePage' => 'historico-medico',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Histórico Médico') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $historico_medico->cliente_id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('historico_medico.update', $historico_medico) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações para o Histórico Médico') }}</h6>

                                <div class="col-lg-4 col-sm-4 col-xs-12" id="form0">
                                    <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                        <label for="exampleFormControlSelect1">Cliente</label>
                                        <input name="cliente_id" class="form-control" id="exampleFormControlSelect1" value="{{ $historico_medico->cliente->nome }}" disabled>
                                        <input type="hidden" name="cliente_id" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" id="exampleFormControlSelect1" value="{{ $historico_medico->cliente_id }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cliente_id'])
                                    </div>
                                </div>

                                <div id="form01">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">1- Tratamento médico atual*</label><br />
                                            <label for="tratMedSim" class="radio-inline"><input type="radio" name="trat_med_atual" id="tratMedSim" value="1" class="radio-inline" <?php  if ( $historico_medico->trat_med_atual == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="tratMedNao" class="radio-inline"><input type="radio" name="trat_med_atual" id="tratMedNao" value="0" class="radio-inline" <?php  if ( $historico_medico->trat_med_atual == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="medicUsoEdit">
                                        <div class="form-group">
                                            <label for="medicUso">Medicamento em uso:</label>
                                            <input type="text" class="form-control" name="trat_med_atual_medic" value="{{$historico_medico->trat_med_atual_medic}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form02">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">2- Antecedentes Alergicos*</label><br />
                                            <label for="antSim" class="radio-inline"><input type="radio" name="antec_alergico" id="antSim" value="1" class="radio-inline" <?php  if ( $historico_medico->antec_alergico == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="antNao" class="radio-inline"><input type="radio" name="antec_alergico" id="antNao" value="0" class="radio-inline" <?php  if ( $historico_medico->antec_alergico == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="antqualEdit">
                                        <div class="form-group">
                                            <label for="antqual">Qual</label>
                                            <input type="text" class="form-control" name="antec_alergico_qual" value="{{$historico_medico->antec_alergico_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form03">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">3- Portador de Marcapasso?*</label><br />
                                            <label for="marcSim" class="radio-inline"><input type="radio" name="port_marcapasso" id="marc" value="1" class="radio-inline" <?php  if ( $historico_medico->port_marcapasso == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="marcNao" class="radio-inline"><input type="radio" name="port_marcapasso" id="marc" value="0" class="radio-inline" <?php  if ( $historico_medico->port_marcapasso == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                </div>

                                <div id="form04">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">4- Alterações Cardíacas*</label><br />
                                            <label for="altSim" class="radio-inline"><input type="radio" name="alt_cardiacas" id="alt" value="1" class="radio-inline"<?php  if ( $historico_medico->alt_cardiacas == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="altNao" class="radio-inline"><input type="radio" name="alt_cardiacas" id="alt" value="0" class="radio-inline" <?php  if ( $historico_medico->alt_cardiacas == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="form05">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">5- Alteração na pressão arterial*</label><br />
                                            <label for="pressaoSim" class="radio-inline"><input type="radio" name="alt_pres_arterial" id="pressao" value="1" class="radio-inline" <?php  if ( $historico_medico->alt_pres_arterial == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="pressaoNao" class="radio-inline"><input type="radio" name="alt_pres_arterial" id="pressao" value="0" class="radio-inline"<?php  if ( $historico_medico->alt_pres_arterial == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="form06">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">6- Distúrbio Circulatório*</label><br />
                                            <label for="distSim" class="radio-inline"><input type="radio" name="dis_circulatorio" id="distSim" value="1" class="radio-inline" <?php  if ( $historico_medico->dis_circulatorio == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="distNao" class="radio-inline"><input type="radio" name="dis_circulatorio" id="distNao" value="0" class="radio-inline" <?php  if ( $historico_medico->dis_circulatorio == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="distEdit">
                                        <div class="form-group">
                                            <label for="dist">Qual:</label>
                                            <input type="text" class="form-control" name="dis_circulatorio_qual" value="{{$historico_medico->dis_circulatorio_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form07">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">7- Distúrbio Renal*</label><br />
                                            <label for="distRSim" class="radio-inline"><input type="radio" name="dis_renal" id="distRsim" value="1" class="radio-inline" <?php  if ( $historico_medico->dis_renal == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="distRNao" class="radio-inline"><input type="radio" name="dis_renal" id="distRnao" value="0" class="radio-inline" <?php  if ( $historico_medico->dis_renal == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disRqualEdit">
                                        <div class="form-group">
                                            <label for="disRqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_renal_qual" value="{{$historico_medico->dis_renal_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form08">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">8- Distúrbio Hormonal*</label><br />
                                            <label for="distHSim" class="radio-inline"><input type="radio" name="dis_hormonal" id="distHsim" value="1" class="radio-inline" <?php  if ( $historico_medico->dis_hormonal == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="distHNao" class="radio-inline"><input type="radio" name="dis_hormonal" id="distHnao" value="0" class="radio-inline" <?php  if ( $historico_medico->dis_hormonal == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disHqualEdit">
                                        <div class="form-group">
                                            <label for="disHqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_hormonal_qual" value="{{$historico_medico->dis_hormonal_qual}}">
                                        </div>
                                    </div>
                                </div>

                                <div id="form09">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">9- Distúrbio Gastro-Intestinal*</label><br />
                                            <label for="distGSim" class="radio-inline"><input type="radio" name="dis_gastro" id="distGsim" value="1" class="radio-inline" <?php  if ( $historico_medico->dis_gastro == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="distGNao" class="radio-inline"><input type="radio" name="dis_gastro" id="distGnao" value="0" class="radio-inline" <?php  if ( $historico_medico->dis_gastro == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="disGqualEdit">
                                        <div class="form-group">
                                            <label for="disGqual">Qual:</label>
                                            <input type="text" class="form-control" name="dis_gastro_qual" value="{{$historico_medico->dis_gastro_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form010">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">10- Epilepsia/Convulsões*</label><br />
                                            <label for="epconv" class="radio-inline"><input type="radio" name="epilepsia_convulsao" id="epconvSim" value="1" class="radio-inline" <?php  if ( $historico_medico->epilepsia_convulsao == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="epconv" class="radio-inline"><input type="radio" name="epilepsia_convulsao" id="epconvNao" value="0" class="radio-inline" <?php  if ( $historico_medico->epilepsia_convulsao == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="epconvFreqEdit">
                                        <div class="form-group">
                                            <label for="epconvFreq">Frequência:</label>
                                            <input type="text" class="form-control" name="epilepsia_convulsao_freq" value="{{$historico_medico->epilepsia_convulsao_freq}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form011">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">11- Labirintite *</label><br />
                                            <label for="labSim" class="radio-inline"><input type="radio" name="labirintite" id="labirintite" value="1" class="radio-inline" <?php  if ( $historico_medico->labirintite == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="labNao" class="radio-inline"><input type="radio" name="labirintite" id="labirintite" value="0" class="radio-inline" <?php  if ( $historico_medico->labirintite == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="form012">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">12- Alterações Psicológicas/Psiquiátricas *</label><br />
                                            <label for="altPSim" class="radio-inline"><input type="radio" name="alteracao_psi" id="altPsim" value="1" class="radio-inline" <?php  if ( $historico_medico->alteracao_psi == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="altPNao" class="radio-inline"><input type="radio" name="alteracao_psi" id="altPnao" value="0" class="radio-inline" <?php  if ( $historico_medico->alteracao_psi == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="altPQualEdit">
                                        <div class="form-group">
                                            <label for="altPQual">Qual:</label>
                                            <input type="text" class="form-control" name="alteracao_psi_qual" value="{{$historico_medico->alteracao_psi_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form013">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">13- Estresse *</label><br />
                                            <label for="estresseSim" class="radio-inline"><input type="radio" name="estresse" id="estresse" value="1" class="radio-inline" <?php  if ( $historico_medico->estresse == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="estresseNao" class="radio-inline"><input type="radio" name="estresse" id="estresse" value="0" class="radio-inline" <?php  if ( $historico_medico->estresse == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="estresseObs">
                                        <div class="form-group">
                                            <label for="estresseObs">Obs:</label>
                                            <input type="text" class="form-control" name="estresse_obs" value="{{$historico_medico->estresse_obs}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form014">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">14- Antecedentes Oncológicos*</label><br />
                                            <label for="antOncSim" class="radio-inline"><input type="radio" name="ant_oncologicos" id="antOncSim" value="1" class="radio-inline" <?php  if ( $historico_medico->ant_oncologicos == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="antOncNao" class="radio-inline"><input type="radio" name="ant_oncologicos" id="antOncNao" value="0" class="radio-inline" <?php  if ( $historico_medico->ant_oncologicos == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="ant_onc_qualEdit">
                                        <div class="form-group">
                                            <label for="ant_oncologicos_qual">Qual:</label>
                                            <input type="text" class="form-control" name="ant_oncologicos_qual" value="{{$historico_medico->ant_oncologicos_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form015">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">15- Diabetes*</label><br />
                                            <label for="diabeteSim" class="radio-inline"><input type="radio" name="diabetes" id="diabetesSim" value="1" class="radio-inline" <?php  if ( $historico_medico->diabetes == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="diabeteNao" class="radio-inline"><input type="radio" name="diabetes" id="diabetesNao" value="0" class="radio-inline" <?php  if ( $historico_medico->diabetes == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="diabetes_tipoEdit">
                                        <div class="form-group">
                                            <label for="diabetes_tipo">Tipo:</label>
                                            <input type="text" class="form-control" name="diabetes_tipo" value="{{$historico_medico->diabetes_tipo}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form016">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">16- Alguma outra doença?*</label><br />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="outra_doenca">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="outra_doenca" value="{{$historico_medico->outra_doenca}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





