@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Criar usuário',
    'activePage' => 'user',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Usuários') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('usuario.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('usuario.store') }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do Usuário') }}</h6>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Nome') }}</label>
                                            <input type="text" name="name" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('nome') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                            <input type="email" name="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email') }}" required>

                                            @include('alerts.feedback', ['field' => 'email'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('unidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-unidade">{{ __('Unidade') }}</label>
                                            <select type="text" name="unidade" unidade="unidade" id="input-unidade" class="form-control{{ $errors->has('unidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Unidade') }}" value="{{ old('unidade') }}" required autofocus>
                                                <option>Selecione uma unidade</option>
                                                @foreach($unidades as $unidade)
                                                    <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'unidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('tipo_usuario') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-tipo_usuario">{{ __('Tipo de usuário') }}</label>
                                            <select type="text" name="tipo_usuario" tipo_usuario="tipo_usuario" id="input-tipo_usuario" class="form-control{{ $errors->has('tipo_usuario') ? ' is-invalid' : '' }}" placeholder="{{ __('Tipo de usuário') }}" value="{{ old('tipo_usuario') }}" required autofocus>
                                                <option>Selecione um tipo de usuário</option>
                                                @foreach(\App\User::TIPO as $tipo_user)
                                                    <option value="{{ $tipo_user }}">{{ $tipo_user }}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'tipo_usuario'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de endereço--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Endereço do Usuário') }}</h6>

                                <div class="row">
                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('cep') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cep">{{ __('CEP') }}</label>
                                            <input type="text" name="cep" cep="cep" id="input-cep" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" value="{{ old('cep') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'cep'])
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="form-group{{ $errors->has('logradouro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-logradouro">{{ __('Logradouro') }}</label>
                                            <input type="text" name="logradouro" logradouro="logradouro" id="input-logradouro" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" value="{{ old('logradouro') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'logradouro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('numero') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero">{{ __('Numero') }}</label>
                                            <input type="text" name="numero" numero="numero" id="input-numero" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero') }}" value="{{ old('numero') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('bairro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-bairro">{{ __('Bairro') }}</label>
                                            <input type="text" name="bairro" bairro="bairro" id="input-bairro" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" value="{{ old('bairro') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'bairro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('complemento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-complemento">{{ __('Complemento') }}</label>
                                            <input type="text" name="complemento" complemento="complemento" id="input-complemento" class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" value="{{ old('complemento') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'complemento'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cidade">{{ __('Cidade') }}</label>
                                            <input type="text" name="cidade" cidade="cidade" id="input-cidade" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Cidade') }}" value="{{ old('cidade') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'cidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estado">{{ __('Estado') }}</label>
                                            <input type="text" name="estado" estado="estado" id="input-estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado') }}" value="{{ old('estado') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'estado'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de contato--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Contato do Usuário') }}</h6>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                            <input type="text" name="telefone" telefone="telefone" id="input-telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'telefone'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                            <input type="text" name="celular" celular="celular" id="input-celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'celular'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de senha do usuário--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Senha do usuário') }}</h6>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-password">{{ __('Senha') }}</label>
                                            <input type="password" name="password" id="input-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha') }}" value="" required>

                                            @include('alerts.feedback', ['field' => 'password'])
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-password-confirmation">{{ __('Confirme Senha') }}</label>
                                            <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control" placeholder="{{ __('Confirme a Senha') }}" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
