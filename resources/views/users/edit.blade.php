@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Editar usuário',
    'activePage' => 'user',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Usuários') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('usuario.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('usuario.update', $usuario) }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informação do Usuário') }}</h6>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                            <input type="text" name="name" id="input-name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('name', $usuario->name) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'name'])
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                            <input type="email" name="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', $usuario->email) }}" required>
                                            @include('alerts.feedback', ['field' => 'email'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('unidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-unidade">{{ __('Unidade') }}</label>
                                            <select type="text" name="unidade" unidade="unidade" id="input-unidade" class="form-control{{ $errors->has('unidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Unidade') }}" value="{{ old('unidade', $usuario->unidade->nome) }}" required>
                                                <option value="{{ old('unidade', $usuario->unidade->id) }}">{{ old('unidade', $usuario->unidade->nome) }}</option>
                                                @foreach($unidades as $unidade)
                                                    <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'unidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('tipo_usuario') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-tipo_usuario">{{ __('Tipo de usuário') }}</label>
                                            <select type="text" name="tipo_usuario" tipo_usuario="tipo_usuario" id="input-tipo_usuario" class="form-control{{ $errors->has('tipo_usuario') ? ' is-invalid' : '' }}" placeholder="{{ __('Tipo de usuário') }}" value="{{ old('tipo_usuario', $usuario->tipo) }}" required>
                                                <option value="{{ old('tipo_usuario', $usuario->tipo) }}">{{ old('tipo_usuario', $usuario->tipo) }}</option>
                                                @foreach(\App\User::TIPO as $tipo_user)
                                                    <option value="{{ $tipo_user }}">{{ $tipo_user }}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'tipo_usuario'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Editar endereço--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Endereço do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('cep') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cep">{{ __('CEP') }}</label>
                                            <input type="text" name="cep" cep="cep" id="input-cep" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" value="{{ old('cep', $usuario->endereco->cep) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cep'])
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="form-group{{ $errors->has('logradouro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-logradouro">{{ __('Logradouro') }}</label>
                                            <input type="text" name="logradouro" logradouro="logradouro" id="input-logradouro" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" value="{{ old('logradouro', $usuario->endereco->logradouro) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'logradouro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('numero') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero">{{ __('Numero') }}</label>
                                            <input type="text" name="numero" numero="numero" id="input-numero" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero') }}" value="{{ old('numero', $usuario->endereco->numero) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('bairro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-bairro">{{ __('Bairro') }}</label>
                                            <input type="text" name="bairro" bairro="bairro" id="input-bairro" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" value="{{ old('bairro', $usuario->endereco->bairro) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'bairro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('complemento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-complemento">{{ __('Complemento') }}</label>
                                            <input type="text" name="complemento" complemento="complemento" id="input-complemento" class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" value="{{ old('complemento', $usuario->endereco->complemento) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'complemento'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cidade">{{ __('Cidade') }}</label>
                                            <input type="text" name="cidade" cidade="cidade" id="input-cidade" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Cidade') }}" value="{{ old('cidade', $usuario->endereco->cidade) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estado">{{ __('Estado') }}</label>
                                            <input type="text" name="estado" estado="estado" id="input-estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado') }}" value="{{ old('estado', $usuario->endereco->estado) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'estado'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Editar contato--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Contato do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                            <input type="text" name="telefone" telefone="telefone" id="input-telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone', $usuario->contato->telefone) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'telefone'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                            <input type="text" name="celular" celular="celular" id="input-celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular', $usuario->contato->celular) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'celular'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Editar senha do usuário--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Senha do usuário') }}</h6>

                                <div class="row">
                                    <div class="col-md-6 pr-1">

                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-password">{{ __('Senha') }}</label>
                                            <input type="password" name="password" id="input-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha') }}" value="">

                                            @include('alerts.feedback', ['field' => 'password'])
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-password-confirmation">{{ __('Confirm Password') }}</label>
                                            <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control" placeholder="{{ __('Confirme a Senha') }}" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
