@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Tratamentos Estéticos',
    'activePage' => 'tratamentos-estetico',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Tratamentos Estéticos') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $tratamento_estetico->cliente_id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('tratamento_estetico.update', $tratamento_estetico) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações para Tratamentos Estéticos') }}</h6>

                                <div class="col-lg-4 col-sm-4 col-xs-12" id="form0">
                                    <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                        <label for="exampleFormControlSelect1">Cliente</label>
                                        <input name="cliente_id" class="form-control" id="exampleFormControlSelect1" value="{{ $tratamento_estetico->cliente->nome }}" disabled>
                                        <input type="hidden" name="cliente_id" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" id="exampleFormControlSelect1" value="{{ $tratamento_estetico->cliente_id }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cliente_id'])
                                    </div>
                                </div>

                                <div id="form001">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">1- Implante Dentário*</label><br />
                                            <label for="impSim" class="radio-inline"><input type="radio" name="impl_dentario" id="impSim" value="1" class="radio-inline" <?php  if ( $tratamento_estetico->impl_dentario == 1 ) { echo "checked=\"checked\"";} else { echo "";}?> > Sim</label>
                                            <label for="impNao" class="radio-inline"><input type="radio" name="impl_dentario" id="impNao" value="0" class="radio-inline" <?php  if ( $tratamento_estetico->impl_dentario == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="impQualEdit">
                                        <div class="form-group">
                                            <label for="impQual">Qual:</label>
                                            <input type="text" class="form-control" name="impl_dentario_qual" value="{{$tratamento_estetico->impl_dentario_qual}}">
                                        </div>
                                    </div>
                                </div>

                                <div id="form002">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">2- Tratamento Dermatológico/Estético*</label><br />
                                            <label for="dermEstSim" class="radio-inline"><input type="radio" name="trat_dermato" id="dermEstSim" value="1" class="radio-inline" <?php  if ( $tratamento_estetico->trat_dermato == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="dermEstNao" class="radio-inline"><input type="radio" name="trat_dermato" id="dermEstNao" value="0" class="radio-inline" <?php  if ( $tratamento_estetico->trat_dermato == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="dermEstEdit">
                                        <div class="form-group">
                                            <label for="dermEst">Qual</label>
                                            <input type="text" class="form-control" name="trat_dermato_qual" value="{{$tratamento_estetico->trat_dermato_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form003">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">3- Cirurgia Plástica Estética?*</label><br />
                                            <label for="plastSim" class="radio-inline"><input type="radio" name="cir_plastica" id="plastSim" value="1" class="radio-inline"  <?php  if ( $tratamento_estetico->cir_plastica == 1 ) { echo "checked=\"checked\"";} else { echo "";}?> > Sim</label>
                                            <label for="plastNao" class="radio-inline"><input type="radio" name="cir_plastica" id="plastNao" value="0" class="radio-inline" <?php  if ( $tratamento_estetico->cir_plastica == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="plastEdit">
                                        <div class="form-group">
                                            <label for="plast">Qual</label>
                                            <input type="text" class="form-control" name="cir_plastica_qual" value="{{$tratamento_estetico->cir_plastica_qual}}">
                                        </div>
                                    </div>
                                </div>

                                <div id="form004">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">4- Cirurgia Reparadora*</label><br />
                                            <label for="repSim" class="radio-inline"><input type="radio" name="cir_reparadora" id="repSim" value="1" class="radio-inline"  <?php  if ( $tratamento_estetico->cir_reparadora == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                            <label for="repNao" class="radio-inline"><input type="radio" name="cir_reparadora" id="repNao" value="0" class="radio-inline" <?php  if ( $tratamento_estetico->cir_reparadora == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="repQualEdit">
                                        <div class="form-group">
                                            <label for="repQual">Qual</label>
                                            <input type="text" class="form-control" name="cir_reparadora_qual" value="{{$tratamento_estetico->cir_reparadora_qual}}">
                                        </div>
                                    </div>
                                </div>
                                <div id="form005">
                                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="">5- Outra Cirurgia?*</label><br />
                                            <label for="outraSim" class="radio-inline"><input type="radio" name="outra_cirurgia" id="outraSim" value="1" class="radio-inline"  <?php  if ( $tratamento_estetico->outra_cirurgia == 1 ) { echo "checked=\"checked\""; } else { echo "";}?>> Sim</label>
                                            <label for="outraNao" class="radio-inline"><input type="radio" name="outra_cirurgia" id="outraNao" value="0" class="radio-inline" <?php  if ( $tratamento_estetico->outra_cirurgia == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4" id="outraQualEdit">
                                        <div class="form-group">
                                            <label for="outraQual">Qual</label>
                                            <input type="text" class="form-control" name="outra_cirurgia_qual" value="{{$tratamento_estetico->outra_cirurgia_qual}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





