@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Cadastro de Novo Atendimento',
    'activePage' => 'atendimento',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Tipos Atendimentos') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('tipo-atendimento.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('tipo-atendimento.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do Novo Atendimento') }}</h6>
                                <div class="row">
                                    <div class="col-md-8 pr-1">
                                        <div class="form-group{{ $errors->has('descricao') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-descricao">{{ __('Descrição') }}</label>
                                            <input type="text" name="descricao" descricao="descricao" id="input-descricao" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" placeholder="{{ __('Descrição') }}" value="{{ old('descricao') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'descricao'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cor') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cor">{{ __('Cor') }}</label>
                                            <select type="tex" name="cor" cor="cor" id="input-cor" class="form-control{{ $errors->has('cor') ? ' is-invalid' : '' }}" placeholder="{{ __('Cor') }}" value="{{ old('cor') }}" required autofocus>
                                                <option>Selecione uma cor</option>
                                                @foreach(App\TipoAtendimento::COLOR as $chave => $color)
                                                    <option value="{{ $chave }}">{{ $color }}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'cor'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


