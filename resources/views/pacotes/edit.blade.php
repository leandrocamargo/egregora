@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Editar Cadastro de Pacotes',
    'activePage' => 'pacote',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm"></div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão dos Pacotes') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('pacote.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="{{ route('pacote.update', $pacote) }}" autocomplete="off" enctype="multipart/form-data" id="editarPacote">
                        <input type="hidden" name="deletados" id="deletados">
                            @csrf
                            @method('PUT')

                            <div class="row pl-lg-4">
                                <h6 class="col-12 heading-small text-muted mb-4">{{ __('Informações do Pacote') }}</h6>

                                <div class="col-12 col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="nome">Nome do Pacote</label>
                                        <input id="nome" type="text" class="form-control" name="nome_pacote" placeholder="Nome do Pacote" value="{{ $pacote->nome_pacote }}">
                                    </div>
                                </div>

                                <div class="col-12 col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="descricao">Descrição do pacote</label>
                                        <input id="descricao" type="text" class="form-control" name="descricao" placeholder="Descrição do Pacote" value="{{ $pacote->descricao }}">
                                    </div>
                                </div>

                                <div class="slot col-7">
                                    <div class="row" id="geralEdit">
                                        <div class="col-12 col-md-5">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Serviços</label>
                                                <select class="form-control serv" id="servico">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    @foreach ($servicos as $servico)
                                                        <option value="{{$servico->id}}">
                                                            {{$servico->nome}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-2">
                                            <div class="form-group">
                                                <label for="valor" id="valorLabel">Preço Unitário</label>
                                                <input id="valor" type="text" class="form-control" maxlength= 0  value="{{ old('valor') }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-2">
                                            <div class="form-group">
                                                <label for="qtd_sessao">Qtd de sessões</label>
                                                <input id="qtd_sessao" type="number" class="form-control" value="{{ old('qtd_sessao') }}">
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-1">
                                            <div class="md-form mt-3">
                                                <button type="button" class="btn btn-primary btn-sm btn-send-contato editar d-none" onclick="edit($(this))">Editar</button>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-1">
                                            <div class="md-form mt-3">
                                                <button type="button" class="btn btn-primary btn-sm btn-send-contato salvar d-none" onclick="salvar($(this))">Salvar</button>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-1">
                                            <div class="md-form mt-3">
                                                <button type="button" class="btn btn-primary btn-sm btn-send-contato excluir d-none" onclick="deletar($(this))">Excluir</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-1 mt-3">
                                    <button type="button" id="adicionarCreate" class="btn btn-primary btn-round btn-send-contato save" > + </button></td>
                                </div>

                                <div class="col-10 todoServico" name="servicos" id="">
                                    <div class="row col-12" id="servicos_list">
                                        @foreach($pacotesServicos as $pserv)
                                            @if($pserv->pivot->deleted_at == null)
                                                <div class="row">
                                                <div class="col-12 col-md-5">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">Serviços</label>
                                                        <select class="form-control serv" disabled id="servico">
                                                            <option value="">
                                                                Selecione
                                                            </option>
                                                            @foreach ($servicos as $serv)
                                                                <option {{ $pserv->pivot->servico_id == $serv->id ? "selected" : '' }} value="{{$serv->id}}">
                                                                    {{$serv->nome}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-md-2">
                                                    <div class="form-group md-form">
                                                        <label for="valor" class="active">Preço Unitário</label>
                                                        <input id="valor_edit" type="text" readonly class="form-control" maxlength="0" value="{{ $pserv->pivot->valor_sessao }}" name="valor[]">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <div class="form-group md-form">
                                                        <label for="qtd_sessao" class="active">Qtd de sessões</label>
                                                        <input id="qtd_sessao_edit" type="number" readonly class="form-control" value="{{ $pserv->pivot->qtd_sessao }}" name="qtd_sessao[]">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-1">
                                                    <div class="md-form mt-3">
                                                        <button type="button" class="btn btn-primary btn-sm btn-send-contato editar waves-effect waves-light" onclick="edit($(this))">Editar</button>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-1">
                                                    <div class="md-form mt-3">
                                                        <button type="button" class="btn btn-primary btn-sm btn-send-contato salvar waves-effect waves-light" onclick="salvar($(this))">Salvar</button>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-1">
                                                    <div class="md-form mt-3">
                                                        <button type="button" class="btn btn-primary btn-sm btn-send-contato excluir waves-effect waves-light" onclick="deletar($(this), {{$pserv->pivot->servico_id}})">Excluir</button>
                                                    </div>
                                                </div>

                                                @if(isset($pserv->pivot->id))
                                                    <input type="hidden" name="id_servicoIdPivot[]" value="{{ $pserv->pivot->id }}">
                                                @endif
                                                <input type="hidden" name="id_servico[]" value="{{ $pserv->id }}"></div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="bruto">Total Bruto</label>
                                        <input id="bruto" type="text" readonly class="form-control" name="bruto" value="{{ $pacote->bruto }}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="liquido">Desconto</label>
                                        <input id="desconto" type="text" class="form-control" name="desconto" placeholder="Desconto" value="{{ $pacote->desconto }}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="liquido">Total Líquido</label>
                                        <input id="liquido" type="text" class="form-control" name="valor_pacote" readonly value="{{ $pacote->valor_pacote }}">
                                    </div>
                                </div>

                                <div class="col-md-3 mt-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-send-contato btn-round" onclick="aplicar()">Aplicar</button>
                                    </div>
                                </div>

                                <div class="col-12 mt-4">
                                    <input type="file" name="imagem">
                                </div>

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

