@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Controle de Cheques',
    'activePage' => 'controle-cheque',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Controle de Cheques') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cheque.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('cheque.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do Cheque') }}</h6>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group{{ $errors->has('contrato_id') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-contrato_id">{{ __('Número do Contrato') }}</label>
                                            <input type="textt" name="contrato_id" contrato_id="contrato_id" id="input-contrato_id" class="form-control{{ $errors->has('contrato_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Número do Contrato') }}" value="{{ old('contrato_id') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'contrato_id'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cliente_id">{{ __('Cliente') }}</label>
                                            <input type="text" name="cliente_id" cliente_id="cliente_id" id="input-cliente_id" class="form-control{{ $errors->has('cliente_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Cliente') }}" value="{{ old('cliente_id') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'cliente_id'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group{{ $errors->has('banco') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-banco">{{ __('Banco') }}</label>
                                            <input type="text" name="banco" banco="banco" id="input-banco" class="form-control{{ $errors->has('banco') ? ' is-invalid' : '' }}" placeholder="{{ __('Banco') }}" value="{{ old('banco') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'banco'])
                                        </div>
                                    </div>

                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('numero_cheque') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero_cheque">{{ __('Número do Cheque') }}</label>
                                            <input type="text" name="numero_cheque" numero_cheque="numero_cheque" id="input-numero_cheque" class="form-control{{ $errors->has('numero_cheque') ? ' is-invalid' : '' }}" placeholder="{{ __('Número do Cheque') }}" value="{{ old('numero_cheque') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero_cheque'])
                                        </div>
                                    </div>

                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('numero_parcela') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero_parcela">{{ __('Número da Parcela') }}</label>
                                            <input type="text" name="numero_parcela" numero_parcela="numero_parcela" id="input-numero_parcela" class="form-control{{ $errors->has('numero_parcela') ? ' is-invalid' : '' }}" placeholder="{{ __('Número da Parcela') }}" value="{{ old('numero_parcela') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero_parcela'])
                                        </div>
                                    </div>

                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('data_vencimento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-data_vencimento">{{ __('Data de Vencimento') }}</label>
                                            <input type="date" name="data_vencimento" data_vencimento="data_vencimento" id="input-data_vencimento" class="form-control{{ $errors->has('data_vencimento') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Vencimento') }}" value="{{ old('data_vencimento') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'data_vencimento'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




