@extends('layouts.app', [
    'namePage' => 'Gestão de Contratos',
    'class' => 'sidebar-mini',
    'activePage' => 'contrato',
    'activeNav' => '',
])

@php
    function sanitizeString($string) {

        // matriz de entrada
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );

        // matriz de saída
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );

        // devolver a string
        return str_replace($what, $by, $string);
    }
@endphp

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('Gestão de Contratos') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('Data ') }}</th>
                                <th>{{ __('Cliente') }}</th>
                                <th>{{ __('Valor') }}</th>
                                <th>{{ __('Desconto') }}</th>
                                <th>{{ __('Pagamento') }}</th>
                                <th>{{ __('Parcela') }}</th>
                                <th>{{ __('Observação') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Ação') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contratos as $contrato)
                                <tr>
                                    <td>{{ date('d/m/Y', strtotime($contrato->data)) }}</td>
                                    <td>{{$contrato->cliente->nome}}</td>
                                    <td>{{$contrato->valor_total}}</td>
                                    <td>{{$contrato->desconto}}</td>
                                    @if(isset($contrato->formaPagamento->nome))
                                        <td>{{$contrato->formaPagamento->nome}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{$contrato->num_parcelas}}</td>
                                    <td>{{$contrato->observacao}}</td>
                                    @if($contrato->status === 'Aberto')
                                        <td class="text-danger text-center" title="Aberto" style="font-size: 20px"><i class="fas fa-question-circle"></i></td>
                                    @elseif($contrato->status === 'Fechado')
                                        <td class="text-success text-center" title="Fechado" style="font-size: 27px"><a href="{{ asset('uploads/contrato/'.strtolower(sanitizeString($contrato->cliente->nome)) . $contrato->data.".pdf") }}" target="_blank" ><i class="fas fa-file-pdf"></i></a></td>
                                    @elseif($contrato->status === 'Expirado')
                                        <td class="text-danger text-center" title="Expirado" style="font-size: 20px"><i class="fas fa-times-circle"></i></td>
                                    @else
                                        <td class="text-warning text-center" title="Orçamento" style="font-size: 20px"><a href="{{ asset('uploads/orcamento/'.strtolower(sanitizeString($contrato->cliente->nome)) . $contrato->data.".pdf") }}" target="_blank" ><i class="fas fa-exclamation-triangle"></i></td>
                                    @endif

                                    @if($contrato->status === 'Expirado')
                                        <td class="text-right">
                                            <a type="button" href="{{route("contratos.edit", $contrato)}}" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="" disabled>
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                        </td>
                                    @else
                                        <td class="text-right">
                                            <a type="button" href="{{route("contratos.edit", $contrato)}}" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                            {{$contratos->links()}}
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".delete-button").click(function(){
                var clickedButton = $( this );
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        clickedButton.parents(".delete-form").submit();
                    }
                })

            });
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function() {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function(e) {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function() {
                alert('You clicked on Like button');
            });
        });
    </script>
@endpush


