@extends('layouts.app')

@section('content')
<div class="container">
    <div class="bc-icons-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb orange lighten-4">
                <li class="breadcrumb-item">
                    <a class="black-text" href="{{ route('home') }}">
                        <i class="fas fa-home"></i>
                    </a>
                    <i class="fas fa-caret-right mx-2" aria-hidden="true"></i>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="black-text">
                        <i class="fas fa-users"></i>
                        <span>Contrato</span>
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <i class="fas fa-plus"></i>
                    Adicionar Contrato
                </li>
            </ol>
        </nav>
    </div>
</div>

<div class="col-md-12">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ session()->get('success') }}</h4>
        </div>
    @endif
    <div class="card">
        <form action="" method="POST" id="cadastrarContrato">
            @csrf
            <h5 class="card-header warning-color white-text text-center">
                <strong>Contrato</strong>
            </h5>
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <label for="exampleFormControlSelect1">Cliente</label>
                    <select name="id_cliente" class="form-control" id="exampleFormControlSelect1">
                        <option value="">
                            Selecione
                        </option>
                        @foreach ($cliente as $clie)
                            <option value="{{$clie->id}}">
                                {{$clie->nome}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="data">Data</label>
                        <input type="date" class="form-control" name="data"  placeholder="Data *" required="" value="<?php echo date('Y-m-d')?>" >
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="num_parcelas">Número de parcelas</label>
                        <input type="text" class="form-control" name="num_parcelas"  placeholder="Número de parcelas *" required="" value="" >
                    </div>
                </div>
                <div class="col-md-5  col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="valor_total">Valor Total</label>
                        <input type="text" class="form-control" name="valor_total"  placeholder="Valor Total *" required="" value="" >
                    </div>
                </div>
            </div>
            <h5 class="card-header warning-color white-text text-center">
                <strong>Adicionar Forma de Pagamento</strong>
            </h5>
            <div class="card-body px-lg-5 pt-2">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="md-form mt-3">
                            <input id="nome" type="text" class="form-control" name="nome" value="{{ old('nome') }}">
                            <label for="nome">Nome</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="md-form mt-3">
                            <input id="observ" type="text" class="form-control" name="observ" value="{{ old('observ') }}">
                            <label for="observ">Observação</label>
                        </div>
                    </div>
                </div>
                {{--<div class="card-footer text-right">--}}
                    {{--<button type="submit" class="btn btn-success btn-sm">Cadastrar</button>--}}
                {{--</div>--}}
            </div>
        </form>
    </div>
</div>

@endsection
