@php
    $valor = number_format(($contrato->valor_total - $contrato->desconto),2 ,',','.');
@endphp
<style>
    h4{
        text-align: center;
    }
    .data-final{
        text-align: right;
    }
    p{
        text-align: justify;
    }
    img{
        width: 20%;
    }
    h2{
        text-align: center;
        color: #000000;
        font-family: "Lato", sans-serif;
        margin-top: -95px;
    }
    h3{
        text-align: center;
    }

</style>

<div class="header">
    <div>
        <img src="imagens/logo.png" alt="">
        <h2>Contrato de Prestação de Serviços</h2>
    </div>

    <h3>Contratada:</h3>

    <div style="text-align: center"><strong>Egrégora Estética - Unidade {{ $unidade->nome }}</strong></div>

    <div>
        <p><strong>Contratante:</strong> {{ $cliente->nome }}</p>
        <p>
            <strong>Endereço:</strong> {{ $cliente['endereco']['logradouro'] }},
            {{ $cliente['endereco']['numero'] }},
            {{ $cliente['endereco']['bairro'] }},
            {{ $cliente['endereco']['complemento'] }},
            {{ $cliente['endereco']['cidade'] }} /
            {{ $cliente['endereco']['estado'] }} ,
            Cep: {{ $cliente['endereco']['cep'] }}
        </p>
        <p><strong>Telefone:</strong> {{ $cliente->contato['telefone'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <strong>Celular:</strong> {{ $cliente->contato['celular'] }}
        </p>
    </div>


    <div style="display: inline-block; width: 100%;">
        <div style="height: auto; width: 50%; display: block; float: left;">
            <table style="border: 1px outset black;">
                <thead>
                <tr style="display: inline-block;">
                    <th style="border: 1px outset black;">Protocolo</th>
                    <th style="border: 1px outset black;">Quantidade de sessões</th>
                </tr>
                </thead>

                <tbody>
                @foreach($request['pacotes'] as $contratoService)
                    @foreach($contratoService['pacotes']['pacotes_servicos'] as $servicos)
                        <tr>
                            <th style="text-align: center; border: 1px outset black;">{{$servicos['nome']}}</th>
                            <th style="text-align: center; border: 1px outset black;">{{$servicos['pivot']['qtd_sessao']}}</th>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="height: auto; width: 50%; display: block; float: right;">
            @if($request['servicos'])
                <table style="border: 1px outset black;">
                    <thead>
                    <tr>
                        <th style="border: 1px outset black;">Protocolo</th>
                        <th style="border: 1px outset black;">Quantidade de sessões</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($request['servicos'] as $service)
                        <tr>
                            <th style="text-align: center; border: 1px outset black;">{{$service['nome']}}</th>
                            <th style="text-align: center; border: 1px outset black;">{{$service['quantidade']}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
    <div>
        <p><strong>Valor total do Pacote:</strong> R$ {{ $valor }}</p>
        <p>
            <strong>Forma de Pagamento:</strong> {{ $formaDePagamento->nome }}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            @if($contrato->num_parcelas > 1)
                Parcelado em <strong>{{ $contrato->num_parcelas }}</strong> vezes.
            @endif
        </p>
    </div>

    <div>
        <h4>No objetivo de prestar um atendimento qualificado, informamos o que segue:</h4>
        <p>1) A contratante é responsável pelas informações referentes à sua saúde, doenças contagiosas, gravidez, cirurgia, uso de "DIU", marca passo ou próteses
            de qualquer especie, alergias, medicamentos de uso contínuo, ou qualquer outra informação relevante relacionada à saúde, ficando a contratada isenta de
            qualquer responsabilidade relativa a efeitos colaterais, sequelas, ou problemas relacionados à pré-existência omitida;</p>
        <p>2) O tratamento acima contratado tem um prazo de 06(seis) meses para ser concluído, caso haja algum imprevisto por parte da contratante, a mesma deverá comunicar para que haja a prorrogação desse prazo por igual período;</p>
        <p>3) Estando, por qualquer motivo, impossibilitada de prosseguir o tratamento, a contratante poderá transferir as sessões restantes à terceira pessoa mediante avaliação da mesma;</p>
        <p>4) As sessões devem ser previamente agendadas e as faltas e atrasos devem ser comunicados com antecedência;</p>
        <p>5) Cada tratamento é único, os resultados variam, sendo assim, não são dadas promessas de resultados, principalmente porque eles dependerão também da cliente;</p>
        <p>6) O presente instrumento poderá ser rescindido mediante o pagamento das sessões já realizadas, tendo por base o valor atual das sessões avulsas, (verso), além do pagamento da multa rescisória no valor de 10% (dez por cento) do valor total do contrato;</p>
        <p>7) O abandono do tratamento não implica em cancelamento automático do presente instrumento;</p>
        <p>8) Em caso de pagamento através de cheques, a não compensação dos mesmos está sujeita ao protesto automático e posterior ação de cobrança;</p>
        <p>9) A contratante declara ter recebido todas as orientações referentes ao tratamento prosposto;</p>
        <p>10) A contratante declara não ter omitido nenhuma informação sobre seu estado de saúde, doenças prévias, cicatrização hipertrófica, gravidez, amamentação, idade, cirurgias anteriores, uso de medicações,, implantes ou protéses, realização de outros tratamentos e alergias;</p>
        <p>11) A contratante expressa concordância e espontânea vontade em submeter-se ao referido tratamento, assumindo a responsabilidade e os riscos pelos eventuais efeitos indesejáveis decorrentes;</p>
        <p>12) Fica eleito o foro desta cidade para dirimir quaisquer questões referentes ao presente instrumento;</p>
        <p>13) A contratante declara ter lido e concordado com os termos do presente contrato.</p>
    </div>
    <br>
    <br>
    <br>

    <div class="data-final">
        _____________________________,_______de___________________________de 20________
    </div>
    <br>
    <br>
    <br>

    <table cellspacing="30">
        <tr>
            <td>____________________________________________________</td> <td>____________________________________________________</td>
        </tr>
        <tr>
            <td><strong>Contratante</strong></td> <td><strong>Contratada</strong></td>
        </tr>
    </table>
</div>
<br><br><br><br><br><br><br>

<div>
    <div class="header">
        <h2>Valor de tabela dos serviços</h2>
    </div>
    <table class="table" style="border: 1px outset black;">
            <thead>
            <tr>
                <th style="border: 1px outset black;">Procedimento</th>
                <th style="border: 1px outset black;">Valor</th>
            </tr>
            </thead>
            <tbody>
            @foreach($servicosTabela as $servicoTabela)
                <tr>
                    <td style="border: 1px outset black;">{{$servicoTabela['nome']}}</td>
                    <td style="border: 1px outset black;">R$ {{$servicoTabela['valor']}}, 00</td>
                </tr>
            @endforeach
            </tbody>
        </table>
</div>
