@extends('layouts.app', [
    'namePage' => 'Fechamento',
    'class' => 'sidebar-mini',
    'activePage' => 'fechamento',
    'activeNav' => '',
])

@section('style')
    <link rel="stylesheet" href="{{asset('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('lib/jquery-ui-1.12.1.custom/jquery-ui.theme.css')}}">
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('Protocolos recomendados') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <fechar-recomendacao></fechar-recomendacao>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
