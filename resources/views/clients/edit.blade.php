@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Editar cliente',
    'activePage' => 'cliente',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Clientes') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cliente.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('cliente.update', $cliente) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações do Cliente') }}</h6>
                                <div class="row">
                                    <div class="col-md-8 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Nome') }}</label>
                                            <input type="text" name="nome" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('nome', $cliente->nome) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cpf">{{ __('CPF') }}</label>
                                            <input type="text" name="cpf" cpf="cpf" id="input-cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('CPF') }}" value="{{ old('cpf', $cliente->cpf) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cpf'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('data_nasc') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-data_nasc">{{ __('Data de Nascimento') }}</label>
                                            <input type="text" name="data_nasc" data_nasc="data_nasc" id="input-data_nasc" class="form-control{{ $errors->has('data_nasc') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Nascimento') }}" value="{{ old('data_nasc', $cliente->data_nasc) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'data_nasc'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('profissao') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-profissao">{{ __('Profissão') }}</label>
                                            <input type="text" name="profissao" profissao="profissao" id="input-profissao" class="form-control{{ $errors->has('profissao') ? ' is-invalid' : '' }}" placeholder="{{ __('Profissão') }}" value="{{ old('profissao', $cliente->profissao) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'profissao'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('estado_civil') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estado_civil">{{ __('Estado Civil') }}</label>
                                            <select type="text" name="estado_civil" estado_civil="estado_civil" id="input-estado_civil" class="form-control{{ $errors->has('estado_civil') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado Civil') }}" value="{{ old('estado_civil', $cliente->estado_civil) }}"  required autofocus>
                                                <option value="{{old('estado_civil', $cliente->estado_civil)}}">{{old('estado_civil', $cliente->estado_civil)}}</option>
                                                @foreach(App\Cliente::ESTADO_CIVIL as $estado_civil)
                                                    <option value="{{$estado_civil}}">{{$estado_civil}}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'estado_civil'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de endereço--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Endereço do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('cep') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cep">{{ __('CEP') }}</label>
                                            <input type="text" name="cep" cep="cep" id="input-cep" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" value="{{ old('cep', isset($cliente->endereco->cep) ? $cliente->endereco->cep : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cep'])
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="form-group{{ $errors->has('logradouro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-logradouro">{{ __('Logradouro') }}</label>
                                            <input type="text" name="logradouro" logradouro="logradouro" id="input-logradouro" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" value="{{ old('logradouro', isset($cliente->endereco->logradouro) ? $cliente->endereco->logradouro : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'logradouro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('numero') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-numero">{{ __('Numero') }}</label>
                                            <input type="text" name="numero" numero="numero" id="input-numero" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero') }}" value="{{ old('numero', isset($cliente->endereco->numero) ? $cliente->endereco->numero : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'numero'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('bairro') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-bairro">{{ __('Bairro') }}</label>
                                            <input type="text" name="bairro" bairro="bairro" id="input-bairro" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" value="{{ old('bairro', isset($cliente->endereco->bairro) ? $cliente->endereco->bairro : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'bairro'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('complemento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-complemento">{{ __('Complemento') }}</label>
                                            <input type="text" name="complemento" complemento="complemento" id="input-complemento" class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" value="{{ old('complemento', isset($cliente->endereco->complemento) ? $cliente->endereco->complemento : "") }}">

                                            @include('alerts.feedback', ['field' => 'complemento'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('cidade') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cidade">{{ __('Cidade') }}</label>
                                            <input type="text" name="cidade" cidade="cidade" id="input-cidade" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" placeholder="{{ __('Cidade') }}" value="{{ old('cidade', isset($cliente->endereco->cidade) ? $cliente->endereco->cidade : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'cidade'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('estado') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-estado">{{ __('Estado') }}</label>
                                            <input type="text" name="estado" estado="estado" id="input-estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado') }}" value="{{ old('estado', isset($cliente->endereco->estado) ? $cliente->endereco->estado : "") }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'estado'])
                                        </div>
                                    </div>
                                </div>

                                <hr> {{--Cadastro de contato--}}
                                <h6 class="heading-small text-muted mb-4">{{ __('Contato do Cliente') }}</h6>

                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('E-mail') }}</label>
                                            <input type="text" name="email" email="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-mail') }}" value="{{ old('email', $cliente->contato->email) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'email'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                            <input type="text" name="telefone" telefone="telefone" id="input-telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone', $cliente->contato->telefone) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'telefone'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                            <input type="text" name="celular" celular="celular" id="input-celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular', $cliente->contato->celular) }}"  required autofocus>

                                            @include('alerts.feedback', ['field' => 'celular'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
