@extends('layouts.app', [
    'namePage' => 'Atender Sessão Cliente',
    'class' => 'sidebar-mini',
    'activePage' => 'sessao',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <!-- Header com nome do cliente/paciente e botão de voltar -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h5 class="mb-0">Sessões</h5>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $cliente[0]['id']) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('Data') }}</th>
                                <th>{{ __('Nº Contrato') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Ação') }}</th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            @foreach($contratos as $contrato)--}}
                                <tr>
{{--                                    <td>{{ date('d/m/Y', strtotime($contrato->data)) }}</td>--}}
                                    <td>teste 1</td>
                                    <td>teste 2</td>
                                    <td>teste 3</td>
                                    <td class="text-right">
                                        <a type="button" href="#" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="Executar">
                                            <i class="now-ui-icons ui-1_check"></i>
                                        </a>
                                    </td>
                                </tr>
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>
        <!-- end row -->
    </div>
@endsection

