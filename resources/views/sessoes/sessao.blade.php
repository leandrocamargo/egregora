@extends('layouts.app', [
    'namePage' => 'Atender Cliente',
    'class' => 'sidebar-mini',
    'activePage' => 'sessao',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <!-- Header com nome do cliente/paciente e botão de voltar -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h5 class="mb-0">Cliente {{ $cliente->nome }}</h5>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('sessao.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <!-- Avaliação -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @if(isset($avaliacao->cliente_id) && $avaliacao->cliente_id == $cliente->id)
                            <h6 class="card-title">
                                {{ __('Avaliação') }}

                                @if(isset($avaliacao->id))
                                    <a type="button" href="{{ route('avaliacao.detalhes', $cliente->id)  }}" rel="tooltip" class="btn btn-warning btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Dashboard') }}">
                                        <i class="now-ui-icons business_chart-bar-32"></i>
                                    </a>

                                    <a type="button" href="{{ route('avaliacao.edit', $avaliacao->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('avaliacao.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>
                                <i class="fa fa-check" style="font-size: 23px; color: green"></i>
                            </h6>
                        @else
                            <h6 class="card-title">
                                {{ __('Avaliação') }}

                                @if(isset($avaliacao->id))
                                    <a type="button" href="{{ route('avaliacao.detalhes', $cliente->id)  }}" rel="tooltip" class="btn btn-warning btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Dashboard') }}">
                                        <i class="now-ui-icons business_chart-bar-32"></i>
                                    </a>

                                    <a type="button" href="{{ route('avaliacao.edit', $avaliacao->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('avaliacao.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>
                            </h6>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- Anamnese -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @if(isset($anamnese->cliente_id) && $anamnese->cliente_id == $cliente->id)
                            <h6 class="card-title">
                                {{ __('Anamnese') }}

                                @if(isset($anamnese->id))
                                    <a type="button" href="{{ route('anamnese.edit', $anamnese->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <i class="fa fa-check" style="font-size: 23px; color: green"></i>
                            </h6>
                        @else
                            <h6 class="card-title">{{ __('Anamnese') }}

                                @if(isset($anamnese->id))
                                    <a type="button" href="{{ route('anamnese.edit', $anamnese->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                            </h6>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- Histórico Médico -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @if(isset($historico_medico->cliente_id) && $historico_medico->cliente_id == $cliente->id)
                            <h6 class="card-title">
                                {{ __('Histórico Médico') }}

                                @if(isset($historico_medico->id))
                                    <a type="button" href="{{ route('historico_medico.edit', $historico_medico->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('historico_medico.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>

                                <i class="fa fa-check" style="font-size: 23px; color: green"></i>
                            </h6>
                        @else
                            <h6 class="card-title">{{ __('Histórico Médico') }}

                                @if(isset($historico_medico->id))
                                    <a type="button" href="{{ route('historico_medico.edit', $historico_medico->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('historico_medico.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>
                            </h6>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- Tratamento  Estético -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        @if(isset($tratamento_estetico->cliente_id) && $tratamento_estetico->cliente_id == $cliente->id)
                            <h6 class="card-title">
                                {{ __('Tratamentos Estéticos') }}

                                @if(isset($tratamento_estetico->id))
                                    <a type="button" href="{{ route('tratamento_estetico.edit', $tratamento_estetico->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('tratamento_estetico.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>

                                <i class="fa fa-check" style="font-size: 23px; color: green"></i>
                            </h6>
                        @else
                            <h6 class="card-title">{{ __('Tratamentos Estéticos') }}

                                @if(isset($tratamento_estetico->id))
                                    <a type="button" href="{{ route('tratamento_estetico.edit', $tratamento_estetico->id)  }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Editar') }}">
                                        <i class="now-ui-icons files_paper"></i>
                                    </a>
                                @endif

                                <a type="button" href="{{ route('tratamento_estetico.create', $cliente->id)  }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="{{ __('Criar') }}">
                                    <i class="now-ui-icons ui-2_settings-90"></i>
                                </a>
                            </h6>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- Contratos do Cliente -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h6 style="text-align: center">Contratos</h6>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('Data') }}</th>
                                <th>{{ __('Nº Contrato') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Ação') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contratos as $contrato)
                                <tr>
                                    <td>{{ date('d/m/Y', strtotime($contrato->data)) }}</td>
                                    <td>{{ $contrato->numero_contrato }}</td>
                                    <td>{{ $contrato->status }}</td>
                                    <td class="text-right">
                                        <a type="button" href="{{ route('sessao.atendimento', $contrato->id) }}" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="Sessões">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="card-footer">
                            {{ $contratos->links() }}
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>
        <!-- end row -->
    </div>
@endsection
