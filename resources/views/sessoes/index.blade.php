@extends('layouts.app', [
    'namePage' => 'Atendimentos',
    'class' => 'sidebar-mini',
    'activePage' => 'sessao',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('Atendimentos') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('Cliente') }}</th>
                                <th>{{ __('Consulta') }}</th>
                                <th>{{ __('Atendimento') }}</th>
                                <th>{{ __('Horário') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Ação') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($atendimentos as $atendimento)
                                <tr>
                                    <td>{{ $atendimento->cliente->nome }}</td>
                                    <td>{{ $atendimento->consulta_tipo }}</td>

                                    @if(isset($atendimento->servico->descricao))
                                        <td>{{ $atendimento->servico->descricao }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ date('d/m/Y H:m', strtotime($atendimento->horario_inicio)) }}</td>
                                    <td>{{ $atendimento->status }}</td>
                                    <td>
                                        <a type="button" href="{{ route('recomendar.protocolo', $atendimento->cliente_id) }}" rel="tooltip" class="btn btn-warning btn-icon btn-sm pull-right" data-original-title="" title="Protocolo">
                                            <i class="now-ui-icons media-2_sound-wave"></i>
                                        </a>

                                        <a type="button" href="{{ route('atender.cliente', $atendimento->cliente_id) }}" rel="tooltip" class="btn btn-success btn-icon btn-sm pull-right" data-original-title="" title="Atendimento">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>

                                        <a type="button" href="{{ route('anamnese.create', $atendimento->cliente_id) }}" rel="tooltip" class="btn btn-primary btn-icon btn-sm pull-right" data-original-title="" title="Anamnese">
                                            <i class="now-ui-icons files_paper"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $atendimentos->links() }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".delete-button").click(function(){
                var clickedButton = $( this );
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        clickedButton.parents(".delete-form").submit();
                    }
                })

            });
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function() {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function(e) {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function() {
                alert('You clicked on Like button');
            });
        });
    </script>
@endpush

