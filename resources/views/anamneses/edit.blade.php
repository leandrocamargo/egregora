@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Anamnese',
    'activePage' => 'anamnese',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Anamnese') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('atender.cliente', $anamnese->cliente_id) }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('anamnese.update', $anamnese) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="pl-lg-4">
                                <h6 class="heading-small text-muted mb-4">{{ __('Informações da Anamnese') }}</h6>

                                <div class="col-lg-4 col-sm-4 col-xs-12" id="form0">
                                    <div class="form-group{{ $errors->has('cliente_id') ? ' has-danger' : '' }}">
                                        <label for="exampleFormControlSelect1">Cliente</label>
                                        <input name="cliente_id" class="form-control" id="exampleFormControlSelect1" value="{{ $anamnese->cliente->nome }}" disabled>
                                        <input type="hidden" name="cliente_id" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" id="exampleFormControlSelect1" value="{{ $anamnese->cliente_id }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cliente_id'])
                                    </div>
                                </div>

                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">1- Você realizou algum tratamento estético anteriormente?*</label><br />
                                        <label for="tratSim" class="radio-inline"><input type="radio" name="trat_estetic_anterior" id="tratSim" value="1" class="radio-inline{{ $errors->has('trat_estetic_anterior') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->trat_estetic_anterior == 1 ) { echo "checked=\"checked\"";} else { echo "";}?> > Sim</label>
                                        <label for="tratNao" class="radio-inline"><input type="radio" name="trat_estetic_anterior" id="tratNao" value="0" class="radio-inline{{ $errors->has('trat_estetic_anterior') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->trat_estetic_anterior == 0 ) { echo "checked=\"checked\"";} else { echo "";}?> > Não</label>

                                        @include('alerts.feedback', ['field' => 'trat_estetic_anterior'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 qualEdit" id="qualEdit">
                                    <div class="form-group">
                                        <label for="qual">Qual</label>
                                        <input type="text" class="form-control"  name="trat_estetic_anterior_qual" value="{{$anamnese->trat_estetic_anterior_qual}}">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">2- Utiliza Cosméticos?*</label><br />
                                        <label for="cosmSim" class="radio-inline"><input type="radio" name="utiliz_cosmeticos" id="cosmSim" value="1" class="radio-inline{{ $errors->has('utiliz_cosmeticos') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->utiliz_cosmeticos == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="cosmNao" class="radio-inline"><input type="radio" name="utiliz_cosmeticos" id="cosmNao" value="0" class="radio-inline{{ $errors->has('utiliz_cosmeticos') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->utiliz_cosmeticos == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'utiliz_cosmeticos'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="qual2Edit">
                                    <div class="form-group">
                                        <label for="qual">Qual</label>
                                        <input type="text" class="form-control" name="utiliz_cosmeticos_qual" value="{{$anamnese->utiliz_cosmeticos_qual}}">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">3- Você tem exposição ao sol?*</label><br />
                                        <label for="expSim" class="radio-inline"><input type="radio" name="exp_sol" id="exp" value="1" class="radio-inline{{ $errors->has('exp_sol') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->exp_sol == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="expNao" class="radio-inline"><input type="radio" name="exp_sol" id="exp" value="0" class="radio-inline{{ $errors->has('exp_sol') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->exp_sol == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'exp_sol'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">4- Você usa filtro solar?*</label><br />
                                        <label for="filtSim" class="radio-inline"><input type="radio" name="uso_filtro" id="filt" value="1" class="radio-inline{{ $errors->has('uso_filtro') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->uso_filtro == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="filtNao" class="radio-inline"><input type="radio" name="uso_filtro" id="filt" value="0" class="radio-inline{{ $errors->has('uso_filtro') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->uso_filtro == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'uso_filtro'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">5- Tabagismo?*</label><br />
                                        <label for="tabSim" class="radio-inline"><input type="radio" name="tabagismo" id="tab" value="1" class="radio-inline{{ $errors->has('tabagismo') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->tabagismo == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="tabNao" class="radio-inline"><input type="radio" name="tabagismo" id="tab" value="0" class="radio-inline{{ $errors->has('tabagismo') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->tabagismo == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'tabagismo'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">6- Consome bebida alcoolica?*</label><br />
                                        <label for="bebNunca" class="radio-inline"><input type="radio" name="bebida_alcoolica" id="beb" value="Nunca" class="radio-inline{{ $errors->has('bebida_alcoolica') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->bebida_alcoolica == 'Nunca' ) { echo "checked=\"checked\"";} else { echo "";}?>> Nunca</label>
                                        <label for="bebRaro" class="radio-inline"><input type="radio" name="bebida_alcoolica" id="beb" value="Raro" class="radio-inline{{ $errors->has('bebida_alcoolica') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->bebida_alcoolica == 'Raro' ) { echo "checked=\"checked\"";} else { echo "";}?>> Raro</label>
                                        <label for="bebSempre" class="radio-inline"><input type="radio" name="bebida_alcoolica" id="beb" value="Sempre" class="radio-inline{{ $errors->has('bebida_alcoolica') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->bebida_alcoolica == 'Sempre' ) { echo "checked=\"checked\"";} else { echo "";}?>> Sempre</label>

                                        @include('alerts.feedback', ['field' => 'bebida_alcoolica'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">7- Funcionamento Intestinal*</label><br />
                                        <label for="intestReg" class="radio-inline"><input type="radio" name="func_intestinal" id="intest" value="Reg" class="radio-inline{{ $errors->has('func_intestinal') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->func_intestinal == 'Reg' ) { echo "checked=\"checked\"";} else { echo "";}?>> Regular</label>
                                        <label for="instestIrreg" class="radio-inline"><input type="radio" name="func_intestinal" id="intest" value="Irreg" class="radio-inline{{ $errors->has('func_intestinal') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->func_intestinal == 'Irreg' ) { echo "checked=\"checked\"";} else { echo "";}?>> Irregular</label>

                                        @include('alerts.feedback', ['field' => 'func_intestinal'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="obs">
                                    <div class="form-group">
                                        <label for="obs">Observação:</label>
                                        <input type="text" class="form-control" name="func_intestinal_obs" value="{{$anamnese->func_intestinal_obs}}">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">8- Qualidade de Sono*</label><br />
                                        <label for="sonoBom" class="radio-inline"><input type="radio" name="qualidade_sono" id="sono" value="Boa" class="radio-inline{{ $errors->has('qualidade_sono') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->qualidade_sono == 'Boa' ) { echo "checked=\"checked\"";} else { echo "";}?> > Boa</label>
                                        <label for="sonoReg" class="radio-inline"><input type="radio" name="qualidade_sono" id="sono" value="Reg" class="radio-inline{{ $errors->has('qualidade_sono') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->qualidade_sono == 'Reg' ) { echo "checked=\"checked\"";} else { echo "";}?> > Regular</label>
                                        <label for="sonoPess" class="radio-inline"><input type="radio" name="qualidade_sono" id="sono" value="Pess" class="radio-inline{{ $errors->has('qualidade_sono') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->qualidade_sono == 'Pess' ) { echo "checked=\"checked\"";} else { echo "";}?> > Péssima</label>

                                        @include('alerts.feedback', ['field' => 'qualidade_sono'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="qtd_horas">
                                    <div class="form-group">
                                        <label for="qtd_horas">Quantas Horas:</label>
                                        <input type="text"  name="qualidade_sono_obs" value="{{$anamnese->qualidade_sono_obs}}" class="form-control{{ $errors->has('qualidade_sono_obs') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'qualidade_sono_obs'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">9- Ingestão de Água*</label><br />
                                        <label for="menos2" class="radio-inline"><input type="radio" name="ingest_agua" id="agua" value="menos2" class="radio-inline{{ $errors->has('ingest_agua') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->ingest_agua == 'menos2' ) { echo "checked=\"checked\"";} else { echo "";}?>> Menos de 2 litros por dia</label>
                                        <label for="mais2" class="radio-inline"><input type="radio" name="ingest_agua" id="agua" value="mais2" class="radio-inline{{ $errors->has('ingest_agua') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->ingest_agua == 'mais2' ) { echo "checked=\"checked\"";} else { echo "";}?>> Mais de 2 litros por dia</label>

                                        @include('alerts.feedback', ['field' => 'ingest_agua'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">10- Alimentação*</label><br />
                                        <label for="alimentBom" class="radio-inline"><input type="radio" name="alimentacao" id="aliment" value="Boa" class="radio-inline{{ $errors->has('alimentacao') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->alimentacao == 'Boa' ) { echo "checked=\"checked\"";} else { echo "";}?>> Boa</label>
                                        <label for="alimentReg" class="radio-inline"><input type="radio" name="alimentacao" id="aliment" value="Reg" class="radio-inline{{ $errors->has('alimentacao') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->alimentacao == 'Reg' ) { echo "checked=\"checked\"";} else { echo "";}?>> Regular</label>
                                        <label for="alimentPess" class="radio-inline"><input type="radio" name="alimentacao" id="aliment" value="Pess" class="radio-inline{{ $errors->has('alimentacao') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->alimentacao == 'Pess' ) { echo "checked=\"checked\"";} else { echo "";}?>> Péssima</label>

                                        @include('alerts.feedback', ['field' => 'alimentacao'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">11- Alimentos de Preferência*</label><br />
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="qtd_horas">
                                    <div class="form-group">
                                        <input type="text"  name="alimentos_pref" value="{{$anamnese->alimentos_pref}}" class="form-control{{ $errors->has('alimentos_pref') ? ' is-invalid' : '' }}">

                                        @include('alerts.feedback', ['field' => 'alimentacao_pref'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">12- Pratica atividade física?*</label><br />
                                        <label for="atvSim" class="radio-inline"><input type="radio" name="prat_atv_fisica" id="atvSim" value="1" class="radio-inline{{ $errors->has('prat_atv_fisica') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->prat_atv_fisica == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="atvNao" class="radio-inline"><input type="radio" name="prat_atv_fisica" id="atvNao" value="0" class="radio-inline{{ $errors->has('prat_atv_fisica') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->prat_atv_fisica == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'prat_atv_fisica'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="qualFreqEdit">
                                    <div class="form-group">
                                        <label for="qualFreq">Qual frequência?</label>
                                        <input type="text" class="form-control" name="prat_atv_fisica_freq" value="{{$anamnese->prat_atv_fisica_freq}}">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">13- Usa Anticoncepcional?*</label><br />
                                        <label for="anticoncepSim" class="radio-inline"><input type="radio" name="uso_anticoncepcional" id="anticoncep" value="1" class="radio-inline{{ $errors->has('uso_anticoncepcional') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->uso_anticoncepcional == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="anticoncepNao" class="radio-inline"><input type="radio" name="uso_anticoncepcional" id="anticoncep" value="0" class="radio-inline{{ $errors->has('uso_anticoncepcional') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->uso_anticoncepcional == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'uso_anticoncepcional'])
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">14- Ja teve alguma gestação?*</label><br />
                                        <label for="gestSim" class="radio-inline"><input type="radio" name="gestacao" id="gestSim" value="1" class="radio-inline{{ $errors->has('gestacao') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->gestacao == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="gestNao" class="radio-inline"><input type="radio" name="gestacao" id="gestNao" value="0" class="radio-inline{{ $errors->has('gestacao') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->gestacao == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'gestacao'])
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4" id="quantasEdit">
                                    <div class="form-group">
                                        <label for="quantas">Quantas?</label>
                                        <input type="number"  name="gestacao_qtde" value="{{$anamnese->gestacao_qtde}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="">15- Está gravida?*</label><br />
                                        <label for="gravSim" class="radio-inline"><input type="radio" name="gravida" id="grav" value="1" class="radio-inline{{ $errors->has('gravida') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->gravida == 1 ) { echo "checked=\"checked\"";} else { echo "";}?>> Sim</label>
                                        <label for="gravNao" class="radio-inline"><input type="radio" name="gravida" id="grav" value="0" class="radio-inline{{ $errors->has('gravida') ? ' is-invalid' : '' }}" <?php  if ( $anamnese->gravida == 0 ) { echo "checked=\"checked\"";} else { echo "";}?>> Não</label>

                                        @include('alerts.feedback', ['field' => 'gravida'])
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




