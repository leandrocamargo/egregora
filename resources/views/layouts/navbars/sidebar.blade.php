<div class="sidebar" data-color="orange">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
  <div class="logo">
    <div class="simple-text logo-normal text-center">
        <img src="{{asset('assets')}}/img/logo.png" alt="" style="max-width: 80px; border-radiu: 100px"> Egregora
    </div>
  </div>
  <div class="sidebar-wrapper" id="sidebar-wrapper">
    <ul class="nav">
      <li class="@if ($activePage == 'home') active @endif">
        <a href="{{ route('home') }}">
          <i class="now-ui-icons design_app"></i>
          <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="@if ($activePage == 'agenda') active @endif">
          <a href="{{ route('agenda.index') }}">
              <i class="now-ui-icons ui-1_calendar-60"></i>
              <p>{{ __('Agenda') }}</p>
          </a>
      </li>
      <li class="@if ($activePage == 'sessao') active @endif">
          <a href="{{ route('sessao.index') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p>{{ __('Atendimento') }}</p>
          </a>
      </li>
      <li>
        <a data-toggle="collapse" href="#cadastro">
            <i class="fas fa-pen-square"></i>
          <p>
            {{ __("Cadastros") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="cadastro">
          <ul class="nav">
            <li class="@if ($activePage == 'profile') active @endif">
              <a href="{{ route('profile.edit') }}">
                <i class="now-ui-icons users_single-02"></i>
                <p> {{ __("Perfil do Usuário") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'unidade') active @endif">
                <a href="{{ route('unidade.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Unidades") }} </p>
                </a>
            </li>
            <li class="@if ($activePage == 'users') active @endif">
              <a href="{{ route('usuario.index') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Gestão de Usuários") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'cliente') active @endif">
                <a href="{{ route('cliente.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Clientes") }} </p>
                </a>
            </li>
            <li class="@if ($activePage == 'servico') active @endif">
                <a href="{{ route('servico.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Servicos") }} </p>
                </a>
            </li>
            <li class="@if ($activePage == 'pacote') active @endif">
                <a href="{{ route('pacote.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Pacotes") }} </p>
                </a>
            </li>
            <li class="@if ($activePage == 'produto') active @endif">
                <a href="{{ route('produto.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Produtos") }} </p>
                </a>
            </li>
            <li class="@if ($activePage == 'atendimento') active @endif">
                <a href="{{ route('tipo-atendimento.index') }}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p> {{ __("Gestão de Atendimentos") }} </p>
                </a>
            </li>
          </ul>
        </div>
      </li>
        <li>
            <a data-toggle="collapse" href="#financeiro">
                <i class="fas fa-hand-holding-usd"></i>
                <p>
                    {{ __("Financeiro") }}
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse" id="financeiro">
                <ul class="nav">
                    <li class="@if ($activePage == 'contrato') active @endif">
                        <a href="{{ route('contratos.index') }}">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p> {{ __("Contratos") }} </p>
                        </a>
                    </li>

                    <li class="@if ($activePage == 'controle_cheque') active @endif">
                        <a href="{{ route('cheque.index') }}">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p> {{ __("Controle de Cheques") }} </p>
                        </a>
                    </li>

                    <li class="@if ($activePage == 'forma_pagamento') active @endif">
                        <a href="{{ route('forma_pagamento.index') }}">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p> {{ __("Formas de Pagamentos") }} </p>
                        </a>
                    </li>

                    <li class="@if ($activePage == 'indicacao') active @endif">
                        <a href="{{ route('indicacao.index') }}">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p> {{ __("Indicações") }} </p>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
  </div>
</div>
