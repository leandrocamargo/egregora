<footer class="footer">
  <div class=" container-fluid ">
    <div class="copyright" id="copyright">
        <p>&copy; Desenvolvido por
            <a class="brand-dev" href="http://tmontec.com.br" target="_blank">
                <img src="{{ asset('/imagens/logoTmontec.png') }}" alt="Tmontec" class="img-responsive">
            </a>
        </p>
    </div>
  </div>
</footer>
