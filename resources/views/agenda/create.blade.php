@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Agenda',
    'activePage' => 'agenda',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Gestão de Agendamentos') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('agenda.index') }}" class="btn btn-primary btn-round">{{ __('Voltar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <div class="pl-lg-4">
                            <h6 class="heading-small text-muted mb-4">{{ __('Informações do Agendamento') }}</h6>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="auto_complete" name="cliente" placeholder="Busca por CPF">
                                        <button class="btn_auto" onclick="getClienteAgendar()">
                                            <span class="input-group-btn">
                                                <i class="fas fa-angle-double-down"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form method="post" action="{{ route('agenda.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" name="input-id" id="input-id">

                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-md-2 pr-1">
                                        <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-cpf">{{ __('CPF') }}</label>
                                            <input type="text" name="cpf" cpf="cpf" id="input-cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('CPF') }}" value="{{ old('cpf') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'cpf'])
                                        </div>
                                    </div>

                                    <div class="col-md-4 pr-1">
                                        <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-nome">{{ __('Cliente') }}</label>
                                            <input type="text" name="nome" nome="nome" id="input-nome" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Cliente') }}" value="{{ old('nome') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'nome'])
                                        </div>
                                    </div>

                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                            <input type="text" name="telefone" telefone="telefone" id="input-telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'telefone'])
                                        </div>
                                    </div>

                                    <div class="col-md-3 pr-1">
                                        <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                            <input type="text" name="celular" celular="celular" id="input-celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular') }}" required autofocus>

                                            @include('alerts.feedback', ['field' => 'celular'])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('horario_inicio') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-horario_inicio">{{ __('Horário Inicial') }}</label>
                                            <input type="text" name="horario_inicio" minlength="16"  maxlength="16" horario_inicio="horario_inicio" id="input-horario_inicio" class="form-control{{ $errors->has('horario_inicio') ? ' is-invalid' : '' }}" placeholder="{{ __('Horário Inicial') }}" value="{{ old('horario_inicio') }}" onkeypress="DataHora(event, this)" required autofocus>

                                            @include('alerts.feedback', ['field' => 'horario_inicio'])
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group{{ $errors->has('horario_final') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-horario_final">{{ __('Horário Final') }}</label>
                                            <input type="text" name="horario_final" minlength="16"  maxlength="16" horario_final="horario_final" id="input-horario_final" class="form-control{{ $errors->has('horario_final') ? ' is-invalid' : '' }}" placeholder="{{ __('Horário Final') }}" value="{{ old('horario_final') }}" onkeypress="DataHora(event, this)" required autofocus>

                                            @include('alerts.feedback', ['field' => 'horario_final'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('consulta_tipo') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-consulta_tipo">{{ __('Consulta') }}</label>
                                            <select type="tex" name="consulta_tipo" consulta_tipo="consulta_tipo" id="input-consulta_tipo" class="form-control{{ $errors->has('consulta_tipo') ? ' is-invalid' : '' }}" placeholder="{{ __('Consulta') }}" value="{{ old('consulta_tipo') }}" required autofocus>
                                                <option value="">Selecione um tipo de consulta</option>
                                                @foreach(App\Agenda::CONSULTA_TIPO as $consulta_tipo)
                                                    <option value="{{ $consulta_tipo }}">{{$consulta_tipo}}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'consulta_tipo'])
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('tipo_atendimento') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-tipo_atendimento">{{ __('Serviço') }}</label>
                                            <select type="tex" name="tipo_atendimento" tipo_atendimento="tipo_atendimento" id="input-tipo_atendimento" class="form-control{{ $errors->has('tipo_atendimento') ? ' is-invalid' : '' }}" placeholder="{{ __('Serviço') }}" value="{{ old('tipo_atendimento') }}">
                                                <option value="">Selecione um atendimento</option>
                                                @foreach($servicos as $servico)
                                                    <option value="{{$servico->id}}">{{$servico->descricao}}</option>
                                                @endforeach
                                            </select>
                                            @include('alerts.feedback', ['field' => 'tipo_atendimento'])
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Salvar') }}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



