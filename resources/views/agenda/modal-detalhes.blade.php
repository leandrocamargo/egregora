<!-- Modal -->
<div class="modal fade" data-backdrop="static" id="modalDetalhes" tabindex="-1" role="dialog" aria-labelledby="modalAgendarTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAgendarTitle">Detalhes do agendamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"id="fechar">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="detalhesAgendamento">
                    <dl class="row">
                        <dt class="col-sm-3">Cliente</dt>
                        <dd class="col-sm-9" id="cliente"></dd>

                        <dt class="col-sm-3">Telefone</dt>
                        <dd class="col-sm-9" id="telefone"></dd>

                        <dt class="col-sm-3">Celular</dt>
                        <dd class="col-sm-9" id="celular"></dd>

                        <dt class="col-sm-3">Horario Inicial</dt>
                        <dd class="col-sm-9" id="horario_inicio"></dd>

                        <dt class="col-sm-3">Horario Final</dt>
                        <dd class="col-sm-9" id="horario_final"></dd>

                        <dt class="col-sm-3">Consulta</dt>
                        <dd class="col-sm-9" id="consulta_tipo"></dd>

                        <dt class="col-sm-3">Serviço</dt>
                        <dd class="col-sm-9" id="servico"></dd>

                        <dt class="col-sm-3">Status</dt>
                        <dd class="col-sm-9" id="status"></dd>
                    </dl>

                    <div class="modal-footer">
                        <button class="btn btn-primary btn-round btn-mostra-modal-editar text-white pull-right" id="editar">Editar</button>
                    </div>
                </div>

                {{--Modal oculto para editar Agendamento--}}
                <div class="editarAgendamento">
                    <form id="editAgenda" method="post" action="" autocomplete="off"
                          enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <div class="pl-lg-4">
                            <h6 class="heading-small text-muted mb-4">{{ __('Informações do Agendamento') }}</h6>
                            <div class="row">
                                <div class="col-md-3 pr-1">
                                    <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-cpf">{{ __('CPF') }}</label>
                                        <input type="text" name="cpf" cpf="cpf" id="cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('CPF') }}" value="{{ old('cpf') }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'cpf'])
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group{{ $errors->has('nome') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-nome">{{ __('Cliente') }}</label>
                                        <input type="text" name="nome" nome="nome" id="cliente" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" placeholder="{{ __('Cliente') }}" value="{{ old('nome') }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'nome'])
                                    </div>
                                </div>

                                <div class="col-md-3 pr-1">
                                    <div class="form-group{{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-telefone">{{ __('Telefone') }}</label>
                                        <input type="text" name="telefone" telefone="telefone" id="telefone" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" value="{{ old('telefone') }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'telefone'])
                                    </div>
                                </div>

                                <div class="col-md-3 pr-1">
                                    <div class="form-group{{ $errors->has('celular') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-celular">{{ __('Celular') }}</label>
                                        <input type="text" name="celular" celular="celular" id="celular" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" value="{{ old('celular') }}" required autofocus>

                                        @include('alerts.feedback', ['field' => 'celular'])
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('horario_inicio') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-horario_inicio">{{ __('Horário Inicial') }}</label>
                                        <input type="text" name="horario_inicio" horario_inicio="horario_inicio" id="horario_inicio" class="form-control{{ $errors->has('horario_inicio') ? ' is-invalid' : '' }}" placeholder="{{ __('Horário Inicial') }}" value="{{ old('horario_inicio') }}" onkeypress="DataHora(event, this)" required autofocus>

                                        @include('alerts.feedback', ['field' => 'horario_inicio'])
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('horario_final') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-horario_final">{{ __('Horário Final') }}</label>
                                        <input type="text" name="horario_final" horario_final="horario_final" id="horario_final" class="form-control{{ $errors->has('horario_final') ? ' is-invalid' : '' }}" placeholder="{{ __('Horário Final') }}" value="{{ old('horario_final') }}" onkeypress="DataHora(event, this)" required autofocus>

                                        @include('alerts.feedback', ['field' => 'horario_final'])
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('consulta_tipo') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-consulta_tipo">{{ __('Consulta') }}</label>
                                        <select type="text" name="consulta_tipo" consulta_tipo="consulta_tipo" id="consulta_tipo" class="form-control{{ $errors->has('consulta_tipo') ? ' is-invalid' : '' }}" required autofocus>
                                            @foreach(App\Agenda::CONSULTA_TIPO as $consulta_tipo)
                                                <option value="{{ $consulta_tipo }}">{{$consulta_tipo}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'consulta_tipo'])
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('tipo_atendimento') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-tipo_atendimento">{{ __('Serviço') }}</label>
                                        <select type="text" name="tipo_atendimento" tipo_atendimento="tipo_atendimento" id="servico" class="form-control{{ $errors->has('tipo_atendimento') ? ' is-invalid' : '' }}" placeholder="{{ __('Serviço') }}" value="{{ old('tipo_atendimento') }}" required autofocus>
                                            @foreach($servicos as $servico)
                                                <option value="{{$servico->id}}">{{$servico->descricao}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'tipo_atendimento'])
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="btn btn-warning btn-round btn-cancela-modal-editar text-white" type="button">Cancelar</button>
                                <button class="btn btn-success btn-round btn-cancela-modal-editar text-white" type="submit">Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


